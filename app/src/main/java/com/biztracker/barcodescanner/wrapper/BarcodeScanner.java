package com.biztracker.barcodescanner.wrapper;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.biztracker.R;
import com.biztracker.barcodescanner.camera.CameraSource;
import com.biztracker.barcodescanner.camera.CameraSourcePreview;
import com.biztracker.barcodescanner.camera.GraphicOverlay;
import com.biztracker.barcodescanner.supporting_files.BarcodeGraphic;
import com.biztracker.barcodescanner.supporting_files.BarcodeTrackerFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.util.List;

/**
 * Created by Karan on 05/11/15.
 */
public class BarcodeScanner extends CameraSourcePreview implements BarcodeTrackerFactory.BarCodeDetected, View.OnTouchListener, View.OnClickListener {

    @Override
    public void onClick(View v) {
        this.stop();
    }

    private class ListenerRunnable implements Runnable{
        private BarcodeDetectionListener mListener;
        private Barcode mBarcode;
        public ListenerRunnable(BarcodeDetectionListener listener,Barcode barcode){
            mListener = listener;
            mBarcode = barcode;
        }
        @Override
        public void run() {
            if(mListener!=null){
                mListener.shouldContinueGivingBarCodes(mBarcode);
            }
        }
    }

    private CameraSource mCameraSource;
    public boolean active;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    public BarcodeDetectionListener listener;
    private Context context;
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    private int _xDelta;
    private int _yDelta;
    private View rootView;

    @Override
    public void onNewBarcodeDetected(final Barcode barcode) {
            if(listener != null){
                Runnable runnable = new ListenerRunnable(listener,barcode);
                final BarcodeDetectionListener tempListener = listener;
                listener = null;
                /*Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        tempListener.shouldContinueGivingBarCodes(barcode);
                    }
                };*/
                Handler handler = new Handler(context.getMainLooper());
                handler.post(runnable);
            }
        }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if(true) return false;  //Do Not Handle Touch
        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                _xDelta = X - lParams.leftMargin;
                _yDelta = Y - lParams.topMargin;
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();
                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;
                view.setLayoutParams(layoutParams);
                break;
        }
        rootView.invalidate();
        return true;
    }

    public interface BarcodeDetectionListener{
        void shouldContinueGivingBarCodes(Barcode barcode);
    }
    public BarcodeScanner(Context context, AttributeSet attrs) {
        super(context, attrs);
        mGraphicOverlay = new GraphicOverlay<>(context, attrs);
        this.addView(mGraphicOverlay);
        this.setOnClickListener(this);
        this.context = context;
    }

    public void setBarcodeDetectionListener(BarcodeDetectionListener listener){
        this.listener = listener;
    }

    public void activateBarcodeScanner(Activity activity){

        int rc = ActivityCompat.checkSelfPermission(this.context, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            this.active = true;
            createCameraSource(true, false);
        } else {
            requestCameraPermission(activity);
        }
    }

    public void startCameraSource(final Activity activity) throws SecurityException {
        // check that the device has play services available.
        final int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        if (code != ConnectionResult.SUCCESS) {
            Handler handler = new Handler(context.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Dialog dlg =
                            GoogleApiAvailability.getInstance().getErrorDialog(activity, code, 9001);
                    dlg.show();
                }
            });
        }

        if (mCameraSource != null) {
            try {
                this.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {

        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context).build();
        BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay);
        barcodeFactory.listener = this;
        barcodeDetector.setProcessor(
                new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational()) {
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = context.registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(context, "Not enough memory", Toast.LENGTH_LONG).show();
                }
        }

        CameraSource.Builder builder = new CameraSource.Builder(context, barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }

        mCameraSource = builder
                .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                .build();
    }

    private void requestCameraPermission(final Activity activity) {
        Log.w("Camera", "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(activity, permissions, RC_HANDLE_CAMERA_PERM);
        }
    }
}
