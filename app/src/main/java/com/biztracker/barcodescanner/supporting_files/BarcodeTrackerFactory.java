package com.biztracker.barcodescanner.supporting_files;

import com.biztracker.barcodescanner.camera.GraphicOverlay;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;

/**
 * Created by Karan on 06/11/15.
 */
public class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode> {

    public interface BarCodeDetected{
        void onNewBarcodeDetected(Barcode barcode);
    }
    public BarCodeDetected listener;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    public BarcodeTrackerFactory(GraphicOverlay<BarcodeGraphic> barcodeGraphicOverlay) {
        mGraphicOverlay = barcodeGraphicOverlay;
    }

    @Override
    public Tracker<Barcode> create(Barcode barcode) {
        BarcodeGraphic graphic = new BarcodeGraphic(mGraphicOverlay);
        if(listener != null){
            listener.onNewBarcodeDetected(barcode);
        }
        return new BarcodeGraphicTracker(mGraphicOverlay, graphic);
    }

}