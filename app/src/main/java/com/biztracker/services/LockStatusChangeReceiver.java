package com.biztracker.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by shubham on 23/04/16.
 */
public class LockStatusChangeReceiver extends BroadcastReceiver{

    public final static String LOCK_ACTION = "com.biztracker.lock.change";
    public final static String LOCK_EXTRA_KEY = "is_locked";

    private boolean mCurrentLockStatus = false;

    public interface OnLockStatusChanged{
        void onLockStatusChanged(boolean isAppLocked);
    }

    private OnLockStatusChanged mLockStatusChangedCallback;

    public LockStatusChangeReceiver(OnLockStatusChanged _lockStatusChangeCallback,boolean _isLocked){
        mLockStatusChangedCallback = _lockStatusChangeCallback;
        mCurrentLockStatus = _isLocked;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("BizApp","LockStatusChange: Received Intent");
        boolean isLocked = intent.getBooleanExtra(LOCK_EXTRA_KEY,false);
        if(mCurrentLockStatus != isLocked){
            mCurrentLockStatus = isLocked;
            if(mLockStatusChangedCallback != null){
                mLockStatusChangedCallback.onLockStatusChanged(isLocked);
            }
        }
    }

}
