package com.biztracker.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

/**
 * Created by shubham on 23/04/16.
 */
public class ConnectivityChangeReceiver extends BroadcastReceiver {

    private boolean isConnected = false;

    public interface OnConnectivityChanged{
        void onConnectedToInternet();
    }

    private OnConnectivityChanged mConnectionCallback;

    public ConnectivityChangeReceiver(OnConnectivityChanged _connectionCallback,Context context){
        mConnectionCallback = _connectionCallback;
        isConnected = isConnectedToInternet(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(isConnectedToInternet(context)) {
            if (isConnected == false) {
                isConnected = true;
                if (mConnectionCallback != null) {
                    mConnectionCallback.onConnectedToInternet();
                }
            }
        }else{
            isConnected = false;
        }

    }

    /*private boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        } else {
            // not connected to the internet
        }
        return false;
    }*/

    public boolean isConnectedToInternet(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        }else {
            if (connectivityManager != null) {
                //noinspection deprecation
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

}
