package com.biztracker.services;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.biztracker.fragments.registration.RegistrationObject;
import com.biztracker.urlconnections.MultipartUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by shubham on 23/04/16.
 */
public class ServerInteractionService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */

    private RegistrationObject mRegistrationObject;

    public ServerInteractionService(){
        super("UserRegistrationService");
    }

    public ServerInteractionService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //If Internet is not connected return
        mRegistrationObject = new RegistrationObject(getApplicationContext());
        if(System.currentTimeMillis() - mRegistrationObject.lastUpdatedOnServer > (60 * 60 * 4 * 1000)){
            Log.d("BizApp", "Registering User On Server");
            registerUserOnServer(mRegistrationObject);
        }else{
            Log.d("BizApp","User was recently updated on Server: "+mRegistrationObject.lastUpdatedOnServer);
        }
    }


    public void registerUserOnServer(RegistrationObject registrationObject){
        try{
            JSONObject jsondata = getPostJson(registrationObject,getApplicationContext());

            MultipartUtility utility = new MultipartUtility("https://biztracker-1282.appspot.com/user_register","utf-8");
            utility.addFormField("key","9DBeo89SDne23nsd");
            if(registrationObject.userid!=null) utility.addFormField("userid",registrationObject.userid);
            utility.addFormField("json_data", jsondata.toString());

            String response = utility.finish();

            JSONObject outputJson = new JSONObject(response);
            JSONObject result = outputJson.getJSONObject("result");
            boolean locked = result.getBoolean("locked");
            String userid = result.getString("user_id");
            String lockMessage = result.getString("lock_message");
            String purchaseAction = result.getString("purchase_action");

            registrationObject.isAppLocked = locked;
            registrationObject.userid = userid;
            registrationObject.isUserRegisteredOnServer = true;
            registrationObject.lockMessage = lockMessage;
            registrationObject.purchaseAction = purchaseAction;
            registrationObject.lastUpdatedOnServer = System.currentTimeMillis();

            registrationObject.storeInPreferences(getApplicationContext());

            //Notify LockStatusChangeReceiver
            Intent intent = new Intent();
            intent.setAction(LockStatusChangeReceiver.LOCK_ACTION);
            intent.putExtra(LockStatusChangeReceiver.LOCK_EXTRA_KEY, registrationObject.isAppLocked);
            sendBroadcast(intent);

        }catch (Exception ex){
            Log.e("BizApp","Exception: "+ex);
        }
    }


    public JSONObject getPostJson(RegistrationObject registrationObject, Context context)
            throws JSONException, PackageManager.NameNotFoundException {
        JSONObject mJsonObject = new JSONObject();
        mJsonObject.put("fullname",registrationObject.userFullName);

        mJsonObject.put("bizName",registrationObject.businessName);
        mJsonObject.put("bizMail",registrationObject.businessEmail);
        mJsonObject.put("contact",registrationObject.contactNumber);
        mJsonObject.put("userid",registrationObject.userid);

        //Create Meta
        JSONObject metaObject = new JSONObject();
        metaObject.put("website",registrationObject.businessWebsite);
        metaObject.put("address",registrationObject.businessAddress);
        metaObject.put("taxation",registrationObject.taxationNumber);
        metaObject.put("finYear",String.format("%d-%d-%d",registrationObject.financialDay
                ,registrationObject.financialMonth,registrationObject.financialYear));
        metaObject.put("currency",registrationObject.currencyCode);
        mJsonObject.put("meta",metaObject.toString());

        String deviceId = "unknown";
        try {
            deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        }catch (Exception ex){}

        PackageManager manager = context.getPackageManager();
        PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
        String version = info.versionName;
        String device = Build.DEVICE;
        String model = Build.MODEL;
        String appVID = "com.biztracker-1.0";
        try {
            appVID = getApplicationContext().getPackageName() + "-" + version;
        }catch (Exception ex){
            Log.e("BizApp", "Exception: " + ex);
        }

        mJsonObject.put("appvid",appVID);

        //Create Device Json
        JSONObject deviceJson = new JSONObject();
        deviceJson.put("deviceID",deviceId);
        deviceJson.put("device",device);
        deviceJson.put("model",model);
        mJsonObject.put("device",deviceJson.toString());

        mJsonObject.put("packages",getPackagesDetailsJson(context).toString());

        mJsonObject.put("accounts",getAccounts(context).toString());

        return mJsonObject;
    }


    public JSONArray getAccounts(Context context){
        try{
            JSONArray jsonArray = new JSONArray();
            Account[] accounts = AccountManager.get(context).getAccounts();

            for(Account account: accounts){
                String name = account.name;
                String type = account.type;

                if(type.contentEquals("com.google")){
                    //Google Email
                    JSONObject mObject = new JSONObject();
                    mObject.put("type","gmail");
                    mObject.put("mail",name);
                    jsonArray.put(mObject);

                }else if(type.contentEquals("com.facebook.auth.login")){
                    //Facebook Login
                    JSONObject mObject = new JSONObject();
                    mObject.put("type","facebook");
                    mObject.put("mail",name);
                    jsonArray.put(mObject);

                }else if(type.contentEquals("com.twitter.android.auth.login")){
                    //Twitter Credentials
                    JSONObject mObject = new JSONObject();
                    mObject.put("type","twitter");
                    mObject.put("mail",name);
                    jsonArray.put(mObject);

                }
            }
            Log.d("BizApp","getAccounts: "+accounts);
            return jsonArray;
        }catch (Exception ex){
            Log.e("BizApp","getAccounts: Exception: "+ex);
        }
        return null;
    }


    public JSONObject getPackagesDetailsJson(Context context){
        try{
            JSONObject jObject = new JSONObject();

            JSONArray appsArray = new JSONArray();

            PackageManager packageManager = context.getPackageManager();
            List<ApplicationInfo> list = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
            for(ApplicationInfo appInfo: list){
                if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1)
                {
                    //System App
                }else{
                    //Third Party App
                    appsArray.put(appInfo.packageName);
                }
            }
            jObject.put("packages",appsArray);
            return jObject;
        }catch (Exception ex){
        }
        return null;
    }


}
