package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;

import com.biztracker.database.DatabaseModel;
import com.biztracker.database.DatabaseTalker;

import java.util.concurrent.Executor;

/**
 * Created by Karan on 26/07/15.
 */
public class SlaveEntityType extends DatabaseObject {

    public int id;
    public String nature;

    public SlaveEntityType(Cursor c) {
        this.id = c.getInt(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_ACCOUNT_TYPES+".ID"));
        this.nature = c.getString(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_ACCOUNT_TYPES + ".NATURE"));
        this.name = c.getString(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_ACCOUNT_TYPES + ".NAME"));
    }

    public SlaveEntityType(){
        id = -1;
    }

    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public ContentValues prepareContentValues() {
        return null;
    }
}
