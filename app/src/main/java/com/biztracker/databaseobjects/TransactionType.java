package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;

import com.biztracker.database.DatabaseTalker;

import java.util.concurrent.Executor;

/**
 * Created by karan on 17/04/16.
 */
public class TransactionType extends DatabaseObject {

    public static final String TRANSACTION_TYPE_SALES = "Sale";
    public static final String TRANSACTION_TYPE_PURCHASE = "Purchase";
    public static final String TRANSACTION_TYPE_RECEIPT = "Receipt";
    public static final String TRANSACTION_TYPE_PAYMENT = "Payment";

    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public ContentValues prepareContentValues() {
        return null;
    }

    public TransactionType(Cursor c){
        id = c.getString(c.getColumnIndex("TRANSACTION_TYPES_ID"));
        name = c.getString(c.getColumnIndex("TRANSACTION_TYPES_NAME"));
    }
}
