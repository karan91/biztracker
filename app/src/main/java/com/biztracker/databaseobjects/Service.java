package com.biztracker.databaseobjects;

import android.content.ContentValues;

import com.biztracker.database.DatabaseTalker;

import java.util.concurrent.Executor;

/**
 * Created by Karan on 16/07/15.
 */
public class Service extends TransactionalEntity {
    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public ContentValues prepareContentValues() {
        return null;
    }
}
