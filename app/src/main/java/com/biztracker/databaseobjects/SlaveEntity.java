package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import com.biztracker.database.DatabaseModel;
import com.biztracker.database.DatabaseTalker;

import java.math.BigDecimal;
import java.text.AttributedString;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.Executor;

/**
 * Created by Karan on 17/07/15.
 */
public class SlaveEntity extends DatabaseObject {

    public BigDecimal value, default_value;
    public int nature;
    public int calculatedAs;
    public List<SlaveEntity> parentList;
    public List<Product> productList;
    public static final int CALCULATED_AS_PERCENT = 1;
    public static final int CALCULATED_AS_ABSOLUTE = 2;
    public static final int NATURE_ADDITIVE = 0;
    public static final int NATURE_SUBTRACTIVE = 1;

    public SlaveEntity(){
        this.id = "-1";
    }
    public static SlaveEntity getForInventoryMode(Cursor c){
        SlaveEntity s = new SlaveEntity();
        s.id = c.getString(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_ACCOUNTS + "_ID"));
        s.name = c.getString(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_ACCOUNTS + "_NAME"));
        s.nature = c.getInt(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_ACCOUNTS+ "_NATURE"));
        s.calculatedAs = c.getInt(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_ACCOUNTS+"_CALCULATED_AS"));
        s.default_value = new BigDecimal(c.getDouble(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_ACCOUNTS+"_DEFAULT_VALUE")));
        return s;
    }

    public static SlaveEntity getForTransactionalMode(Cursor c, List<Product> products, List<SlaveEntity> parentList){
        HashMap<String, Product> productHashMap = new HashMap<>();
        for(Product p : products){
            productHashMap.put(p.id, p);
        }
        SlaveEntity slaveEntity = SlaveEntity.getForInventoryMode(c);
        slaveEntity.productList = new ArrayList<>();
        slaveEntity.value = new BigDecimal(c.getDouble(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_TRANSACTION_DETAILS+"_VALUE")));
        String productIdsString = c.getString(c.getColumnIndex(DatabaseModel.TABLE_SLAVE_TRANSACTION_DETAILS+"_PRODUCT_IDS"));
        if(productIdsString != null){
            String[] productIds = (TextUtils.split(productIdsString, ","));
            for(String productId : productIds){
                Product product = productHashMap.get(productId);
                slaveEntity.productList.add(product);
            }
        }
        slaveEntity.parentList = parentList;
        return slaveEntity;
    }

    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {
        DatabaseTalker.getInstance().insertSlaveEntity(this, callBack, processId, executor);
    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    private BigDecimal getAbsoluteInvoiceValue(){
        BigDecimal productsValue = new BigDecimal(0);
        for(Product p : productList){
            BigDecimal productValue = p.getInvoiceValue();
            for(SlaveEntity slaveEntity : parentList){
                if(slaveEntity == this){
                    break;
                }
                if(slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_PERCENT){
                    if(slaveEntity.productList.contains(p)){
                        if(slaveEntity.nature == NATURE_ADDITIVE){
                            productValue = productValue.add(productValue.multiply(slaveEntity.getPercentDecimalValue()));
                        }
                        else if(slaveEntity.nature == NATURE_SUBTRACTIVE){
                            productValue = productValue.subtract(productValue.multiply(slaveEntity.getPercentDecimalValue()));
                        }
                    }
                }
            }
            productsValue = productsValue.add(productValue);
        }
        BigDecimal result = productsValue.multiply(this.getPercentDecimalValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
        return result;
    }

    public BigDecimal getInvoiceValueOfProduct(Product p){
        BigDecimal productValue = p.getInvoiceValue();
        for(SlaveEntity slaveEntity : parentList){
            if(slaveEntity == this){
                break;
            }
            if(slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_PERCENT){
                if(slaveEntity.productList.contains(p)){
                    if(slaveEntity.nature == NATURE_ADDITIVE){
                        productValue = productValue.add(productValue.multiply(slaveEntity.getPercentDecimalValue()));
                    }
                    else if(slaveEntity.nature == NATURE_SUBTRACTIVE){
                        productValue = productValue.subtract(productValue.multiply(slaveEntity.getPercentDecimalValue()));
                    }
                }
            }
        }
        productValue = productValue.setScale(2, BigDecimal.ROUND_FLOOR);
        return productValue;
    }

    public Spannable getFormattedTentativeValueForProductValue(BigDecimal v){
        v = v.multiply(getPercentDecimalValue());
        v = v.abs();
        NumberFormat format = NumberFormat.getInstance();
        String valueString = v.toString();
        if(valueString.contains(".") && !valueString.endsWith(".00")){
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
        }
        else{
            format.setMaximumFractionDigits(0);
        }
        format.setCurrency(Company.company.currency);
        String formattedValue = format.format(v);
        formattedValue = format.getCurrency().getSymbol() + formattedValue;
        Spannable spannable = new SpannableString(formattedValue);

        int color = Color.parseColor("#FFFFFF");
        if(this.nature == NATURE_SUBTRACTIVE){
            color = Color.parseColor("#E63C5A");
        }
        else if(this.nature == NATURE_ADDITIVE){
            color = Color.parseColor("#6CC483");
        }
        spannable.setSpan(new ForegroundColorSpan(color), 0, formattedValue.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public BigDecimal getTentativeValueForProductValue(BigDecimal v){
        v = v.multiply(getPercentDecimalValue());
        return v;
    }

    public BigDecimal getInvoiceValue(){
        BigDecimal absValue = null;
        if(calculatedAs == CALCULATED_AS_PERCENT) {
            if(nature == NATURE_ADDITIVE){
                absValue = getAbsoluteInvoiceValue();
            }
            else if(nature == NATURE_SUBTRACTIVE){
                absValue = getAbsoluteInvoiceValue().negate();
            }
            return absValue.setScale(2);
        }
        else if(calculatedAs == CALCULATED_AS_ABSOLUTE){
            absValue = value;
            if(nature == NATURE_ADDITIVE){
                return absValue.setScale(2);
            }
            else if(nature == NATURE_SUBTRACTIVE){
                return absValue.negate().setScale(2);
            }
        }
        return null;
    }

    @Override
    public ContentValues prepareContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseModel.TABLE_SLAVE_ACCOUNTS+"_name", name);
        if(!id.equals("-1"))    cv.put(DatabaseModel.TABLE_SLAVE_ACCOUNTS+"_id", id);
        else{
            cv.put(DatabaseModel.TABLE_SLAVE_ACCOUNTS+"_id", generateUniqueId());
        }
        cv.put(DatabaseModel.TABLE_SLAVE_ACCOUNTS+"_nature", nature);
        cv.put(DatabaseModel.TABLE_SLAVE_ACCOUNTS+"_calculated_as", calculatedAs);
        cv.put(DatabaseModel.TABLE_SLAVE_ACCOUNTS+"_default_value", default_value.doubleValue());
        return cv;
    }

    public ContentValues prepareContentValuesForTransaction(String transactionId){
        ContentValues cv = new ContentValues();
        cv.put("FOREIGN_SLAVE_ACCOUNTS_ID", this.id);
        cv.put("FOREIGN_TRANSACTION_LIST_ID", transactionId);
        cv.put("SLAVE_TRANSACTION_DETAILS_VALUE", this.getValue().toString());
        if(productList == null || productList.isEmpty()){
            cv.put("SLAVE_TRANSACTION_DETAILS_PRODUCT_IDS", "");
        }
        else {
            List<String> productIds = new ArrayList<>(this.productList.size());
            for (Product p : productList) {
                productIds.add(p.id);
            }
            cv.put("SLAVE_TRANSACTION_DETAILS_PRODUCT_IDS", TextUtils.join(",", productIds));
        }
        return cv;
    }

    public BigDecimal getValue(){
        if(value == null)   return default_value;
        return value;
    }

    public Spannable getFormattedValue(){
        BigDecimal v = getValue();
        v = v.abs();
        NumberFormat format = NumberFormat.getInstance();
        String valueString = v.toString();
        if(valueString.contains(".") && !valueString.endsWith(".00")){
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
        }
        else{
            format.setMaximumFractionDigits(0);
        }
        format.setCurrency(Company.company.currency);
        String formattedValue = format.format(v);
        formattedValue = format.getCurrency().getSymbol() + formattedValue;
        Spannable spannable = new SpannableString(formattedValue);

        int color = Color.parseColor("#FFFFFF");
        if(this.nature == NATURE_SUBTRACTIVE){
            color = Color.parseColor("#E63C5A");
        }
        else if(this.nature == NATURE_ADDITIVE){
            color = Color.parseColor("#6CC483");
        }
        spannable.setSpan(new ForegroundColorSpan(color), 0, formattedValue.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public void setPercentageValue(BigDecimal percentValue){
        if(this.calculatedAs == CALCULATED_AS_PERCENT) {
            this.value = percentValue;
        }
    }

    public Spannable getFormattedInvoiceValue(){

        BigDecimal invoiceValue = this.getInvoiceValue();
        invoiceValue = invoiceValue.abs();
        NumberFormat format = NumberFormat.getInstance(Locale.ROOT);
        if(!invoiceValue.toString().endsWith(".00")){
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
        }
        else{
            format.setMaximumFractionDigits(0);
        }
        format.setCurrency(Company.company.currency);
        String formattedValue = format.format(invoiceValue);
        formattedValue = format.getCurrency().getSymbol() + formattedValue;
        Spannable spannable = new SpannableString(formattedValue);
        int color = Color.parseColor("#FFFFFF");
        if(this.nature == NATURE_SUBTRACTIVE){
            color = Color.parseColor("#E63C5A");
        }
        else if(this.nature == NATURE_ADDITIVE){
            color = Color.parseColor("#6CC483");
        }
        spannable.setSpan(new ForegroundColorSpan(color), 0, formattedValue.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public String getFormattedPlainInvoiceValue(){
        BigDecimal invoiceValue = this.getInvoiceValue();
        invoiceValue = invoiceValue.abs();
        NumberFormat format = NumberFormat.getInstance(Locale.ROOT);
        if(!invoiceValue.toString().endsWith(".00")){
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
        }
        else{
            format.setMaximumFractionDigits(0);
        }
        format.setCurrency(Company.company.currency);
        String formattedValue = format.format(invoiceValue);
        formattedValue = format.getCurrency().getSymbol() + formattedValue;
        return formattedValue;
    }

    public BigDecimal getPercentDecimalValue() {
        if(this.calculatedAs == CALCULATED_AS_PERCENT){
            return getValue().divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        }
        else{
            throw new RuntimeException("Cannot give percent value for absolute valued slave entities");
        }
    }
}
