package com.biztracker.databaseobjects;

import android.content.ContentValues;

import com.biztracker.database.DatabaseTalker;

import java.util.concurrent.Executor;

/**
 * Created by shubham on 09/07/15.
 */
public abstract class Discount extends DatabaseObject {

    public final static int DISCOUNT_TYPE_PERCENTAGE = 1;
    public final static int DISCOUNT_TYPE_FIXED = 2;

    protected int TYPE;
    public float maximumDiscountLimit = 0;
    public float minimumBillAmount = 0;
    protected float value;


    public int getDiscountType(){
        return TYPE;
    }
    public abstract float getDiscountOnValue(float amount);


    public class FixedDiscount extends Discount {

        public FixedDiscount(){
            this.TYPE = DISCOUNT_TYPE_FIXED;
        }

        public float getDiscountValue(){
            return value;
        }

        @Override
        public float getDiscountOnValue(float amount) {
            if(amount > minimumBillAmount){
                return value;
            }
            return 0;
        }

        @Override
        public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

        }

        @Override
        public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

        }

        @Override
        public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

        }

        @Override
        public ContentValues prepareContentValues() {
            return null;
        }
    }

    public class PercentageDiscount extends Discount {

        public PercentageDiscount(){
            this.TYPE = DISCOUNT_TYPE_PERCENTAGE;
        }

        public float getPercentage(){
            return value;
        }

        @Override
        public float getDiscountOnValue(float amount) {
            if(amount > minimumBillAmount){
                float discount = (amount * value) / 100;
                if(maximumDiscountLimit > 0){
                    discount = (discount > maximumDiscountLimit)?maximumDiscountLimit:discount;
                }
                return discount;
            }
            return 0;
        }

        @Override
        public int getDiscountType() {
            return DISCOUNT_TYPE_PERCENTAGE;
        }

        @Override
        public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

        }

        @Override
        public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

        }

        @Override
        public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

        }

        @Override
        public ContentValues prepareContentValues() {
            return null;
        }
    }


}
