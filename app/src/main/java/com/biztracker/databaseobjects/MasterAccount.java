package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.location.Address;
import android.os.Bundle;
import android.provider.ContactsContract;

import com.biztracker.database.DatabaseModel;
import com.biztracker.database.DatabaseTalker;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.UUID;
import java.util.concurrent.Executor;

/**
 * Created by shubham on 09/07/15.
 */
public class MasterAccount extends DatabaseObject {

    public MasterAccountType accountType;
    public String email;
    public String phone;
    public String taxationNumber;
    public String address;
    public BigDecimal openingBalance;
    public BigDecimal closingBalance;
    private String tableName = DatabaseModel.TABLE_MASTER_ACCOUNTS;

    public MasterAccount(Cursor c){

        id = c.getString(c.getColumnIndex(tableName+"_ID"));
        name = c.getString(c.getColumnIndex(tableName+"_NAME"));
        email = c.getString(c.getColumnIndex(tableName + "_EMAIL"));
        phone = c.getString(c.getColumnIndex(tableName + "_PHONE"));
        taxationNumber = c.getString(c.getColumnIndex(tableName + "_TAXATION_NUMBER"));
        address = c.getString(c.getColumnIndex(tableName + "_ADDRESS"));
        accountType = new MasterAccountType(c);
        openingBalance = new BigDecimal(c.getDouble(c.getColumnIndex("MASTER_ACCOUNTS_OPENING_BALANCE")));
        closingBalance = new BigDecimal(c.getDouble(c.getColumnIndex("MASTER_ACCOUNTS_CLOSING_BALANCE")));
    }

    public MasterAccount() {
        id = "-1";
        this.name = "";
    }

    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {
            DatabaseTalker.getInstance().insertOrUpdateMasterAccount(this, callBack, processId, executor);
    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    public void refreshObjectFromDatabase(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor){
        DatabaseTalker.getInstance().refreshMasterAccountObject(this, callBack, processId, executor);
    }

    public static MasterAccount getMasterAccountFromId(String id){
        MasterAccount m = DatabaseTalker.getInstance().getMasterAccountForId(id);
        return m;
    }

    @Override
    public ContentValues prepareContentValues() {

        ContentValues cv = new ContentValues();
        cv.put(tableName + "_NAME", name);
        cv.put(tableName + "_EMAIL", email);
        cv.put(tableName + "_PHONE", phone);
        cv.put(tableName + "_ADDRESS", address);
        cv.put(tableName + "_OPENING_BALANCE", openingBalance.toString());
        cv.put(tableName + "_CLOSING_BALANCE", closingBalance.toString());
        cv.put(tableName + "_TAXATION_NUMBER", taxationNumber);
        cv.put("FOREIGN_MASTER_ACCOUNT_TYPES_ID", accountType.id);
        if(!id.equals("-1"))    cv.put(tableName + "_ID", id);
        else{
            cv.put(tableName+"_ID", generateUniqueId());
        }
        return cv;
    }

    public String getFormattedClosingBalance(){


        BigDecimal absValue = closingBalance.abs();

        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        format.setCurrency(Company.company.currency);
        String formattedValue = format.format(absValue);

        if(formattedValue.endsWith("00")){
            formattedValue = formattedValue.replace(".00", "");
        }

        if(closingBalance.compareTo(BigDecimal.ZERO) >= 0){
            return format.getCurrency().getSymbol() + " " +formattedValue;
        }
        else{
            return "- " + format.getCurrency().getSymbol() + " " +formattedValue;
        }
    }

    public void setOpeningBalance(BigDecimal openingBalance){
        if(this.openingBalance == null || this.openingBalance == new BigDecimal(0)){
            this.openingBalance = openingBalance;
            this.closingBalance = openingBalance;
        }
        else{
            BigDecimal delta = this.closingBalance.subtract(this.openingBalance);
            this.openingBalance= openingBalance;
            this.closingBalance= this.openingBalance.add(delta);
        }
    }

}
