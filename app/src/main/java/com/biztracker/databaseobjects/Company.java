package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;

import com.biztracker.BizTrackerApp;
import com.biztracker.common.Utilities;
import com.biztracker.database.DatabaseTalker;

import java.util.Currency;
import java.util.concurrent.Executor;

/**
 * Created by karan on 14/04/16.
 */
public class Company extends DatabaseObject {

    public String personsName, businessName, startingDate, address, phone, taxationNumber, emailAddress, businessWebsite;
    public Currency currency;
    public static Company company;

    public static void setCompanyFromBusinessName(String businessName){
        if(Utilities.doesDatabaseExist(BizTrackerApp.getAppContext(),businessName + ".bt")){
            Company company = new Company();
            DatabaseTalker.getInstance().loadCompany(businessName, company);
            Company.company = company;
        }
    }
    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {
        DatabaseTalker.getInstance().createNewCompany(this, callBack, processId, executor);
    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {
        DatabaseTalker.getInstance().updateCompany(this, callBack, processId, executor);
    }

    @Override
    public ContentValues prepareContentValues() {

        ContentValues cv = new ContentValues();
        cv.put("COMPANY_DETAILS_NAME", personsName);
        cv.put("COMPANY_DETAILS_BUSINESS_NAME", businessName);
        cv.put("COMPANY_DETAILS_STARTING_FROM", startingDate);
        cv.put("COMPANY_DETAILS_ADDRESS", address);
        cv.put("COMPANY_DETAILS_PHONE", phone);
        cv.put("COMPANY_DETAILS_TAXATION_NUMBER", taxationNumber);
        cv.put("COMPANY_DETAILS_EMAIL", emailAddress);
        cv.put("COMPANY_DETAILS_CURRENCY", currency.getCurrencyCode());

        return cv;
    }

    public void loadFromCursor(Cursor cursor){
        this.id = String.valueOf(cursor.getInt(cursor.getColumnIndex("COMPANY_DETAILS_ID")));
        this.personsName = cursor.getString(cursor.getColumnIndex("COMPANY_DETAILS_NAME"));
        this.businessName = cursor.getString(cursor.getColumnIndex("COMPANY_DETAILS_BUSINESS_NAME"));
        this.startingDate = cursor.getString(cursor.getColumnIndex("COMPANY_DETAILS_STARTING_FROM"));
        this.address = cursor.getString(cursor.getColumnIndex("COMPANY_DETAILS_ADDRESS"));
        this.phone = cursor.getString(cursor.getColumnIndex("COMPANY_DETAILS_PHONE"));
        this.taxationNumber = cursor.getString(cursor.getColumnIndex("COMPANY_DETAILS_TAXATION_NUMBER"));
        this.emailAddress = cursor.getString(cursor.getColumnIndex("COMPANY_DETAILS_EMAIL"));
        String currencyCode = cursor.getString(cursor.getColumnIndex("COMPANY_DETAILS_CURRENCY"));
        this.currency = Currency.getInstance(currencyCode);
    }
}
