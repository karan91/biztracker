package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;

import com.biztracker.database.DatabaseModel;
import com.biztracker.database.DatabaseTalker;

import java.util.UUID;
import java.util.concurrent.Executor;

/**
 * Created by Karan on 22/07/15.
 */
public class MasterAccountType extends DatabaseObject {

    public static String NATURE_CREDIT = "cr";
    public static String NATURE_DEBIT = "dr";
    public String defaultNature;
    public int billable;
    private static String tableName = DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES;

    public MasterAccountType(Cursor c){
        name = c.getString(c.getColumnIndex(tableName+"_NAME"));
        defaultNature = c.getString(c.getColumnIndex(tableName+"_DEFAULT_NATURE"));
        billable = c.getInt(c.getColumnIndex(tableName+"_BILLABLE"));
        id = c.getString(c.getColumnIndex(tableName+"_ID"));
    }

    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {
        DatabaseTalker.getInstance().insertOrUpdateMasterAccountType(this, callBack, processId, executor);
    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    public MasterAccountType(){
        id = "-1";
    }

    @Override
    public ContentValues prepareContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(tableName+"_NAME", name);
        cv.put(tableName+"_DEFAULT_NATURE", defaultNature);
        cv.put(tableName+"_BILLABLE", billable);
        if(!id.equals("-1"))    cv.put(tableName+"_ID", id);
        else{
            cv.put(tableName+"_ID", generateUniqueId());
        }
        return cv;
    }
}
