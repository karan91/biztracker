package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;

import com.biztracker.database.DatabaseModel;
import com.biztracker.database.DatabaseTalker;

import java.util.UUID;
import java.util.concurrent.Executor;

/**
 * Created by shubham on 09/07/15.
 */
public class Unit extends DatabaseObject {
    public String displayName;
    public static String tableName = DatabaseModel.TABLE_UNITS;

    public Unit(){
        this.id = "-1";
    }
    public Unit(Cursor cursor){
        this.name = cursor.getString(cursor.getColumnIndex(tableName+"_NAME"));
        this.displayName = cursor.getString(cursor.getColumnIndex(tableName+"_BILLED_NAME"));
        this.id = cursor.getString(cursor.getColumnIndex(tableName+"_ID"));
    }

    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

        DatabaseTalker.getInstance().insertOrUpdateUnit(this, callBack, processId, executor);
    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public ContentValues prepareContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(tableName+"_NAME", name);
        cv.put(tableName+"_BILLED_NAME", displayName);
        if(!id.equals("-1"))    cv.put(tableName+"_ID", id);
        else{
            cv.put(tableName+"_ID", generateUniqueId());
        }
        return cv;
    }


}
