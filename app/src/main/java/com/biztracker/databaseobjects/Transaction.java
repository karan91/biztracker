package com.biztracker.databaseobjects;


import android.content.ContentValues;
import android.database.Cursor;

import com.biztracker.database.DatabaseModel;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by shubham on 09/07/15.
 */
public abstract class Transaction extends DatabaseObject {

    public final static int TYPE_SALES = 1;
    public final static int TYPE_PURCHASE = 2;
    public final static int TYPE_SALES_RETURN = 3;
    public final static int TYPE_PURCHASE_RETURN = 4;
    public final static int TYPE_RECEIPT = 5;
    public final static int TYPE_PAYMENT = 6;
    public final static int TYPE_JOURNAL = 7;
    public String tableName = DatabaseModel.TABLE_TRANSACTION_LIST;
    public abstract void setType(int type);
    protected int type;
    public int getType(){
        return type;
    }


    public MasterAccount debitAccount;
    public MasterAccount creditAccount;
    public String transactionNumber;
    public Date date;
    public String note;
    public BigDecimal originalValue;
    public String getPrettyDate(){
        if(date != null){
            SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
            return formatter.format(date);
        }
        return "";
    }
    public Transaction(){
        id = "-1";
    }

    public void setDebitAccount(MasterAccount debitAccount) {
        this.debitAccount = debitAccount;
    }

    public void setCreditAccount(MasterAccount creditAccount) {
        this.creditAccount = creditAccount;
    }

    public Transaction(Cursor c){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        try {
            this.date = format.parse(String.valueOf(c.getInt(c.getColumnIndex("TRANSACTION_LIST_DATE"))));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.id = c.getString(c.getColumnIndex("TRANSACTION_LIST_ID"));
        this.setType(c.getInt(c.getColumnIndex("FOREIGN_TRANSACTION_TYPES_ID")));
        this.transactionNumber = c.getString(c.getColumnIndex("TRANSACTION_LIST_TRANSACTION_NUMBER"));
        this.note = c.getString(c.getColumnIndex("TRANSACTION_LIST_NARRATION"));
    }

    @Override
    public ContentValues prepareContentValues() {

        ContentValues cv = new ContentValues();
        cv.put("FOREIGN_TRANSACTION_TYPES_ID", type);
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        cv.put("TRANSACTION_LIST_DATE", format.format(date));
        cv.put("FOREIGN_DEBIT_MASTER_ACCOUNTS_ID", debitAccount.id);
        cv.put("FOREIGN_CREDIT_MASTER_ACCOUNTS_ID", creditAccount.id);
        cv.put("TRANSACTION_LIST_NARRATION", note);
        cv.put("TRANSACTION_LIST_TRANSACTION_NUMBER", transactionNumber);
        if(!id.equals("-1")){
            cv.put(tableName + "_ID", id);
        }
        else{
            String uniqueId = generateUniqueId();
            this.id = uniqueId;
            cv.put(tableName+"_ID", uniqueId);
        }
        return cv;
    }

    public abstract MasterAccount getActiveAccount();

    public abstract String getTotalFormattedValue();

    public String getTypeName(){
        switch (this.type){
            case TYPE_PAYMENT:  return "Payment";
            case TYPE_RECEIPT:  return "Receipt";
            case TYPE_SALES:  return "Sale";
            case TYPE_PURCHASE:  return "Purchase";
            default: return "";
        }
    }

    public abstract BigDecimal getNetValue();
}
