package com.biztracker.databaseobjects;

import android.content.ContentValues;

import com.biztracker.database.DatabaseTalker;

import java.util.List;
import java.util.concurrent.Executor;

/**
 * Created by shubham on 09/07/15.
 */
public abstract class TransactionalEntity extends DatabaseObject {

    public String description;
    public Unit unit;
}
