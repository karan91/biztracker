package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.biztracker.database.DatabaseModel;
import com.biztracker.database.DatabaseTalker;
//import com.firebase.client.Firebase;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * Created by Karan on 17/07/15.
 */
public class InvoicedTransaction extends Transaction {
    //PRODUCT-NAME  QUANTITY PRICE DISCOUNT
    public ArrayList<Product> productList;
    public List<Service> serviceList;
    public List<SlaveEntity> slaveEntities;

    public InvoicedTransaction(Cursor c){
        super(c);
    }

    public InvoicedTransaction(String transactionId) {
        this.id = transactionId;
    }

    @Override
    public void setType(int type){
        this.type = type;
    }

    public MasterAccount getActiveAccount(){
        if(type == TYPE_SALES){
            return this.debitAccount;
        }
        else{
            return this.creditAccount;
        }
    }

    public boolean isLoaded(){
        if(productList == null) return false;
        return true;
    }

    public List<ContentValues> prepareProductDetailsContentValues(){
        List<ContentValues> list = new ArrayList<>();
       for(Product p : productList){
           ContentValues cv = new ContentValues();
           cv.put("FOREIGN_MASTER_TRANSACTION_LIST_ID",id);
           cv.put("FOREIGN_TRANSACTIONAL_ENTITIES_ID",p.id);
           cv.put("MASTER_TRANSACTION_DETAILS_QUANTITY",p.entityQty.toString());
           cv.put("MASTER_TRANSACTION_DETAILS_PRICE",p.entityValue.toString());
           list.add(cv);
       }
        return list;
    }

    public List<ContentValues> prepareSlaveEntitiesContentValues(){
        List<ContentValues> list = new ArrayList<>();
        for(SlaveEntity slaveEntity : slaveEntities){
            ContentValues cv = new ContentValues();
            cv.put("FOREIGN_SLAVE_ACCOUNTS_ID", slaveEntity.id);
            cv.put("FOREIGN_TRANSACTION_LIST_ID", this.id);
            cv.put("SLAVE_TRANSACTION_DETAILS_VALUE", slaveEntity.getValue().doubleValue());
            if(slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_PERCENT){
                List<String> productIds = new ArrayList<>();
                for(Product p : slaveEntity.productList){
                    productIds.add(String.valueOf(p.id));
                }
                String result = TextUtils.join(",",productIds);
                cv.put("SLAVE_TRANSACTION_DETAILS_PRODUCT_IDS", result);
            }
            list.add(cv);
        }
        return list;
    }

    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {
        DatabaseTalker.getInstance().insertInvoiceTransaction(this, callBack, processId, executor);
        //this.syncToFirebase();
    }

    private String getSqlFormattedDate(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        return formatter.format(this.date);
    }

    /*private void syncToFirebase(){
        Firebase invoiceRef = new Firebase("https://vivid-inferno-1712.firebaseio.com/").child("invoices").child(String.valueOf(this.type)).child(String.valueOf(this.id));
        invoiceRef.child("date").setValue(this.getSqlFormattedDate());
        invoiceRef.child("debit_account").setValue(this.debitAccount.id);
        invoiceRef.child("credit_account").setValue(this.creditAccount.id);
        Firebase productsRef = invoiceRef.child("products");
        Firebase slaveEntitiesRef = invoiceRef.child("slave_entities");
        for(Product p : this.productList){
            Firebase productRef = productsRef.child(String.valueOf(p.id));
            productRef.child("price").setValue(p.entityValue.toString());
            productRef.child("qty").setValue(p.entityQty.toString());
        }
        for(SlaveEntity slaveEntity : this.slaveEntities){
            Firebase slaveEntityRef = slaveEntitiesRef.child(String.valueOf(slaveEntity.id));
            slaveEntityRef.child("value").setValue(slaveEntity.getValue().toString());
            Firebase slaveProductsRef = slaveEntityRef.child("products");
            List<String> productIds = new ArrayList<>();
            for(Product p : (List<Product>)safe(slaveEntity.productList)){
                productIds.add(String.valueOf(p.id));
            }
            slaveProductsRef.setValue(productIds);
        }
    }*/

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {
        DatabaseTalker.getInstance().deleteInvoiceTransaction(this, callBack, processId, executor);
    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {
        DatabaseTalker.getInstance().updateInvoiceTransaction(this, callBack, processId, executor);
    }

    public BigDecimal getTotalValueOfProducts(){

        BigDecimal result = new BigDecimal(0.0);
        for(Product p : this.productList){
            BigDecimal total = p.entityQty.multiply(p.entityValue);
            result = result.add(total);
        }
        return result;
    }

    public BigDecimal getTotalValueOfInvoice(){
        BigDecimal result = getTotalValueOfProducts();
        for(SlaveEntity slave : slaveEntities){
            BigDecimal slaveValue = slave.getInvoiceValue();
            result = result.add(slaveValue);
        }
        return result;
    }

    public BigDecimal getTotalValueOfInvoiceExcludingSlaveEntities(){
        BigDecimal result = new BigDecimal(0);
        for(Product p : productList){
            result = result.add(p.getInvoiceValue());
        }
        return result;
    }

    public String getFormattedTotalValueOfInvoiceExcludingSlaveEntities(){
        BigDecimal result = getTotalValueOfInvoiceExcludingSlaveEntities();
        NumberFormat format = NumberFormat.getInstance();
        String resultString = result.toString();
        if(resultString.contains(".") && !resultString.endsWith(".00")){
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
        }
        else{
            format.setMaximumFractionDigits(0);
        }
        format.setCurrency(Company.company.currency);
        String formattedValue = format.format(result);
        return format.getCurrency().getSymbol() + " " +formattedValue;
    }

    @Override
    public String getTotalFormattedValue(){
        BigDecimal result = getTotalValueOfInvoice();

        NumberFormat format = NumberFormat.getInstance();
        String resultString = result.toString();
        if(resultString.contains(".") && !resultString.endsWith(".00")){
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
        }
        else{
            format.setMaximumFractionDigits(0);
        }
        format.setCurrency(Company.company.currency);
        String formattedValue = format.format(result);
        return format.getCurrency().getSymbol() + " " +formattedValue;
    }

    @Override
    public BigDecimal getNetValue() {
        return getTotalValueOfInvoice();
    }

    public static InvoicedTransaction getNewInvoicedTransaction(int type){

        InvoicedTransaction transaction = new InvoicedTransaction();
        transaction.productList = new ArrayList<>();
        transaction.serviceList = new ArrayList<>();
        transaction.slaveEntities = new ArrayList<>();
        transaction.setType(type);
        transaction.date = new Date();
        if(type == Transaction.TYPE_PURCHASE){
            transaction.setCreditAccount(null);
            transaction.setDebitAccount(MasterAccount.getMasterAccountFromId("5"));
        }
        else if(type == Transaction.TYPE_SALES){
            transaction.setDebitAccount(null);
            transaction.setCreditAccount(MasterAccount.getMasterAccountFromId("3"));
        }
        else    return null;
        transaction.id = "-1";
        transaction.transactionNumber = String.valueOf(DatabaseTalker.getInstance().getCountForTransaction(transaction) + 1);
        return transaction;
    }

    public void removeProduct(Product product){
        this.productList.remove(product);
        for(SlaveEntity slaveEntity : slaveEntities){
            slaveEntity.productList.remove(product);
        }
    }

    public static List safe( List other ) {
        return other == null ? Collections.EMPTY_LIST : other;
    }

    public InvoicedTransaction(){
        this.id = "-1";
    }
}
