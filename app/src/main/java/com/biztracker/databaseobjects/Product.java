package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.biztracker.database.DatabaseModel;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.fragments.history.ProductHistory.ProductHistoryBean;

import java.lang.ref.SoftReference;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.Executor;

/**
 * Created by Karan on 16/07/15.
 */
public class Product extends TransactionalEntity implements ProductHistoryBean.ProductHistoryDelegate {

    private static final int DB_TRANSACTION_FAILED = -1;
    public BigDecimal openingUnits;
    public BigDecimal closingUnits;
    public BigDecimal purchasePrice;
    public BigDecimal sellingPrice;
    public BigDecimal entityValue;
    public BigDecimal entityQty;
    public String barCodeValue;
    public List<ProductHistoryBean> productHistoryList;
    public static final int PRODUCT_UPDATED = 1;
    public static final int PRODUCT_CREATED = 2;
    public static String tableName = DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES;
    public Product(String name, BigDecimal openingQty, Unit unit, BigDecimal sellingPrice, BigDecimal purchasePrice){
        this.name = name;
        this.openingUnits = openingQty;
        this.unit = unit;
        this.purchasePrice = purchasePrice;
        this.sellingPrice = sellingPrice;
        id = "-1";
    }

    public Product(){
        id = "-1";
    }

    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {
        DatabaseTalker.getInstance().insertOrUpdateProduct(this, callBack, processId, executor);
    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    public Product getForCompactInvoiceMode(Cursor c){

        Product p = new Product();


        return p;
    }


    public static Product getForInventoryMode(Cursor c){

        Product p = new Product();
        p.id = c.getString(c.getColumnIndex(tableName + "_ID"));
        p.name = c.getString(c.getColumnIndex(tableName + "_ITEM_NAME"));
        p.closingUnits = new BigDecimal(c.getDouble(c.getColumnIndex(tableName+"_CLOSING_QTY")));
        p.openingUnits = new BigDecimal(c.getDouble(c.getColumnIndex(tableName+"_OPENING_QTY")));
        p.sellingPrice = new BigDecimal(c.getDouble(c.getColumnIndex(tableName+"_SALES_PRICE_PER_UNIT")));
        p.purchasePrice = new BigDecimal(c.getDouble(c.getColumnIndex(tableName+"_PURCHASE_PRICE_PER_UNIT")));
        p.barCodeValue = c.getString(c.getColumnIndex(tableName + "_BARCODE_ID"));
        p.unit = new Unit(c);

        return p;
    }

    public static Product getForInvoiceTransactionMode(Cursor cursor){
        Product p = Product.getForInventoryMode(cursor);
        p.entityQty = new BigDecimal(cursor.getString(cursor.getColumnIndex("MASTER_TRANSACTION_DETAILS_QUANTITY")));
        p.entityValue = new BigDecimal(cursor.getString(cursor.getColumnIndex("MASTER_TRANSACTION_DETAILS_PRICE")));
        return p;
    }


    public void setTransactionalValues(BigDecimal value, BigDecimal qty){
        this.entityValue = value;
        this.entityQty = qty;
    }

    public static Product getForDetailedInvoiceMode(Cursor c){
        Product p = getForInventoryMode(c);
        p.entityValue = new BigDecimal(c.getString(c.getColumnIndex("MASTER_TRANSACTION_DETAILS_QUANTITY")));
        p.entityQty = new BigDecimal(c.getString(c.getColumnIndex("MASTER_TRANSACTION_DETAILS_PRICE")));
        return p;
    }

    public BigDecimal getInvoiceValue(){
        return entityValue.multiply(entityQty).setScale(2, RoundingMode.FLOOR);
    }

    public String getFormattedInvoiceValue(){
        BigDecimal value = getInvoiceValue();
        NumberFormat format = NumberFormat.getInstance(Locale.ROOT);
        format.setMaximumFractionDigits(2);
        format.setCurrency(Company.company.currency);
        String formattedValue = format.getCurrency().getSymbol() + format.format(value);
        return formattedValue;
    }

    public int CreateOrUpdateProduct(SQLiteDatabase database){
        if(!id.equals("-1")){
            Cursor cursor = database.rawQuery("SELECT "+tableName+"_OPENING_QTY, "+tableName+"_CLOSING_QTY FROM TRANSACTIONAL_ENTITIES WHERE "+tableName+"_ID='"+id+"'", null);
            BigDecimal originalOpening = new BigDecimal(0.0);
            BigDecimal originalClosing = new BigDecimal(0.0);
            if(cursor.moveToFirst()) {
                originalOpening = new BigDecimal(cursor.getString(cursor.getColumnIndex(tableName+"_OPENING_QTY")));
                originalClosing = new BigDecimal(cursor.getString(cursor.getColumnIndex(tableName+"_CLOSING_QTY")));
            }
            BigDecimal diffQty = originalOpening.subtract(openingUnits);
            closingUnits = originalClosing.add(diffQty);
            ContentValues cv = prepareContentValues();
            database.update(DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES, cv, tableName+"ID=?", new String[]{String.valueOf(id)});
            return PRODUCT_UPDATED;
        }
        else{
            ContentValues cv = prepareContentValues();
            if(database.insert(DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES, null, cv) != -1) return PRODUCT_CREATED;

            return DB_TRANSACTION_FAILED;
        }
    }

    public ContentValues prepareContentValues(){
        ContentValues cv = new ContentValues();
        cv.put(tableName+"_ITEM_NAME", name);
        cv.put(tableName+"_BARCODE_ID", barCodeValue);
        cv.put(tableName+"_SALES_PRICE_PER_UNIT", sellingPrice.toPlainString());
        cv.put(tableName+"_PURCHASE_PRICE_PER_UNIT", purchasePrice.toPlainString());
        cv.put(tableName+"_OPENING_QTY", openingUnits.toPlainString());
        cv.put(tableName+"_CLOSING_QTY", closingUnits.toPlainString());
        cv.put("FOREIGN_"+DatabaseModel.TABLE_UNITS+"_ID", unit.id);
        if(!this.id.equals("-1")){
            cv.put(tableName+"_ID", this.id);
        }
        else{
            cv.put(tableName+"_ID", generateUniqueId());
        }
        return cv;
    }

    public void setProductHistoryList(List<ProductHistoryBean> productHistoryList){
        this.productHistoryList.addAll(productHistoryList);
        for(ProductHistoryBean bean : this.productHistoryList){
            bean.delegate = new SoftReference<ProductHistoryBean.ProductHistoryDelegate>(this);
        }
    }

    public ContentValues prepareContentValuesForTransaction(String transactionId){
        ContentValues cv = new ContentValues();
        cv.put("FOREIGN_MASTER_TRANSACTION_LIST_ID", transactionId);
        cv.put("FOREIGN_TRANSACTIONAL_ENTITIES_ID", this.id);
        cv.put("MASTER_TRANSACTION_DETAILS_QUANTITY", Double.valueOf(this.entityQty.toPlainString()));
        cv.put("MASTER_TRANSACTION_DETAILS_PRICE", Double.valueOf(this.entityValue.toPlainString()));
        return cv;
    }

    public boolean equals(Product p){
        return (this.id == p.id);
    }

    @Override
    public Unit getProductUnit() {
        return this.unit;
    }
}
