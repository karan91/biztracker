package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;

import com.biztracker.database.DatabaseTalker;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.concurrent.Executor;

/**
 * Created by karan on 09/06/16.
 */

public class PaymentReceiptTransaction extends Transaction {

    public BigDecimal amount;
    @Override
    public void setType(int type){
     this.type = type;
    }

    @Override
    public void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor) {

    }

    @Override
    public ContentValues prepareContentValues() {
        ContentValues cv = super.prepareContentValues();
        cv.put(this.tableName + "_AMOUNT", this.amount.doubleValue());
        return cv;
    }

    @Override
    public MasterAccount getActiveAccount() {
        if(this.type == TYPE_RECEIPT){
            return creditAccount;
        }
        else if(this.type == TYPE_PAYMENT){
            return debitAccount;
        }
        return null;
    }

    @Override
    public String getTotalFormattedValue() {
        BigDecimal result = this.amount;

        NumberFormat format = NumberFormat.getInstance();
        String resultString = result.toString();
        if(resultString.contains(".") && !resultString.endsWith(".00")){
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
        }
        else{
            format.setMaximumFractionDigits(0);
        }
        format.setCurrency(Company.company.currency);
        String formattedValue = format.format(result);
        return format.getCurrency().getSymbol() + " " +formattedValue;
    }

    @Override
    public BigDecimal getNetValue() {
        return this.amount;
    }

    public PaymentReceiptTransaction(Cursor c){
        super(c);
        this.amount = new BigDecimal(c.getDouble(c.getColumnIndex("TRANSACTION_LIST_AMOUNT")));
        this.originalValue = new BigDecimal(c.getDouble(c.getColumnIndex("TRANSACTION_LIST_AMOUNT")));
    }

    public PaymentReceiptTransaction(){
        this.id = "-1";
    }
}
