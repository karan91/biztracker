package com.biztracker.databaseobjects;

import android.content.ContentValues;
import android.database.Cursor;

import com.biztracker.database.DatabaseTalker;

import org.json.JSONObject;

import java.util.UUID;
import java.util.concurrent.Executor;

/**
 * Created by shubham on 09/07/15.
 */
public abstract class DatabaseObject implements Cloneable{

    public boolean synchronizingOnServer = false;
    public String id;
    public String name;
    public abstract void insertObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor);
    public abstract void deleteObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor);
    public abstract void updateObject(DatabaseTalker.queryCompleteCallBack callBack, int processId, Executor executor);

    public DatabaseObject getCopy(){
        DatabaseObject result = null;
        try {
            result =  (DatabaseObject) this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return result;
    }

    public abstract ContentValues prepareContentValues();

    public String generateUniqueId(){
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }

}
