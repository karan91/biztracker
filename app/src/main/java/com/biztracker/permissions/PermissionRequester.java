package com.biztracker.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;


/**
 * Created by shubham on 10/04/16.
 */
public class PermissionRequester {

    public static final class Permissions{
        public static final String CAMERA = Manifest.permission.CAMERA;
        public static final String CONTACTS = Manifest.permission.READ_CONTACTS;
        public static final String STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        public static final String CALL_USER = Manifest.permission.CALL_PHONE;
    }


    public interface OnPermissionRequesterCompleted{
        void onPermissionGranted(String permission);
        void onPermissionCancelled(String permission);
    }

    private String[] permissions = null;    //Request Only One Permission
    private int REQUEST_CODE = 1341;
    private OnPermissionRequesterCompleted mCallback;
    private boolean isCompleted = false;

    public PermissionRequester(OnPermissionRequesterCompleted _callback,String[] _permissions){
        isCompleted = false;
        mCallback = _callback;
        this.permissions = _permissions;

        //Generate Request Code
        int hashPermission = this.permissions.hashCode();
        if(hashPermission < 0) hashPermission = -hashPermission;
        REQUEST_CODE = hashPermission;

    }

    public boolean canHandleRequestCode(int requestCode){
        if(this.isCompleted == false && REQUEST_CODE == requestCode) return true;
        return false;
    }

    public void handlePermissionRequestCompleted(String[] permissions,int[] grantResults){

        this.isCompleted = true;

        for(int i=0;i<permissions.length; i++){
            if(mCallback!=null){
                String permission = permissions[i];
                int grantResult = grantResults[i];
                if(grantResult== PackageManager.PERMISSION_GRANTED){
                    mCallback.onPermissionGranted(permission);
                }else{
                    mCallback.onPermissionCancelled(permission);
                }
            }else{
                Log.d("PermissionRequester","Callback is null");
            }
        }
    }

    public void requestPermissions(Activity activity){
        if(Build.VERSION.SDK_INT >= 23){
            try{
                activity.requestPermissions(permissions,REQUEST_CODE);
            }catch (Exception ex){}
        }
    }

    public static boolean shouldShowRequestPermissionRationale(Activity activity,String permission){
        if(Build.VERSION.SDK_INT >= 23){
            return (activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED)?false:true;
        }
        return false;
    }


}
