package com.biztracker;

import android.text.Editable;
import android.text.TextWatcher;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by shubham on 11/04/16.
 */
public class CustomTextWatcher implements TextWatcher {


    public int delayInMillis = 250;
    public interface CustomTextWatcherCallback{
        void onSearchForText(String text);
    }

    private CustomTextWatcherCallback watcherCallback;
    private Timer timer = null;

    public CustomTextWatcher(CustomTextWatcherCallback _watcherCallback){
        watcherCallback = _watcherCallback;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        final String text = s.toString();
        if(timer != null){
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(watcherCallback != null){
                    watcherCallback.onSearchForText(text);
                }
            }
        },delayInMillis); //250ms delay
    }
}
