package com.biztracker.fragments.taxesanddiscounts;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.biztracker.R;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.SlaveEntity;
import com.biztracker.fragments.InputFragment;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.masteraccounts.MasterAccountFragment;

import java.math.BigDecimal;

/**
 * Created by shubham on 24/04/16.
 */
public abstract class AddSlaveAccountFragment extends InputFragment implements View.OnClickListener, DatabaseTalker.queryCompleteCallBack {

    private CheckBox mPercentCheckBox;
    private CheckBox mAbsoluteCheckBox;

    private View mPercentIconView;
    private TextView saveButton;
    private EditText mNameText;
    private EditText mValueText;
    private SlaveEntity slaveEntity;

    private boolean isPercent = true;

    public interface SlaveAccountFragmentDelegate{
        void onSlaveAccountCreated(AddSlaveAccountFragment fragment);
    }

    public SlaveAccountFragmentDelegate delegate;
    @Override
    public boolean validateUIFields() {

        if(this.mNameText.getText().toString().trim().isEmpty()){
            Toast.makeText(getActivity(), "Input Name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(this.mValueText.getText().toString().trim().isEmpty()){
            Toast.makeText(getActivity(), "Input value", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    protected boolean isDiscount(){ return  false; }

    protected boolean isTax(){ return false; }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discounts_addition,null);

        isPercent = true;

        saveButton = (TextView) view.findViewById(R.id.save_btn);
        saveButton.setOnClickListener(this);

        mPercentCheckBox = (CheckBox)view.findViewById(R.id.percent_checkbox);
        mAbsoluteCheckBox = (CheckBox)view.findViewById(R.id.absolute_checkbox);

        mPercentIconView = view.findViewById(R.id.percent_icon);

        mNameText = (EditText)view.findViewById(R.id.name_et);
        mValueText = (EditText)view.findViewById(R.id.value_field);

        mPercentCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mAbsoluteCheckBox != null) {
                    mAbsoluteCheckBox.setChecked(!isChecked);
                }
                onPercentChecked(isChecked);
            }
        });

        mAbsoluteCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mPercentCheckBox != null){
                    mPercentCheckBox.setChecked(!isChecked);
                }

                onPercentChecked(!isChecked);
            }
        });

        return view;
    }


    public void onPercentChecked(boolean checked){
        if(checked){
            isPercent = true;
            if(mPercentIconView != null)    mPercentIconView.setVisibility(View.VISIBLE);
        }else{
            isPercent = false;
            if(mPercentIconView != null)    mPercentIconView.setVisibility(View.INVISIBLE);
        }
    }

    public SlaveEntity getSlaveEntity(){
        SlaveEntity slaveEntity = new SlaveEntity();
        slaveEntity.name = mNameText.getText().toString().trim();

        if (isPercent) {
            slaveEntity.calculatedAs = SlaveEntity.CALCULATED_AS_PERCENT;
        } else {
            slaveEntity.calculatedAs = SlaveEntity.CALCULATED_AS_ABSOLUTE;
        }

        slaveEntity.default_value = new BigDecimal(mValueText.getText().toString().trim());

        if (isDiscount()) {
            slaveEntity.nature = SlaveEntity.NATURE_SUBTRACTIVE;
        } else if (isTax()) {
            slaveEntity.nature = SlaveEntity.NATURE_ADDITIVE;
        }
        return slaveEntity;
    }

    @Override
    public void onClick(View v) {
        if(validateUIFields()) {
            if (slaveEntity == null) {
                DatabaseTalker.getInstance().slaveAccountExists(this, 2, AsyncTask.SERIAL_EXECUTOR, mNameText.getText().toString().trim());
            }
            else{

            }
        }
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if(processId == 1) {
            if (databaseObject != null) {
                Toast.makeText(getActivity(), "Account Created", Toast.LENGTH_SHORT).show();
                flushUIValues();
                if(this.delegate != null){
                    this.delegate.onSlaveAccountCreated(AddSlaveAccountFragment.this);
                }
            }
        }
        else if(processId == 2){
            if((boolean)databaseObject == false){
                slaveEntity = getSlaveEntity();
                slaveEntity.insertObject(this, 1, AsyncTask.THREAD_POOL_EXECUTOR);
            }
            else{
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Cannot Save");
                builder.setMessage(mNameText.getText().toString().trim() + " is present in your record");
                builder.setPositiveButton("OK", null);
                builder.show();
            }
        }
    }

    private void flushUIValues(){
        mNameText.setText("");
        mValueText.setText("");
        isPercent = true;
        mPercentCheckBox.setChecked(true);
        mAbsoluteCheckBox.setChecked(false);
        slaveEntity = null;
    }

    @Override
    public void resetTransactionAndUIFields() {
        flushUIValues();
    }
}
