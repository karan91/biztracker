package com.biztracker.fragments.taxesanddiscounts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;

/**
 * Created by shubham on 24/04/16.
 */
public class AddTaxChargesFragment extends AddSlaveAccountFragment {

    public AddTaxChargesFragment(){
        mFragmentTag = "TaxChargesFragment";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ((TextView)view.findViewById(R.id.header)).setText("Add Taxes / Charges");
        return view;
    }

    @Override
    protected boolean isTax() {
        return true;
    }
}
