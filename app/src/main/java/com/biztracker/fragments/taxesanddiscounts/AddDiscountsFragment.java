package com.biztracker.fragments.taxesanddiscounts;


/**
 * Created by shubham on 24/04/16.
 */
public class AddDiscountsFragment extends AddSlaveAccountFragment{

    public AddDiscountsFragment(){
        mFragmentTag = "DiscountFragment";
    }

    @Override
    protected boolean isDiscount() {
        return true;
    }
}
