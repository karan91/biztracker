package com.biztracker.fragments.onboarding;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.R;
import com.biztracker.fragments.SuperFragment;

/**
 * Created by shubham on 15/04/16.
 */
public class OnBoardingFragment extends SuperFragment {

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboarding,null);
        ViewPager viewpager = (ViewPager)view.findViewById(R.id.view_pager);
        viewpager.setAdapter(new OnBoardingPagerAdapter(getActivity().getSupportFragmentManager()));
        return view;
    }

    private class OnBoardingPagerAdapter extends FragmentPagerAdapter{

        public OnBoardingPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt("index", position);
            ChildOnBoardingFragment childFragment = new ChildOnBoardingFragment();
            childFragment.setArguments(bundle);
            return childFragment;
        }

        @Override
        public int getCount() {
            return 5;
        }
    }
}
