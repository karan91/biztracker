package com.biztracker.fragments.onboarding;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.fragments.SuperFragment;

/**
 * Created by shubham on 15/04/16.
 */
public class ChildOnBoardingFragment extends SuperFragment {

    private TextView headingView;
    private TextView subjectView;
    private ImageView imageView;

    private final static String[] headings = new String[]{
            "Create Invoices",
            "Works Offline",
            "Barcode Scanner",
            "Safe and Secure",
            "Universal Search"
    };

    private final static String[] subjects = new String[]{
            "",
            "",
            "",
            "",
            ""
    };

    private final static int[] images = new int[]{

    };

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_child_onboard,null);

        headingView = (TextView)view.findViewById(R.id.heading);
        subjectView = (TextView)view.findViewById(R.id.subject);
        imageView = (ImageView)view.findViewById(R.id.imageview);

        Bundle bundle = getArguments();
        int index = bundle.getInt("index");

        headingView.setText(headings[index]);

        return view;
    }
}
