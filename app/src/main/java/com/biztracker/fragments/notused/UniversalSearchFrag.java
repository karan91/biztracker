package com.biztracker.fragments.notused;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.R;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.InvoiceFrag;
import com.biztracker.fragments.history.AccountsHistory.UniSearchAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karan on 03/02/16.
 */
public class UniversalSearchFrag extends SuperFragment implements DatabaseTalker.queryCompleteCallBack, UniSearchAdapter.OnTransactionTappedListener {

    View parentView;
    RecyclerView recyclerView;
    List<Transaction> transactionList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(parentView == null) {
            parentView = inflater.inflate(R.layout.universal_search_layout, container, false);
            transactionList = new ArrayList<>();
            recyclerView = (RecyclerView) parentView.findViewById(R.id.universal_search_recycler_view);
            UniSearchAdapter adapter = new UniSearchAdapter(transactionList, getActivity());
            recyclerView.setAdapter(adapter);
            adapter.delegate = this;
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            DatabaseTalker.getInstance().getAllTransactions(this, 1, AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else{
            container.removeView(parentView);
        }
        return parentView;
    }

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {

        if(processId == 1 && databaseObject != null){
            List<Transaction> trans = (List<Transaction>) databaseObject;
            transactionList.addAll(trans);
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onTransactionTapped(InvoicedTransaction transaction) {
        InvoiceFrag frag = new InvoiceFrag();
        frag.invoiceTransaction = transaction;
        DatabaseTalker.getInstance().loadProductsInInvoicedTransaction(transaction, frag, 5, AsyncTask.THREAD_POOL_EXECUTOR);
        this.transitionToFragment(frag);
    }
}
