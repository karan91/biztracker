package com.biztracker.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.activities.MainActivity;
import com.biztracker.R;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.InvoiceFrag;

/**
 * Created by shubham on 09/07/15.
 */
public abstract class SuperFragment extends Fragment {

    public SuperFragment previousFragment;
    boolean exit;

    protected String mFragmentTag = "superfragment";
    private boolean isParent = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(savedInstanceState != null){
            Log.d("SuperFragment","SuperFragment: "+this+"Key: "+mFragmentTag+" previousFragment:"+previousFragment);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public MainActivity getMainActivity(){
        MainActivity result = (MainActivity)getActivity();
        return result;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
    }

    public abstract boolean validateUIFields();

    public void onNativeBackPress(){
        Activity activity = getActivity();
        if(activity != null && activity instanceof MainActivity){
            activity.onBackPressed();
        }
    }

    public void onBackPressed(){

        MainActivity activity = getMainActivity();
        if(activity != null) {
            if (exit || !(this instanceof InputFragment)) {
                activity.onOpenDrawer(null);
                resetTransactionAndUIFields();
                exit = false;
            } else {
                DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == AlertDialog.BUTTON_POSITIVE) {
                            exit = true;
                            onBackPressed();
                        } else {
                            exit = false;
                        }
                    }
                };
                AlertDialog dialog = new AlertDialog.Builder(getMainActivity()).setTitle("Exit").setMessage("Do you want to exit?").create();
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", onClickListener);
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", onClickListener);
                dialog.show();
            }
        }
    }

    public void resetTransactionAndUIFields(){}

    public void transitionToFragment(SuperFragment fragment){
        if(this instanceof InvoiceFrag){
            isParent = true;
        }
        else{
            isParent = false;
        }
        fragment.isParent = false;
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        if(isParent){
            transaction.replace(R.id.root_fragment_frame, fragment).addToBackStack("ppbackstack").commit();
        }else {
            transaction.replace(R.id.root_fragment_frame, fragment).addToBackStack(null).commit();
        }
    }

    public void navigateToParentFragment(){
        if(this.isParent == false){
            getFragmentManager().popBackStackImmediate("ppbackstack", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("SuperFragment","SuperFragment: "+this+"key: "+mFragmentTag);
        if(previousFragment != null){
        }
    }

    public void onFragmentOpened(){}

}
