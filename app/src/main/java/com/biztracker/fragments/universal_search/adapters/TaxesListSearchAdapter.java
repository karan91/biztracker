package com.biztracker.fragments.universal_search.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.databaseobjects.Company;
import com.biztracker.databaseobjects.SlaveEntity;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;

import java.util.Currency;

/**
 * Created by shubham on 06/06/16.
 */
public class TaxesListSearchAdapter extends BaseUniversalSearchAdapter<SlaveEntity>{

    public TaxesListSearchAdapter(Context context) {
        super(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TaxesListRecyclerHolder(this.inflater.inflate(R.layout.universal_search_list_item_slave,parent,false));
    }

    private class TaxesListRecyclerHolder extends BaseUniversalRecyclerHolder<SlaveEntity>{

        private TextView taxNameView;
        private TextView taxRateView;

        public TaxesListRecyclerHolder(View itemView) {
            super(itemView);
            taxNameView = (TextView) itemView.findViewById(R.id.slave_name_search_tv);
            taxRateView = (TextView) itemView.findViewById(R.id.slave_rate_tv);
            itemView.setOnClickListener(TaxesListRecyclerHolder.this);
        }

        @Override
        public void onClick(View view) {
            if(delegate!=null){
                delegate.onItemClicked(this.item);
            }
        }

        @Override
        public void setItem(SlaveEntity _item) {
            super.setItem(_item);
            this.taxNameView.setText(_item.name);
            if(_item.calculatedAs == SlaveEntity.CALCULATED_AS_ABSOLUTE) {
                this.taxRateView.setText(_item.getFormattedValue());
            }else if(_item.calculatedAs == SlaveEntity.CALCULATED_AS_PERCENT){
                this.taxRateView.setText(_item.getValue().toString()+" %");
            }
        }
    }
}
