package com.biztracker.fragments.universal_search.childfragments.base;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.common.Utilities;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.DividerItemDecoration;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;

import java.util.List;

/**
 * Created by shubham on 10/04/16.
 */
public abstract class UniversalSearchBaseFragment<E> extends SuperFragment implements DatabaseTalker.queryCompleteCallBack,
BaseUniversalSearchAdapter.onItemClickListener{

    private RecyclerView mRecyclerView;
    private View mView;
    private boolean isNewSearchQueryExecuted = false;
    private TextView noItemsTextView;
    private String lastQuery = "";
    public EditText searchHolder;

    private boolean shouldSearchForNewQuery = false;
    private int mHeight = 10;

    public String mQuery = "";
    public String mPlaceholder = "";
    public String noItemsString = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(mView == null){
            mView = inflater.inflate(R.layout.universal_search_child_fragment,null);
            mHeight = 5 * ((int)getResources().getDimension(R.dimen.one_dp));
            mRecyclerView = (RecyclerView)mView.findViewById(R.id.recycler_view);
            noItemsTextView = (TextView) mView.findViewById(R.id.no_items_text_view);
            noItemsTextView.setText(noItemsString);
            BaseUniversalSearchAdapter mAdapter = createAdapter(getActivity().getApplicationContext());
            mRecyclerView.setAdapter(mAdapter);
            //Set Callback
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(),LinearLayoutManager.VERTICAL,false));
            DividerItemDecoration decoration = new DividerItemDecoration(20, 20);
            decoration.disableTopPadding = false;
            mRecyclerView.addItemDecoration(decoration);

            //Log.d("BizApp","BaseFragment: onCreateView: "+this);
            onPresentedOnScreen();
        }else{
            container.removeView(mView);
        }
        return mView;
    }

    @Override
    public boolean validateUIFields() {
        return false;
    }

    public abstract BaseUniversalSearchAdapter createAdapter(Context context);
    public abstract void requestDataFromDatabaseForQuery(String query);

    //To Handle Current Search Fragment
    public void onPresentedOnScreen(){
        shouldSearchForNewQuery = true;
        if(this.isNewSearchQueryExecuted == false){
            this.isNewSearchQueryExecuted = true;
            requestDataFromDatabaseForQuery(this.lastQuery);
        }
    }

    public void onWentOffScreen(){
        shouldSearchForNewQuery = false;
    }

    public void setSearchQuery(String query){
        this.lastQuery = query;
        if(shouldSearchForNewQuery == false){
            isNewSearchQueryExecuted = false;
        }else{
            isNewSearchQueryExecuted = true;
            requestDataFromDatabaseForQuery(query);
        }
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if((processId == 1 || processId == 2)&& databaseObject != null){
            if(databaseObject instanceof List){
                List<E> items = (List<E>)databaseObject;
                if(mRecyclerView != null) {
                    BaseUniversalSearchAdapter searchAdapter = (BaseUniversalSearchAdapter)mRecyclerView.getAdapter();
                    if (searchAdapter != null) {
                        searchAdapter.delegate = this;
                        searchAdapter.setItems(items);
                        if(processId == 2){
                            if(items.isEmpty()){
                                noItemsTextView.setVisibility(View.VISIBLE);
                            }
                        }
                        if(!items.isEmpty()){
                                noItemsTextView.setVisibility(View.GONE);
                        }
                    } else {
                     //   Log.e("UniSearchFrag", "onReceivedDatabaseObject - Search Adapter is null: " + this);
                    }
                }else{
                    //Log.e("UniSearchFrag", "onReceivedDatabaseObject - Recycler View is null: " + this);
                }
            }else{
                //Log.e("UniSearchFrag","onReceivedDatabaseObject - List Error");
            }
        }
    }

    @Override
    public void onItemClicked(Object item) {
        if(searchHolder.hasFocus()) {
            searchHolder.clearFocus();
            Utilities.hideKeyboard(getActivity());

        }
    }

    @Override
    public void onFragmentOpened() {
        requestDataFromDatabaseForQuery(this.mQuery);
    }
}
