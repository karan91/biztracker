package com.biztracker.fragments.universal_search.childfragments;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.fragments.history.AccountsHistory.AccountsHistoryViewerFragment;
import com.biztracker.fragments.universal_search.adapters.AccountListSearchAdapter;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;
import com.biztracker.fragments.universal_search.childfragments.base.UniversalSearchBaseFragment;

/**
 * Created by shubham on 10/04/16.
 */
public class UniSearchAccountsFragment extends UniversalSearchBaseFragment<MasterAccount> {

    public UniSearchAccountsFragment(){
        mFragmentTag = "UniSearchAccountsFragment";
        mPlaceholder = "Search for Accounts";
        noItemsString = "Nothing to show here.\nAdd an account from the left drawer";
    }

    @Override
    public BaseUniversalSearchAdapter createAdapter(Context context) {
        return new AccountListSearchAdapter(context);
    }

    @Override
    public void requestDataFromDatabaseForQuery(String query) {
        //Log.d("Search", "Search for Query: " + query + " " + this);
        if(query == null || query.trim().isEmpty()) {
            mQuery = null;
            DatabaseTalker.getInstance().getAllMasterAccounts(this, 2, AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else{
            mQuery = query;
            DatabaseTalker.getInstance().getMasterAccountsForQuery(this, 1, AsyncTask.THREAD_POOL_EXECUTOR, query);
        }
    }

    @Override
    public void onItemClicked(Object item) {
        //MasterAccount
        //Log.d("BizApp","Item Clicked: "+item);
        super.onItemClicked(item);
        AccountsHistoryViewerFragment fragment = new AccountsHistoryViewerFragment();
        fragment.masterAccount = (MasterAccount) item;
        this.transitionToFragment(fragment);
    }

}
