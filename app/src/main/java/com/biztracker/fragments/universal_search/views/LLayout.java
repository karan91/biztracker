package com.biztracker.fragments.universal_search.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

import com.biztracker.R;

/**
 * Created by shubham on 10/04/16.
 */
public class LLayout extends LinearLayout{

    private int SEL_COLOR = 0xff414141;
    private int BG_COLOR = 0xffb7b7b7;

    private Paint mLinePaint = new Paint();
    private Paint mSelectionPaint = new Paint();
    private int mPosition = 0;

    public LLayout(Context context) {
        super(context);
        initialize();
    }

    public LLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public LLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public LLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    public void initialize(){
        mLinePaint.setAntiAlias(true);
        mSelectionPaint.setAntiAlias(true);
        mLinePaint.setColor(BG_COLOR);
        mSelectionPaint.setColor(SEL_COLOR);
    }

    public void setCurrentPosition(int position){
        mPosition = position;
        if(mPosition == 0){
            findViewById(R.id.invoices_btn).setBackgroundColor(0xFF3A506B);
            findViewById(R.id.accounts_btn).setBackgroundColor(Color.BLACK);
            findViewById(R.id.products_btn).setBackgroundColor(Color.BLACK);
        }else if(mPosition == 1){
            findViewById(R.id.invoices_btn).setBackgroundColor(Color.BLACK);
            findViewById(R.id.accounts_btn).setBackgroundColor(0xFF3A506B);
            findViewById(R.id.products_btn).setBackgroundColor(Color.BLACK);
        }else if(mPosition == 2){
            findViewById(R.id.invoices_btn).setBackgroundColor(Color.BLACK);
            findViewById(R.id.accounts_btn).setBackgroundColor(Color.BLACK);
            findViewById(R.id.products_btn).setBackgroundColor(0xFF3A506B);
        }
        invalidate();
    }

}
