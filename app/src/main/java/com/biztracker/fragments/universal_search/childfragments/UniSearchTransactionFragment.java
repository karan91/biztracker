package com.biztracker.fragments.universal_search.childfragments;

import android.content.Context;
import android.os.AsyncTask;

import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.PaymentReceiptTransaction;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.prints.PDFInvoiceViewAndPrintFragment;
import com.biztracker.fragments.Transactions.invoice.ViewablePurchaseInvoiceFrag;
import com.biztracker.fragments.Transactions.invoice.ViewableSalesInvoiceFrag;
import com.biztracker.fragments.Transactions.paymentsAndReceipts.ViewablePaymentFrag;
import com.biztracker.fragments.Transactions.paymentsAndReceipts.ViewableReceiptFrag;
import com.biztracker.fragments.universal_search.adapters.InvoiceListSearchAdapter;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;
import com.biztracker.fragments.universal_search.childfragments.base.UniversalSearchBaseFragment;
import com.biztracker.prints.PDFPaymentFragment;
import com.biztracker.prints.PDFReceiptFragment;

/**
 * Created by shubham on 10/04/16.
 */
public class UniSearchTransactionFragment extends UniversalSearchBaseFragment<Transaction> implements InvoiceListSearchAdapter.InvoiceCellActionCallback {

    public UniSearchTransactionFragment(){
        mFragmentTag = "UniSearchTransactionFragment";
        mPlaceholder = "Search for Invoices";
        noItemsString = "Nothing to show here.\nCreate a transaction from the left drawer";
    }

    @Override
    public BaseUniversalSearchAdapter createAdapter(Context context) {
        return new InvoiceListSearchAdapter(context,this);
    }

    @Override
    public void requestDataFromDatabaseForQuery(String query) {
        //Log.d("Search","Search for Query: "+query + " "+this);
        if(query == null || query.trim().isEmpty()) {
            mQuery = null;
            DatabaseTalker.getInstance().getAllTransactions(this, 2, AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else{
            mQuery = query;
            DatabaseTalker.getInstance().getInvoicesForSearchQuery(this, 1, AsyncTask.THREAD_POOL_EXECUTOR, query);
        }
    }

    @Override
    public void onPrintItem(Transaction transaction) {

        if(transaction instanceof InvoicedTransaction){
            PDFInvoiceViewAndPrintFragment fragment = new PDFInvoiceViewAndPrintFragment();
            fragment.showPrintUI = true;
            fragment.setTransaction((InvoicedTransaction) transaction);
            this.transitionToFragment(fragment);
        }
        else if(transaction instanceof PaymentReceiptTransaction){
            if(transaction.getType() == Transaction.TYPE_PAYMENT){
                PDFPaymentFragment fragment = new PDFPaymentFragment();
                fragment.showPrintUI = true;
                fragment.setTransaction((PaymentReceiptTransaction) transaction);
                this.transitionToFragment(fragment);
            }
            else if(transaction.getType() == Transaction.TYPE_RECEIPT){
                PDFReceiptFragment fragment = new PDFReceiptFragment();
                fragment.showPrintUI = true;
                fragment.setTransaction((PaymentReceiptTransaction) transaction);
                this.transitionToFragment(fragment);
            }
        }

    }
    @Override
    public void onShareItem(Transaction transaction) {
        //Log.d("BizApp","Share");

        if(transaction instanceof InvoicedTransaction){
            PDFInvoiceViewAndPrintFragment fragment = new PDFInvoiceViewAndPrintFragment();
            fragment.showShareUI = true;
            fragment.setTransaction((InvoicedTransaction) transaction);
            this.transitionToFragment(fragment);
        }
        else if(transaction instanceof PaymentReceiptTransaction){
            if(transaction.getType() == Transaction.TYPE_PAYMENT){
                PDFPaymentFragment fragment = new PDFPaymentFragment();
                fragment.showShareUI = true;
                fragment.setTransaction((PaymentReceiptTransaction) transaction);
                this.transitionToFragment(fragment);
            }
            else if(transaction.getType() == Transaction.TYPE_RECEIPT){
                PDFReceiptFragment fragment = new PDFReceiptFragment();
                fragment.showShareUI = true;
                fragment.setTransaction((PaymentReceiptTransaction) transaction);
                this.transitionToFragment(fragment);
            }
        }
    }

    @Override
    public void onViewItem(Transaction transaction) {
        //Log.d("BizApp","View");
        if(transaction.getType() == Transaction.TYPE_PURCHASE){
            ViewablePurchaseInvoiceFrag fragment = new ViewablePurchaseInvoiceFrag();
            fragment.invoiceTransaction = (InvoicedTransaction) transaction;
            transitionToFragment(fragment);
        }
        else if(transaction.getType() == Transaction.TYPE_SALES){
            ViewableSalesInvoiceFrag fragment = new ViewableSalesInvoiceFrag();
            fragment.invoiceTransaction = (InvoicedTransaction)transaction;
            transitionToFragment(fragment);
        }
        else if(transaction.getType() == Transaction.TYPE_PAYMENT){
            ViewablePaymentFrag fragment = new ViewablePaymentFrag();
            fragment.setTransaction((PaymentReceiptTransaction)transaction);
            transitionToFragment(fragment);
        }
        else if(transaction.getType() == Transaction.TYPE_RECEIPT){
            ViewableReceiptFrag fragment = new ViewableReceiptFrag();
            fragment.setTransaction((PaymentReceiptTransaction)transaction);
            transitionToFragment(fragment);
        }
    }



    @Override
    public void onItemClicked(Object item) {
        super.onItemClicked(item);
    }
}
