package com.biztracker.fragments.universal_search.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.databaseobjects.Company;
import com.biztracker.databaseobjects.Product;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;

/**
 * Created by shubham on 09/04/16.
 */
public class ProductListSearchAdapter extends BaseUniversalSearchAdapter<Product> {

    public ProductListSearchAdapter(Context context) {
        super(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductListRecyclerHolder(
                this.inflater.inflate(R.layout.universal_search_list_item_product,parent,false));
    }



    private class ProductListRecyclerHolder extends BaseUniversalRecyclerHolder<Product>{
        View parentView;
        TextView productNameHolder;
        TextView unitsLeftHolder;
        TextView salePriceHolder;

        public ProductListRecyclerHolder(View itemView) {
            super(itemView);
            parentView = itemView;
            parentView.setOnClickListener(this);
            productNameHolder = (TextView) parentView.findViewById(R.id.product_name_search_tv);
            unitsLeftHolder = (TextView) parentView.findViewById(R.id.units_left_tv);
            salePriceHolder = (TextView) parentView.findViewById(R.id.sales_price_tv);
        }

        @Override
        public void setItem(Product _item) {
            super.setItem(_item);
            parentView.setTag(_item);
            productNameHolder.setText(_item.name);
            unitsLeftHolder.setText(_item.closingUnits.toString());
            String currencySymbol = Company.company.currency.getSymbol();
            salePriceHolder.setText(currencySymbol + " " +_item.sellingPrice.toString());
        }

        @Override
        public void onClick(View v) {
            if(delegate != null) delegate.onItemClicked(v.getTag());
        }
    }

}
