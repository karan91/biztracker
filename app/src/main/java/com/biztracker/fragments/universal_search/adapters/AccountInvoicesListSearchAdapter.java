package com.biztracker.fragments.universal_search.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by shubham on 10/04/16.
 */
public class AccountInvoicesListSearchAdapter extends BaseUniversalSearchAdapter<Transaction> {

    public InvoiceListSearchAdapter.InvoiceCellActionCallback callback;
    public int selectedPos = -1;
    public MasterAccount masterAccount;

    public AccountInvoicesListSearchAdapter(Context context, InvoiceListSearchAdapter.InvoiceCellActionCallback actionCallback) {
        super(context);
        this.callback = actionCallback;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == selectedPos) return 2;
        return 1;
    }

    @Override
    public void setItems(List<Transaction> _items) {
        selectedPos = -1;
        super.setItems(_items);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 2){
            return new AccountInvoicesRecyclerHolder(
                    this.inflater.inflate(R.layout.universal_search_list_item_account_invoice_expanded,parent,false));
        }else {
            return new AccountInvoicesRecyclerHolder(
                    this.inflater.inflate(R.layout.universal_search_list_item_account_invoice, parent, false));
        }
    }





    private class AccountInvoicesRecyclerHolder extends BaseUniversalRecyclerHolder<Transaction>{

        private TextView amountLabel;
        private TextView invoiceLabel;
        private TextView dateLabel;
        private TextView transactionTypeLabel;
        private TextView masterAccountHolder;

        private View parentView;
        private final static String INVOICE_ID_DISPLAY_FORMAT = "%s #%s";

        public AccountInvoicesRecyclerHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            Log.d("Adapter", "Creating INvoice Adapter: " + itemView);

            this.amountLabel = ((TextView)itemView.findViewById(R.id.transcation_amount));

            this.invoiceLabel = (TextView)itemView.findViewById(R.id.invoice_id);
            this.dateLabel = (TextView)itemView.findViewById(R.id.date_text);
            this.transactionTypeLabel = ((TextView)itemView.findViewById(R.id.transaction_type));
            this.masterAccountHolder = (TextView) itemView.findViewById(R.id.master_account_name_holder);
            this.parentView = itemView;

            View view = itemView.findViewById(R.id.action_view);
            if(view != null){
                view.setOnClickListener(AccountInvoicesRecyclerHolder.this);
                itemView.findViewById(R.id.action_print).setOnClickListener(AccountInvoicesRecyclerHolder.this);
                itemView.findViewById(R.id.action_share).setOnClickListener(AccountInvoicesRecyclerHolder.this);
            }
        }

        @Override
        public void setItem(Transaction _item) {
            super.setItem(_item);
            Transaction transaction = (Transaction)_item;
            //invoice id
            this.parentView.setTag(_item);
            String invoice_id = (transaction.transactionNumber == null)?"":transaction.transactionNumber;

            //TODO: set invoice number to invoice label
            this.amountLabel.setText(transaction.getTotalFormattedValue());
            this.dateLabel.setText(transaction.getPrettyDate());
            if(masterAccount.id.equals(transaction.debitAccount.id)){
                this.masterAccountHolder.setText(transaction.creditAccount.name);
            }
            else{
                this.masterAccountHolder.setText(transaction.debitAccount.name);
            }
            this.transactionTypeLabel.setText(transaction.getTypeName());
        }

        @Override
        public void onClick(View v) {

            if(v.getId() == R.id.action_print){
                if(callback!=null) callback.onPrintItem(this.item);
                return;
            }else if(v.getId() == R.id.action_share){
                if(callback!=null) callback.onShareItem(this.item);
                return;
            }else if(v.getId() == R.id.action_view){
                if(callback!=null) callback.onViewItem(this.item);
                return;
            }

            if(selectedPos == this.position) {
                selectedPos = -1;
                notifyItemChanged(this.position);
            }
            else {
                int pos1 = selectedPos;
                selectedPos = this.position;
                if(pos1 >= 0) notifyItemChanged(pos1);
                notifyItemChanged(this.position);
            }
            delegate.onItemClicked(v.getTag());
        }

    }
}
