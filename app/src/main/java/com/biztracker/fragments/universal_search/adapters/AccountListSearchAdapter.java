package com.biztracker.fragments.universal_search.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;


/**
 * Created by shubham on 10/04/16.
 */
public class AccountListSearchAdapter extends BaseUniversalSearchAdapter<MasterAccount> {
    public AccountListSearchAdapter(Context context) {
        super(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AccountListRecyclerHolder(
                this.inflater.inflate(R.layout.universal_search_list_item_account,parent,false));
    }

    private class AccountListRecyclerHolder extends BaseUniversalRecyclerHolder<MasterAccount> {
        View parentView;
        TextView accountNameView;
        TextView accountTypeHolder;
        TextView balanceHolder;

        public AccountListRecyclerHolder(View itemView) {
            super(itemView);
            parentView = itemView;
            parentView.setOnClickListener(this);
            accountNameView = (TextView) parentView.findViewById(R.id.account_name);
            accountTypeHolder = (TextView) parentView.findViewById(R.id.account_type);
            balanceHolder = (TextView) parentView.findViewById(R.id.account_balance_tv);
        }

        @Override
        public void setItem(MasterAccount _item) {
            super.setItem(_item);
            accountNameView.setText(_item.name);
            accountTypeHolder.setText(_item.accountType.name);
            Log.d("BALANCE", "Bal:" + _item.openingBalance);
            balanceHolder.setText(_item.getFormattedClosingBalance());
            parentView.setTag(_item);
        }

        @Override
        public void onClick(View v) {
            if(delegate != null){
                delegate.onItemClicked(this.item);
            }
        }
    }
}
