package com.biztracker.fragments.universal_search.childfragments;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.Product;
import com.biztracker.fragments.history.ProductHistory.ProductHistoryViewerFragment;
import com.biztracker.fragments.universal_search.adapters.ProductListSearchAdapter;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;
import com.biztracker.fragments.universal_search.childfragments.base.UniversalSearchBaseFragment;

/**
 * Created by shubham on 10/04/16.
 */
public class UniSearchProductsFragment extends UniversalSearchBaseFragment<Product>{

    public UniSearchProductsFragment(){
        mFragmentTag = "UniSearchProductFragment";
        mPlaceholder = "Search for Products";
        noItemsString = "Nothing to show here.\nAdd a product from the left drawer";
    }

    @Override
    public BaseUniversalSearchAdapter createAdapter(Context context) {
        return new ProductListSearchAdapter(context);
    }

    @Override
    public void requestDataFromDatabaseForQuery(String query) {
        if(query == null || query.trim().isEmpty()){
            mQuery = null;
            DatabaseTalker.getInstance().getAllProducts(this,2, AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else{
            mQuery = query;
            DatabaseTalker.getInstance().getProductsForSearchQuery(this, 1, AsyncTask.THREAD_POOL_EXECUTOR, query);
        }
    }

    @Override
    public void onItemClicked(Object item) {
        super.onItemClicked(item);
        //Product
        ProductHistoryViewerFragment frag = new ProductHistoryViewerFragment();
        frag.product = (Product) item;
        this.transitionToFragment(frag);
        //Log.d("BizApp","Item Clicked: "+item);
    }
}
