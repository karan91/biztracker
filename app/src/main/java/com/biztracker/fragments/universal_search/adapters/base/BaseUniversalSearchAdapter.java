package com.biztracker.fragments.universal_search.adapters.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.R;

import java.util.List;

/**
 * Created by shubham on 09/04/16.
 */
public abstract class BaseUniversalSearchAdapter<E> extends RecyclerView.Adapter{

    public interface onItemClickListener{
        void onItemClicked(Object item);
    }

    private Context mContext;
    protected LayoutInflater inflater;
    private List<E> items;
    public onItemClickListener delegate;

    public BaseUniversalSearchAdapter(Context context) {
        mContext = context;
        inflater = LayoutInflater.from(context);
    }

    public void setItems(List<E> _items){
        this.items = _items;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(this.items == null) return 0;
        return this.items.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        E item = this.items.get(position);
        BaseUniversalRecyclerHolder h = (BaseUniversalRecyclerHolder)holder;
        h.position = position;
        h.setItem(item);
    }

    public abstract class BaseUniversalRecyclerHolder<E> extends RecyclerView.ViewHolder implements View.OnClickListener{

        protected E item = null;
        protected int position = 0;

        public BaseUniversalRecyclerHolder(View itemView) {
            super(itemView);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            itemView.setLayoutParams(lp);
        }

        public void setItem(E _item) {
            item = _item;
        }
    }

}
