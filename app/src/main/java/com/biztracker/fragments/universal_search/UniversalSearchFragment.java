package com.biztracker.fragments.universal_search;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.biztracker.CustomTextWatcher;
import com.biztracker.R;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.universal_search.childfragments.UniSearchAccountsFragment;
import com.biztracker.fragments.universal_search.childfragments.UniSearchTransactionFragment;
import com.biztracker.fragments.universal_search.childfragments.UniSearchProductsFragment;
import com.biztracker.fragments.universal_search.childfragments.base.UniversalSearchBaseFragment;
import com.biztracker.fragments.universal_search.views.LLayout;

/**
 * Created by shubham on 10/04/16.
 */
public class UniversalSearchFragment extends SuperFragment{

    private SearchPagerAdapter mPagerAdapter;

    private EditText editText;
    private View mView;
    private ViewPager mViewPager;
    private LLayout mLLayout;

    private int mCurrentVisiblePage = 0;

    public UniversalSearchFragment(){
        mFragmentTag = "UniversalSearchFragment";
    }

    public void setCurrentVisiblePage(int page){
        mCurrentVisiblePage = page;
        if(mViewPager != null) mViewPager.setCurrentItem(page,true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("BizApp","BizApp: "+this);
        if(mView == null){
            View view = inflater.inflate(R.layout.universal_search_fragment,null);

            editText = (EditText)view.findViewById(R.id.searchbox);
            editText.removeTextChangedListener(mTextWatcher);
            editText.addTextChangedListener(mTextWatcher);

            mViewPager = (ViewPager)view.findViewById(R.id.view_pager);
            int[] btnids = new int[]{R.id.invoices_btn,R.id.products_btn,R.id.accounts_btn};
            for(int btnid: btnids){
                view.findViewById(btnid).setOnClickListener(mOnTabClickListener);
            }

            if(mPagerAdapter == null){
                mPagerAdapter = (SearchPagerAdapter)mViewPager.getAdapter();
                Log.d("PagerAdapter","PagerAdapter : "+mPagerAdapter);
                if(mPagerAdapter == null) {
                    mPagerAdapter = new SearchPagerAdapter(getActivity().getSupportFragmentManager());
                }
            }
            mViewPager.setAdapter(mPagerAdapter);
            mView = view;
            mLLayout = (LLayout)view.findViewById(R.id.btn_layout);

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }
                @Override
                public void onPageSelected(int position) {
                    mCurrentVisiblePage = position;
                    Log.d("onPageSelected","onPageSelected: "+position);
                    if(mPagerAdapter!=null){
                        mPagerAdapter.onPageSelected(position);
                        if(position == 0){
                            if(mPagerAdapter.mInvoicesFragment != null){
                                editText.setHint(mPagerAdapter.mInvoicesFragment.mPlaceholder);
                                editText.setText(mPagerAdapter.mInvoicesFragment.mQuery);
                            }
                        }else if(position == 1){
                            if(mPagerAdapter.mAccountsFragment != null){
                                editText.setHint(mPagerAdapter.mAccountsFragment.mPlaceholder);
                                editText.setText(mPagerAdapter.mAccountsFragment.mQuery);
                            }
                        }else if(position == 2){
                            if(mPagerAdapter.mProductsFragment != null){
                                editText.setHint(mPagerAdapter.mProductsFragment.mPlaceholder);
                                editText.setText(mPagerAdapter.mProductsFragment.mQuery);
                            }
                        }
                    }
                    if(mLLayout != null) mLLayout.setCurrentPosition(position);
                }
                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });

        }else{
            container.removeView(mView);
        }

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mViewPager!=null) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    mViewPager.setCurrentItem(mCurrentVisiblePage,true);
                }
            });
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private CustomTextWatcher mTextWatcher = new CustomTextWatcher(new CustomTextWatcher.CustomTextWatcherCallback() {
        @Override
        public void onSearchForText(String text) {
            if(mPagerAdapter != null && mViewPager != null){
                //mPagerAdapter.onSearchForText(text);
                int position = mViewPager.getCurrentItem();
                if(position == 0){
                    if(mPagerAdapter.mInvoicesFragment != null){
                        mPagerAdapter.mInvoicesFragment.setSearchQuery(text);
                    }
                }else if(position == 1){
                    if(mPagerAdapter.mAccountsFragment != null){
                        mPagerAdapter.mAccountsFragment.setSearchQuery(text);
                    }
                }else if(position == 2){
                    if(mPagerAdapter.mProductsFragment != null){
                        mPagerAdapter.mProductsFragment.setSearchQuery(text);
                    }
                }
            }
        }
    });

    private View.OnClickListener mOnTabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mViewPager == null){
                Log.e("UniSearchFragment","View Pager is null");
                return;
            }
            switch (v.getId()){
                case R.id.invoices_btn:
                    mViewPager.setCurrentItem(0); break;
                case R.id.products_btn:
                    mViewPager.setCurrentItem(2); break;
                case R.id.accounts_btn:
                    mViewPager.setCurrentItem(1); break;
            }
        }
    };

    @Override
    public boolean validateUIFields() {
        return false;
    }


    private class SearchPagerAdapter extends FragmentStatePagerAdapter{
        private UniversalSearchBaseFragment mAccountsFragment;
        private UniversalSearchBaseFragment mInvoicesFragment;
        private UniversalSearchBaseFragment mProductsFragment;

        private UniversalSearchBaseFragment mSelectedFragment;

        public SearchPagerAdapter(FragmentManager fm){
            super(fm);
        }

        public void onPageSelected(int position){
            if(mSelectedFragment != null){
                mSelectedFragment.onWentOffScreen();
            }

            mSelectedFragment = null;

            Fragment fragment = getItem(position);
            if(fragment != null){
                mSelectedFragment = ((UniversalSearchBaseFragment)fragment);
                mSelectedFragment.onPresentedOnScreen();
            }
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                if(mInvoicesFragment == null){
                    mInvoicesFragment = new UniSearchTransactionFragment();
                    mInvoicesFragment.searchHolder = editText;
                }
                return mInvoicesFragment;
            }else if(position == 2){
                if(mProductsFragment == null){
                    mProductsFragment  = new UniSearchProductsFragment();
                    mProductsFragment.searchHolder = editText;
                }
                return mProductsFragment;
            }else if(position == 1){
                if(mAccountsFragment == null){
                    mAccountsFragment = new UniSearchAccountsFragment();
                    mAccountsFragment.searchHolder = editText;
                }
                return mAccountsFragment;
            }
            return null;
        }

        public void onSearchForText(String text){
            if(mAccountsFragment != null) mAccountsFragment.setSearchQuery(text);
            if(mProductsFragment != null) mProductsFragment.setSearchQuery(text);
            if(mInvoicesFragment != null) mInvoicesFragment.setSearchQuery(text);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Parcelable saveState() {
            Bundle bundle = new Bundle();
            if(mAccountsFragment!=null) bundle.putString("sp_account",mAccountsFragment.getTag());
            if(mProductsFragment!=null) bundle.putString("sp_product",mProductsFragment.getTag());
            if(mInvoicesFragment!=null) bundle.putString("sp_invoices",mInvoicesFragment.getTag());
            Log.d("BizApp","Save State: "+bundle.toString());
            return bundle;
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
            Bundle bundle = (Bundle)state;
            Log.d("BizApp","Load State: "+bundle.toString());
            String tag = bundle.getString("sp_account",null);
            if(tag!=null) mAccountsFragment = (UniversalSearchBaseFragment)getFragmentManager().findFragmentByTag(tag);
            tag = bundle.getString("sp_product",null);
            if(tag!=null) mProductsFragment = (UniversalSearchBaseFragment)getFragmentManager().findFragmentByTag(tag);
            tag = bundle.getString("sp_invoices",null);
            if(tag!=null) mInvoicesFragment = (UniversalSearchBaseFragment)getFragmentManager().findFragmentByTag(tag);

            Log.d("BizApp","State: Accounts: "+mAccountsFragment);
            Log.d("BizApp","State: Products: "+mProductsFragment);
            Log.d("BizApp","State: Invoices: "+mInvoicesFragment);
        }

    }

    @Override
    public void onFragmentOpened() {
        if(mPagerAdapter != null){
            if(mPagerAdapter.mAccountsFragment != null) mPagerAdapter.mAccountsFragment.onFragmentOpened();
            if(mPagerAdapter.mInvoicesFragment != null) mPagerAdapter.mInvoicesFragment.onFragmentOpened();
            if(mPagerAdapter.mProductsFragment != null) mPagerAdapter.mProductsFragment.onFragmentOpened();
        }
    }
}

