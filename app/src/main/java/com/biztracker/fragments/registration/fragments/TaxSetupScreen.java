package com.biztracker.fragments.registration.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.activities.RegistrationActivity;
import com.biztracker.common.Utilities;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.Company;
import com.biztracker.fragments.SuperFragment;

import java.util.Currency;
import java.util.Locale;

/**
 * Created by shubham on 16/04/16.
 */
public class TaxSetupScreen extends SetupFragment implements DatePickerDialog.OnDateSetListener, CurrencyChooserFrag.CurrencySelectionListener, DatabaseTalker.queryCompleteCallBack {

    private EditText taxationEditText;
    private TextView financialYearView;
    private TextView currencyView;

    private int financialYear = 2016;
    private int financialMonth = 4;
    private int financialDay = 1;

    private final static String months[] = new String[]{
            "Jan","Feb","March","April","May","June","July","Aug","Sept","Oct","Nov","Dec"
    };

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tax_setup,null);
        taxationEditText = (EditText)view.findViewById(R.id.taxation_et);
        financialYearView = (TextView)view.findViewById(R.id.financial_year);
        currencyView = (TextView) view.findViewById(R.id.currency);
        view.findViewById(R.id.next_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });

        if(mRegistrationObject != null){
            financialYear = (int)mRegistrationObject.financialYear;
            financialMonth = (int)mRegistrationObject.financialMonth;
            financialDay = (int)mRegistrationObject.financialDay;

            taxationEditText.setText(mRegistrationObject.taxationNumber);
            String dateString = getDateStringFrom((int)mRegistrationObject.financialYear,(int)mRegistrationObject.financialMonth,(int)mRegistrationObject.financialDay);
            financialYearView.setText(dateString);
        }

        financialYearView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),TaxSetupScreen.this,
                        financialYear,financialMonth - 1,financialDay);
                dialog.show();
            }
        });

        view.findViewById(R.id.currency_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CurrencyChooserFrag frag = new CurrencyChooserFrag();
                frag.delegate = TaxSetupScreen.this;
                frag.show(getActivity().getSupportFragmentManager(), "currency_chooser");
            }
        });

        Currency currentCurrency = null;

        TelephonyManager teleMgr = (TelephonyManager)getContext().getSystemService(Context.TELEPHONY_SERVICE);
        if(teleMgr != null){
            String isoCode = teleMgr.getSimCountryIso();
            if(isoCode == null) isoCode = teleMgr.getNetworkCountryIso();
            if(isoCode == null || isoCode.equals("")){
                currentCurrency = Currency.getInstance(Locale.getDefault());
            }else{
                Locale locale = new Locale("",isoCode);
                currentCurrency = Currency.getInstance(locale);
            }
        }else{
            currentCurrency = Currency.getInstance(Locale.getDefault());
        }
        if(mRegistrationObject == null){
            RegistrationActivity activity = (RegistrationActivity) getActivity();
            mRegistrationObject = activity.mRegistrationObject;
        }
        mRegistrationObject.currencyCode = currentCurrency.getCurrencyCode();
        currencyView.setText(currentCurrency.getSymbol());

        return view;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if(financialYearView != null){
            financialYearView.setText(getDateStringFrom(year,monthOfYear + 1,dayOfMonth));
        }
    }

    public String getDateStringFrom(int finYear,int finMonth, int finDay){
        financialYear = finYear;
        financialMonth = finMonth;
        financialDay = finDay;
        String dateString = String.format("%s %d, %d",months[finMonth - 1],finDay,finYear);
        return dateString;
    }

    private void next(){
        taxationEditText.clearFocus();
        Utilities.hideKeyboard(getActivity());
        if(mRegistrationObject != null){
            mRegistrationObject.taxationNumber = taxationEditText.getText().toString();
            mRegistrationObject.financialDay = financialDay;
            mRegistrationObject.financialMonth = financialMonth;
            mRegistrationObject.financialYear = financialYear;
        }
        //moveToNextIndex();
        setupDatabase();
        //Move To Next Screen
    }

    private void setupDatabase(){

        if(mRegistrationObject != null){

            Log.d("BizApp","Setting Up Database");

            Company company = new Company();
            company.emailAddress = mRegistrationObject.businessEmail;
            company.personsName = mRegistrationObject.userFullName;
            company.phone = mRegistrationObject.contactNumber;
            company.address = mRegistrationObject.businessAddress;
            company.taxationNumber = mRegistrationObject.taxationNumber;
            company.startingDate = financialYearView.getText().toString();
            company.businessWebsite = mRegistrationObject.businessWebsite;
            company.currency = Currency.getInstance(mRegistrationObject.currencyCode);
            company.businessName = mRegistrationObject.businessName;
            company.insertObject(this, 1, AsyncTask.SERIAL_EXECUTOR);

        }else{

            Log.d("BizApp","Problem While Setting Up Database");
        }

        //Move To Next Screen

    }


    @Override
    public void onCurrencySelected(String currencyCode) {
        mRegistrationObject.currencyCode = currencyCode;
        Currency currency = Currency.getInstance(currencyCode);
        currencyView.setText(currency.getSymbol());
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if(databaseObject != null && processId == 1){
            Company.company = (Company) databaseObject;
            mRegistrationObject.hasCompletedTaxSetupInformation = true;
            mRegistrationObject.storeInPreferences(getActivity().getApplicationContext());
            moveToNextIndex();
        }
    }
}
