package com.biztracker.fragments.registration.fragments;

import android.animation.ObjectAnimator;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.biztracker.CustomTextWatcher;
import com.biztracker.R;
import com.biztracker.activities.MainActivity;
import com.biztracker.common.Utilities;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.Company;
import com.biztracker.fragments.HomeScreen;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.views.KeyPreIMEEditText;
import com.biztracker.views.RobotoMediumEditText;

import io.fabric.sdk.android.services.concurrency.AsyncTask;

/**
 * Created by karan on 14/04/16.
 */
public class CompanySetupScreen extends SetupFragment implements KeyPreIMEEditText.OnKeyPreIMEKeyboardHiddenListener{


    View parentView;
    protected EditText personName, companyName, phoneNumber, emailField;
    protected RobotoMediumEditText addressField,businessWebsiteField;

    private LinearLayout llayout;

    protected View nextButton;
    private Handler mHandler;


    //Button startingDatePicker, save;
    //TODO: Create a Website Column Too

    String personNameString = "", companyNameString = "", phoneNumberString = "", addressString = "", emailFieldString = "", businessWebsiteString = "";

    @Override
    public boolean validateUIFields() {
        personNameString = personName.getText().toString().trim();
        companyNameString = companyName.getText().toString().trim();
        phoneNumberString = phoneNumber.getText().toString().trim();
        addressString = addressField.getText().toString().trim();
        emailFieldString = emailField.getText().toString().trim();
        businessWebsiteString = businessWebsiteField.getText().toString().trim();

        if(personNameString.isEmpty() || companyNameString.isEmpty()){
            return false;
        }

        return true;
    }


    private CustomTextWatcher mUserNameTextWatcher = new CustomTextWatcher(new CustomTextWatcher.CustomTextWatcherCallback() {
        @Override
        public void onSearchForText(String text) {
            personNameString = text.trim();
            onFieldsUpdated();
        }
    });

    private CustomTextWatcher mCompanyNameTextWatcher = new CustomTextWatcher(new CustomTextWatcher.CustomTextWatcherCallback() {
        @Override
        public void onSearchForText(String text) {
            companyNameString = text.trim();
            onFieldsUpdated();
        }
    });

    private CustomTextWatcher mAddressTextWatcher = new CustomTextWatcher(new CustomTextWatcher.CustomTextWatcherCallback() {
        @Override
        public void onSearchForText(String text) {
            addressString = text.trim();
            onFieldsUpdated();
        }
    });

    private void onFieldsUpdated(){
        if(personNameString.isEmpty() || companyNameString.isEmpty()){
            onHideNextButton();
        }else{
            onShowNextButton();
        }
    }

    public void onHideNextButton(){
        if(nextButton != null && mHandler != null)  mHandler.post(new Runnable() {
            @Override
            public void run() {
                nextButton.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void onShowNextButton(){
        if(nextButton != null && mHandler != null)  mHandler.post(new Runnable() {
            @Override
            public void run() {
                nextButton.setVisibility(View.VISIBLE);
            }
        });
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(mHandler == null) mHandler = new Handler();

        if(parentView == null) {
            parentView = inflater.inflate(R.layout.company_registration_layout, container, false);

            llayout = (LinearLayout) parentView.findViewById(R.id.llayout);

            personName = (EditText) llayout.findViewById(R.id.person_name_et);
            companyName = (EditText) llayout.findViewById(R.id.b_title_et);
            phoneNumber = (EditText) llayout.findViewById(R.id.contact_et);
            emailField = (EditText) llayout.findViewById(R.id.company_email_et);

            businessWebsiteField = (RobotoMediumEditText) llayout.findViewById(R.id.website_et);
            addressField = (RobotoMediumEditText) llayout.findViewById(R.id.address_et);

            personName.addTextChangedListener(mUserNameTextWatcher);
            companyName.addTextChangedListener(mCompanyNameTextWatcher);
            addressField.addTextChangedListener(mAddressTextWatcher);

            if(mRegistrationObject != null){
                personName.setText(mRegistrationObject.userFullName);
                companyName.setText(mRegistrationObject.businessName);
                phoneNumber.setText(mRegistrationObject.contactNumber);
                addressField.setText(mRegistrationObject.businessAddress);
                emailField.setText(mRegistrationObject.businessEmail);
                businessWebsiteField.setText(mRegistrationObject.businessWebsite);
            }

            nextButton = parentView.findViewById(R.id.next_btn);
            nextButton.setOnClickListener(onNextClickListener);

            businessWebsiteField.mKeyboardHiddenListener = this;
            businessWebsiteField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        int distanceY = v.getHeight() + v.getPaddingTop() + v.getPaddingBottom();
                        animateLinearLayoutToPosition(-distanceY);
                    }
                }
            });

            addressField.mKeyboardHiddenListener = this;
            addressField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        int distanceY = v.getHeight() * 2 + v.getPaddingTop()  + v.getPaddingBottom() * 2;
                        animateLinearLayoutToPosition(-distanceY);
                    }
                }
            });

            addressField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_NEXT){
                        animateLinearLayoutToPosition(0);
                        if(mHandler != null){
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                   addressField.clearFocus();
                                }
                            },250);
                        }
                    }
                    return false;
                }
            });

        }
        else{
            container.removeView(parentView);
        }
        return parentView;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("BizApp", "onConfigurationChanged: " + newConfig);
    }

    @Override
    public void onKeyboardHidden(final EditText editText) {
        if(mHandler != null){
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    animateLinearLayoutToPosition(0);
                    editText.clearFocus();
                }
            });
        }
    }

    public void animateLinearLayoutToPosition(final int verticalPosition){
        if(llayout == null) return;
        ObjectAnimator animator = ObjectAnimator.ofFloat(llayout, "translationY", verticalPosition);
        animator.setDuration(250);
        animator.start();
    }


    View.OnClickListener onNextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(validateUIFields() && Utilities.doesDatabaseExist(getContext(), companyNameString + ".bt") == false){
                if(mRegistrationObject != null){
                    mRegistrationObject.businessEmail = emailFieldString;
                    mRegistrationObject.businessName = companyNameString;
                    mRegistrationObject.userFullName = personNameString;
                    mRegistrationObject.businessAddress = addressString;
                    mRegistrationObject.businessWebsite = businessWebsiteString;
                    mRegistrationObject.contactNumber = phoneNumberString;
                    mRegistrationObject.hasCompletedBusinessRegistration = true;

                    mRegistrationObject.storeInPreferences(getActivity().getApplicationContext());
                    moveToNextIndex();
                }
            }
        }
    };

}
