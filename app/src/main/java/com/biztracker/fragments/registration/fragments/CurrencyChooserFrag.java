package com.biztracker.fragments.registration.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.biztracker.R;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by karan on 02/06/16.
 */

public class CurrencyChooserFrag extends DialogFragment implements AdapterView.OnItemClickListener {

    public interface CurrencySelectionListener{
        void onCurrencySelected(String currencyCode);
    }
    View parentView;
    ListView currencyList;
    List<String> currencies;
    HashMap<String, String> currencyMap;
    public CurrencySelectionListener delegate;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String currencyCode = currencyMap.get(currencies.get(position));
        this.delegate.onCurrencySelected(currencyCode);
        this.dismiss();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(parentView == null){
            parentView = inflater.inflate(R.layout.currency_view_layout, container, false);
            currencyList = (ListView) parentView.findViewById(R.id.currency_list);
            currencies = getAllCurrencies();
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, currencies);
            currencyList.setAdapter(adapter);
            currencyList.setOnItemClickListener(this);
        }
        else{
            container.removeView(parentView);
        }
        return parentView;
    }

    public List<String> getAllCurrencies()
    {
        List<String> toret = new ArrayList<>();
        Locale[] locs = Locale.getAvailableLocales();
        currencyMap = new HashMap<>();
        for(Locale loc : locs) {
            try {
                Currency currency = Currency.getInstance( loc );
                String currString = currency.getSymbol() + "     " + currency.getDisplayName();
                if(currencyMap.get(currString) == null) {
                    if(currency.getCurrencyCode().equals("INR") || currency.getCurrencyCode().equals("USD")){
                        toret.add(0, currString);
                    }
                    else{
                        toret.add(currString);
                    }
                    currencyMap.put(currString, currency.getCurrencyCode());
                }
            } catch(Exception exc)
            {
                // Locale not found
            }
        }

        return toret;
    }
}
