package com.biztracker.fragments.registration.fragments;

import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.registration.RegistrationObject;

/**
 * Created by shubham on 16/04/16.
 */
public abstract class SetupFragment extends SuperFragment {

    public interface OnSetupFragmentCallback{
        void showPagerFragmentAtIndex(int index);
        void onRegistrationCompleted();
    }

    protected RegistrationObject mRegistrationObject;
    private int mIndex = 0;
    public OnSetupFragmentCallback mCallback;


    public void setIndex(int index){
        mIndex = index;
    }

    public void setRegistrationObject(RegistrationObject registrationObject){
        mRegistrationObject = registrationObject;
    }

    public void moveToNextIndex(){
        if(mCallback != null){
            mCallback.showPagerFragmentAtIndex(mIndex+1);
        }
    }

    @Override
    public void onBackPressed() {
    }
}
