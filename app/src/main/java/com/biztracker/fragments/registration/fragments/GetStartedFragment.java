package com.biztracker.fragments.registration.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.R;
import com.biztracker.fragments.SuperFragment;

/**
 * Created by shubham on 13/04/16.
 */
public class GetStartedFragment extends SuperFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_get_started,null);
    }

    @Override
    public boolean validateUIFields() {
        return false;
    }

}
