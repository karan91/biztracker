package com.biztracker.fragments.registration;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by shubham on 16/04/16.
 */
public class RegistrationObject {

    public final static long ONE_HOUR = 60 * 60 * 1000;
    public final static long EIGHT_HOURS = 8 * ONE_HOUR;
    public final static long ONE_DAY = 24 * ONE_HOUR;
    public final static long FIFTEEN_DAYS = 15 * ONE_DAY;
    public final static long THIRTY_DAYS = 30 * ONE_DAY;

    public String userFullName = "";
    public String businessName = "";
    public String businessEmail = "";
    public String contactNumber = "";
    public String businessWebsite = "";
    public String businessAddress = "";
    public String currencyCode = "";
    public String taxationNumber = "";

    public long financialMonth = 4;  //April
    public long financialYear = 2016;    //2016
    public long financialDay = 1;    //1st

    public boolean allowedCameraAccess = false;

    public boolean hasCompletedBusinessRegistration = false;
    public boolean hasCompletedTaxSetupInformation = false;
    public boolean hasCompletedBarcodeScannerScreen = false;

    public boolean isOfflineSetupCompleted = false;

    public long lastUpdatedOnServer = 0;

    public long appInstalledOn = 0;
    public long appLastOpenedOn = 0;
    public long timesAppOpened = 0;
    public long lockAppAfter = 0;

    public boolean isAppLocked = false;
    public String lockMessage = "";
    public String purchaseAction = "";

    public String userid = "";

    public boolean isUserRegisteredOnServer = false;


    public RegistrationObject(Context context){
        SharedPreferences mPrivateRegistrationPrefs = context.getSharedPreferences("BizRegisPref", Activity.MODE_PRIVATE);
        if(mPrivateRegistrationPrefs.contains("install_reg_pref")){
            String jsonString = mPrivateRegistrationPrefs.getString("install_reg_pref",null);
            Log.d("BizApp","Loading Json: "+jsonString);
            if(jsonString != null){
                try{
                    JSONObject jObject = new JSONObject(jsonString);
                    setJsonObject(jObject);
                    return;
                }catch (Exception ex){
                    Log.e("BizApp","Exception: "+ex);
                }
            }
        }

        appInstalledOn = System.currentTimeMillis();
        appLastOpenedOn = System.currentTimeMillis();

    }

    public void storeInPreferences(Context context){
        SharedPreferences mPrivateRegistrationPrefs = context.getSharedPreferences("BizRegisPref", Activity.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mPrivateRegistrationPrefs.edit();
        JSONObject jsonObject = getJsonObject();
        if(jsonObject != null) {
            //Log.d("BizApp","Json: "+jsonObject.toString());
            mEditor.putString("install_reg_pref", jsonObject.toString());
            mEditor.commit();
        }else{
            Log.e("BizApp","Json Object is Null");
        }
    }

    public void onResume(){

    }

    public void onPause(){

    }

    public boolean shouldLockApp(){
        return isAppLocked;
        /*
        if(System.currentTimeMillis() > lockAppAfter){
            return true;
        }else{
            return false;
        }*/
    }

    private void setJsonObject(JSONObject jsonObject){
        Bundle bundle = new Bundle();
        Iterator<String> keys = jsonObject.keys();
        while (keys.hasNext()){
            String key = keys.next();
            try{
                Object object = jsonObject.get(key);
                if(object instanceof String){
                    bundle.putString(key,(String)object);
                }else if(object instanceof Integer){
                    bundle.putLong(key,Long.valueOf((Integer)object));
                }else if(object instanceof Long){
                    bundle.putLong(key,(Long)object);
                }else if(object instanceof Boolean){
                    bundle.putBoolean(key,(Boolean)object);
                }else if(object instanceof Double){
                    bundle.putDouble(key,(Double)object);
                }
            }catch (Exception ex){
                Log.e("BizApp","Registration Object: Exception: "+ex);
            }
        }

        //Log.d("BizApp","Bundle: "+bundle);
        setBundle(bundle);
    }

    private void setBundle(Bundle bundle){

        userFullName = bundle.getString("full_name_et");
        businessName = bundle.getString("bus_name_et");
        businessEmail = bundle.getString("bus_email_et");
        contactNumber = bundle.getString("bus_phone_et");
        businessWebsite = bundle.getString("bus_website_et");
        businessAddress = bundle.getString("bus_address_et");
        taxationNumber = bundle.getString("taxation_et");
        currencyCode = bundle.getString("currency_code");
        financialMonth = bundle.getLong("fin_month_et");
        financialYear = bundle.getLong("fin_year_et");
        financialDay = bundle.getLong("fin_day_et");

        allowedCameraAccess = bundle.getBoolean("has_camera_access");

        hasCompletedBusinessRegistration = bundle.getBoolean("setup_bus_registered");
        hasCompletedTaxSetupInformation = bundle.getBoolean("setup_tax_info");
        hasCompletedBarcodeScannerScreen = bundle.getBoolean("setup_barcode_scann");

        isOfflineSetupCompleted = bundle.getBoolean("setup_offline_completed");

        //Lock App
        appInstalledOn = bundle.getLong("app_installed_on",System.currentTimeMillis());
        appLastOpenedOn = bundle.getLong("app_last_opened_on",System.currentTimeMillis());
        lockAppAfter = bundle.getLong("app_lock_after", System.currentTimeMillis() + THIRTY_DAYS);
        timesAppOpened = bundle.getLong("times_app_opened",0);

        isAppLocked = bundle.getBoolean("is_app_locked",false);
        lockMessage = bundle.getString("lock_message","");
        purchaseAction = bundle.getString("purchase_action","");

        if(System.currentTimeMillis() - appLastOpenedOn > EIGHT_HOURS)  timesAppOpened++;

        isUserRegisteredOnServer = bundle.getBoolean("is_user_registerd_on_server",false);
        userid = bundle.getString("user_id", "");

        lastUpdatedOnServer = bundle.getLong("last_updated_on_server",0);
    }

    public Bundle getBundle(){
        Bundle bundle = new Bundle();
        bundle.putString("full_name_et",userFullName);
        bundle.putString("bus_name_et",businessName);
        bundle.putString("bus_email_et",businessEmail);
        bundle.putString("bus_phone_et",contactNumber);
        bundle.putString("bus_website_et",businessWebsite);
        bundle.putString("bus_address_et", businessAddress);
        bundle.putString("taxation_et", taxationNumber);
        bundle.putString("currency_code", currencyCode);
        bundle.putLong("fin_month_et", financialMonth);
        bundle.putLong("fin_year_et", financialYear);
        bundle.putLong("fin_day_et", financialDay);

        bundle.putBoolean("has_camera_access", allowedCameraAccess);

        bundle.putBoolean("setup_bus_registered", hasCompletedBusinessRegistration);
        bundle.putBoolean("setup_tax_info", hasCompletedTaxSetupInformation);
        bundle.putBoolean("setup_barcode_scann", hasCompletedBarcodeScannerScreen);

        bundle.putBoolean("setup_offline_completed", isOfflineSetupCompleted);

        //Lock App
        bundle.putLong("app_installed_on",appInstalledOn);
        bundle.putLong("app_last_opened_on",appLastOpenedOn);
        bundle.putLong("app_lock_after",lockAppAfter);
        bundle.putLong("times_app_opened", timesAppOpened);

        bundle.putBoolean("is_app_locked", isAppLocked);
        bundle.putString("lock_message", lockMessage);
        bundle.putString("purchase_action",purchaseAction);

        bundle.putBoolean("is_user_registerd_on_server", isUserRegisteredOnServer);
        bundle.putString("user_id", userid);

        bundle.putLong("last_updated_on_server",lastUpdatedOnServer);

        return bundle;
    }

    public JSONObject getJsonObject(){
        Bundle bundle = getBundle();
        JSONObject json = new JSONObject();
        Set<String> keys = bundle.keySet();
        for (String key : keys) {
            try {
                //Log.d("BizApp","key: "+key);
                json.put(key, JSONObject.wrap(bundle.get(key)));
            } catch(JSONException e) {
                Log.e("BizApp","Exception: "+e);
            }
        }
        return json;
    }

}
