package com.biztracker.fragments.registration.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.R;
import com.biztracker.activities.BaseActivity;
import com.biztracker.permissions.PermissionRequester;

/**
 * Created by shubham on 16/04/16.
 */
public class BarCodeSetupScreen extends SetupFragment implements PermissionRequester.OnPermissionRequesterCompleted{

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_barcode_setup,null);
        view.findViewById(R.id.allow_access).setOnClickListener(onAllowCameraClickListener);
        view.findViewById(R.id.next_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPermissionCancelled(null);
            }
        });
        return view;
    }

    public View.OnClickListener onAllowCameraClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            BaseActivity activity = (BaseActivity)getActivity();
            activity.requestPermission(PermissionRequester.Permissions.CAMERA,BarCodeSetupScreen.this);
        }
    };

    @Override
    public void onPermissionGranted(String permission) {
        BaseActivity activity = (BaseActivity)getActivity();
        //Move To Next Fragment
        if(mRegistrationObject != null){
            mRegistrationObject.allowedCameraAccess = true;
            mRegistrationObject.hasCompletedBarcodeScannerScreen = true;
            mRegistrationObject.isOfflineSetupCompleted = true;
            mRegistrationObject.storeInPreferences(activity.getApplicationContext());

            if(mCallback != null)   mCallback.onRegistrationCompleted();
        }
    }

    @Override
    public void onPermissionCancelled(String permission) {
        BaseActivity activity = (BaseActivity)getActivity();
        //Move To Next Fragment
        if(mRegistrationObject != null){
            mRegistrationObject.allowedCameraAccess = false;
            mRegistrationObject.hasCompletedBarcodeScannerScreen = true;
            mRegistrationObject.isOfflineSetupCompleted = true;
            mRegistrationObject.storeInPreferences(activity.getApplicationContext());

            if(mCallback != null)   mCallback.onRegistrationCompleted();
        }
    }
}
