package com.biztracker.fragments.registration;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


/**
 * Created by shubham on 14/04/16.
 */
public class BizTrackerInstallSettings {

    private final static String TAG = "BZAPP";

    public final static long ONE_HOUR = 60 * 60 * 1000;
    public final static long EIGHT_HOURS = 8 * ONE_HOUR;
    public final static long ONE_DAY = 24 * ONE_HOUR;
    public final static long FIFTEEN_DAYS = 15 * ONE_DAY;
    public final static long THIRTY_DAYS = 30 * ONE_DAY;

    public long appInstalledOn = 0;
    public long appOpenedOn = 0;
    public int timesAppOpened = 0;
    public boolean isAppLocked = false;
    public String gmailID = null;

    public boolean isUserRegistered = false;

    private static String KEY_APP_INSTALLED_ON = "appInstalledOn";
    private static String KEY_APP_LAST_OPENED_ON = "appLastOpenedOn";
    private static String KEY_APP_TIMES_OPENED = "appOpenedTimes";
    private static String KEY_IS_APP_LOCK = "bizPloID";  //lazy means (App is Locked) or crazy means (App is Unlocked)
    private static String KEY_USER_GMAIL_ID = "userGmailID";

    private SharedPreferences mPrivateAppPreferences = null;

    private final static String PRIVATE_PREF_NAME = "BizInstallPref";

    public BizTrackerInstallSettings(Context context){

        mPrivateAppPreferences = context.getSharedPreferences(PRIVATE_PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrivateAppPreferences.edit();

        if(mPrivateAppPreferences.contains(KEY_USER_GMAIL_ID)){
            gmailID = mPrivateAppPreferences.getString(KEY_USER_GMAIL_ID,null);
        }else{
            gmailID = null;
        }

        if(gmailID != null && gmailID.length() != 0){
            isUserRegistered = true;
        }

        if(mPrivateAppPreferences.contains(KEY_APP_INSTALLED_ON)){
            appInstalledOn = mPrivateAppPreferences.getLong(KEY_APP_INSTALLED_ON, System.currentTimeMillis());
        }else{
            appInstalledOn = System.currentTimeMillis();
            editor.putLong(KEY_APP_INSTALLED_ON,appInstalledOn);
        }

        if(mPrivateAppPreferences.contains(KEY_APP_LAST_OPENED_ON)){
            appOpenedOn = mPrivateAppPreferences.getLong(KEY_APP_LAST_OPENED_ON, System.currentTimeMillis());
        }else{
            appOpenedOn = System.currentTimeMillis();
            editor.putLong(KEY_APP_LAST_OPENED_ON,appOpenedOn);
        }

        if(mPrivateAppPreferences.contains(KEY_APP_TIMES_OPENED)){
            timesAppOpened = mPrivateAppPreferences.getInt(KEY_APP_TIMES_OPENED, 0);
        }else{
            timesAppOpened = 1;
            editor.putInt(KEY_APP_TIMES_OPENED,timesAppOpened);
        }

        String appLockValue = null;

        if(mPrivateAppPreferences.contains(KEY_IS_APP_LOCK)){
            appLockValue = mPrivateAppPreferences.getString(KEY_IS_APP_LOCK,"lazy");
        }else{
            appLockValue = "crazy";
        }

        if(System.currentTimeMillis() - appOpenedOn >= EIGHT_HOURS){
            timesAppOpened++;
            editor.putLong(KEY_APP_LAST_OPENED_ON,System.currentTimeMillis());
            editor.putInt(KEY_APP_TIMES_OPENED,timesAppOpened);
        }

        isAppLocked = appLockValue.contentEquals("crazy")?false:true;

        if(isAppLocked == false){
            if(System.currentTimeMillis() - appInstalledOn >= THIRTY_DAYS){
                isAppLocked = true;
            }
        }

        editor.putString(KEY_IS_APP_LOCK, (isAppLocked) ? "lazy" : "crazy");
        editor.commit();

        Log.d("BIZSET","Locked: "+((isAppLocked)?"true":"false"));
        Log.d("BIZSET","Times: "+timesAppOpened);
    }

    public void storeInContext(){

        SharedPreferences.Editor editor = mPrivateAppPreferences.edit();
        editor.putString(KEY_USER_GMAIL_ID,gmailID);
        editor.commit();

    }

}
