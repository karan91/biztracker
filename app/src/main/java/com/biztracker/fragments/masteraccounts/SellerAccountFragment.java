package com.biztracker.fragments.masteraccounts;

/**
 * Created by shubham on 02/05/16.
 */
public class SellerAccountFragment extends MasterAccountFragment{
    public SellerAccountFragment(){
        mFragmentTag = "SellerAccountFragment";
        this.setAsSupplierAccount();
    }
}
