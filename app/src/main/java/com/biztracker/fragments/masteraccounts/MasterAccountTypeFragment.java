package com.biztracker.fragments.masteraccounts;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.biztracker.R;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.MasterAccountType;
import com.biztracker.fragments.SuperFragment;

/**
 * Created by Karan on 28/09/15.
 */
public class MasterAccountTypeFragment extends SuperFragment implements DatabaseTalker.queryCompleteCallBack{


    View view;
    RadioGroup invoiceRelated;
    RadioGroup defaultNature;
    EditText typeName;
    private static MasterAccountType accountType;


    public static MasterAccountTypeFragment loadWithAccountType(MasterAccountType type){
        accountType = type;
        MasterAccountTypeFragment fragment = new MasterAccountTypeFragment();
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.master_account_type_addition, container, false);
        invoiceRelated = (RadioGroup) view.findViewById(R.id.invoice_related);
        defaultNature = (RadioGroup) view.findViewById(R.id.default_nature);
        typeName = (EditText) view.findViewById(R.id.master_account_type_et);
        typeName.setText("Karanveer singh's my name....");
        if(accountType != null) loadUIFields();
        return view;
    }

    private void loadUIFields() {
        typeName.setText(accountType.name);
        if(accountType.defaultNature.equals("dr"))  defaultNature.check(R.id.type_debit_radio);
        else    defaultNature.check(R.id.type_credit_radio);
        if(accountType.billable == 0)   invoiceRelated.check(R.id.invoice_related_no);
        else    invoiceRelated.check(R.id.invoice_related_yes);
    }

    @Override
    public boolean validateUIFields() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save:
                if(validateUIFields()){
                    if(accountType == null) accountType = new MasterAccountType();
                    accountType.name = typeName.getText().toString().trim();

                    if(defaultNature.getCheckedRadioButtonId() == R.id.type_debit_radio)    accountType.defaultNature = "dr";
                    else if(defaultNature.getCheckedRadioButtonId() == R.id.type_credit_radio)  accountType.defaultNature = "cr";

                    if(invoiceRelated.getCheckedRadioButtonId() == R.id.invoice_related_yes)    accountType.billable = 1;
                    else if(invoiceRelated.getCheckedRadioButtonId() == R.id.invoice_related_no)    accountType.billable = 0;

                    accountType.insertObject(this, 1, AsyncTask.THREAD_POOL_EXECUTOR);
                }
                break;
        }

        return true;
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if(processId == 1){
            if(databaseObject instanceof MasterAccountType) Toast.makeText(getActivity(), "Account Type Created", Toast.LENGTH_SHORT).show();
        }
    }
}
