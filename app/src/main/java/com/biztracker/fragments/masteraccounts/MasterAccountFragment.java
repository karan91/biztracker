package com.biztracker.fragments.masteraccounts;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.biztracker.CustomTextWatcher;
import com.biztracker.R;
import com.biztracker.common.DatabaseObjectAdapter;
import com.biztracker.contacts_reader.Contact;
import com.biztracker.contacts_reader.ContactsReader;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.DatabaseObject;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.MasterAccountType;
import com.biztracker.fragments.InputFragment;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.utilities.Session;
import com.biztracker.views.RobotoMediumTextView;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Karan on 28/09/15.
 */
public abstract class MasterAccountFragment extends InputFragment implements DatabaseTalker.queryCompleteCallBack {


    View view;
    AutoCompleteTextView name;
    EditText email, phone, address, taxNumber, openingBalance;
    Spinner accountType;
    DatabaseObjectAdapter accountTypeAdapter;
    ArrayList<DatabaseObject> accountTypes;
    RobotoMediumTextView saveButton;
    boolean editingMasterAccount = false;
    private MasterAccount account;
    private View dummyFocusableView;
    private TextView headerView;

    private boolean isCustomerAccount = false;
    private boolean isSupplierAccount  = false;


    public interface MasterAccountFragmentDelegate{
        void onMasterAccountCreated(MasterAccountFragment fragment);
    }

    public MasterAccountFragmentDelegate delegate;
    @Override
    public boolean validateUIFields() {

        if(name.getText().toString().trim().equals("")){
            Toast.makeText(getActivity(), "Name cannot be empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public MasterAccountFragment(){}

    public void setAsCustomerAccount(){
        isCustomerAccount = true;
        isSupplierAccount = false;

        if(accountType != null) {
            accountType.setVisibility(View.GONE);
        }

        if(headerView != null){
            headerView.setText("Add Customer");
        }
        this.clearAllFields();
    }

    public void setAsSupplierAccount(){
        isSupplierAccount = true;
        isCustomerAccount = false;

        if(accountType != null) {
            accountType.setVisibility(View.GONE);
        }

        if(headerView != null){
            headerView.setText("Add Supplier");
        }
        this.clearAllFields();
    }

    public void setAsOtherAccount(){
        isSupplierAccount = false;
        isCustomerAccount = false;

        if(accountType != null) {
            accountType.setVisibility(View.VISIBLE);
        }

        if(headerView != null){
            headerView.setText("Add Account");
        }
    }

    public void setMasterAccount(MasterAccount masterAccount){
        this.account = masterAccount;
        this.editingMasterAccount = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(isEditingFragment()){
            view = inflater.inflate(R.layout.fragment_editing_master_account, container, false);
        }else {
            view = inflater.inflate(R.layout.fragment_master_account, container, false);
        }

        if(Session.mContactsReader == null){
            Session.mContactsReader = new ContactsReader(getMainActivity());
        }

        dummyFocusableView = view.findViewById(R.id.focusable_dummy_view);
        name = (AutoCompleteTextView) view.findViewById(R.id.account_name_et);
        openingBalance = (EditText) view.findViewById(R.id.opening_balance);
        email = (EditText)view.findViewById(R.id.email_et);
        phone = (EditText)view.findViewById(R.id.phone_et);
        address = (EditText)view.findViewById(R.id.address_et);
        taxNumber = (EditText)view.findViewById(R.id.taxation_et);
        saveButton = (RobotoMediumTextView) view.findViewById(R.id.save_btn);
        saveButton.setOnClickListener(saveAccountListener);
        accountType = (Spinner) view.findViewById(R.id.master_account_type_sp);

        headerView = ((TextView)view.findViewById(R.id.header));

        name.setAdapter(new ContactsAutoCompleteAdapter(getContext()));
        name.setOnItemClickListener(mOnContactItemClickListener);

        if(isCustomerAccount || isSupplierAccount){
            accountType.setVisibility(View.GONE);
            if(isCustomerAccount){
                headerView.setText("Add Customer");
            }else{
                headerView.setText("Add Supplier");
            }
        }else {
            headerView.setText("Add Account");
            accountType.setVisibility(View.VISIBLE);
        }

        if(isEditingFragment()){
            ImageView backButton = (ImageView) view.findViewById(R.id.back_btn);
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNativeBackPress();
                }
            });
            backButton.setImageResource(R.drawable.ic_keyboard_backspace_white_24dp);
            ((TextView)view.findViewById(R.id.header)).setText("Edit Account");

            if(isCustomerAccount == false && isSupplierAccount == false){
                accountType.setVisibility(View.GONE);
            }

            TextView accountTypeEt = (TextView)view.findViewById(R.id.account_type_et);
            if(isCustomerAccount){
                accountTypeEt.setText("Type: Customer");
            }else if(isSupplierAccount){
                accountTypeEt.setText("Type: Supplier");
            }else{
                if(account != null && account.accountType != null) {
                    accountTypeEt.setText("Type: "+account.accountType.name);
                }
            }
        }

        //name.addTextChangedListener(new CustomTextWatcher(mNameWatcherCallback));

        if(savedInstanceState != null){
            String accountId = savedInstanceState.getString("account_id");
            if(accountId != null && accountId.contentEquals("-1") == false){
                DatabaseTalker.getInstance().getMasterAccount(accountId, this, 5, AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }

        loadAccountTypeSpinner();

        if(this.account != null){
            loadUIFields();
        }

        return view;
    }

    private AdapterView.OnItemClickListener mOnContactItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Object object = view.getTag();
            if(object != null && object instanceof Contact){
                Contact contact = (Contact) object;
                phone.setText(contact.phoneNo);
                email.setText(contact.email);
            }
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(account!=null) outState.putString("account_id",this.account.id);
    }

    protected boolean isEditingFragment(){
        return false;
    }

    private void loadAccountTypeSpinner() {
        DatabaseTalker.getInstance().getAllMasterAccountTypes(this, 1, null);
    }

    private void loadUIFields() {
        name.setText(account.name);
        email.setText(account.email);
        phone.setText(account.phone);
        address.setText(account.address);
        taxNumber.setText(account.taxationNumber);
        openingBalance.setText(account.openingBalance.toString());
        int i = 0;
        for(DatabaseObject type : accountTypes){
            if(type.name.equals(account.accountType.name))  {
                accountType.setSelection(i);
                break;
            }
            i++;
        }
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if(processId == 1){
            if(databaseObject != null){
                accountTypes = (ArrayList<DatabaseObject>) databaseObject;
                accountTypeAdapter = new DatabaseObjectAdapter(getActivity(), android.R.layout.simple_list_item_1, accountTypes, null);
                accountTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                accountType.setAdapter(accountTypeAdapter);
            }
        }
        else if(processId == 2){
            if(editingMasterAccount && databaseObject != null){
                View view = this.getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                this.onNativeBackPress();
                Toast.makeText(getActivity(), "Account Updated", Toast.LENGTH_SHORT).show();
            }
            else{
                View view = this.getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                this.clearAllFields();
                if(databaseObject instanceof MasterAccount) {
                    Toast.makeText(getActivity(), "New Account created", Toast.LENGTH_SHORT).show();
                    if(this.delegate != null){
                        this.delegate.onMasterAccountCreated(this);
                    }
                }
            }
        }
        else if(processId == 3){
            if((boolean)databaseObject == false){
                account = new MasterAccount();
                saveAccountListener.onClick(null);
            }
            else{
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                alertBuilder.setTitle("Account Exists");
                alertBuilder.setMessage(name.getText().toString().trim() + " already exists");
                alertBuilder.setPositiveButton("OK", null);
                alertBuilder.show();
            }
        }

        else if(processId == 5 && databaseObject != null){
            this.account = (MasterAccount) databaseObject;
            loadUIFields();
        }
    }

    View.OnClickListener saveAccountListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(validateUIFields()){
                if(account == null){
                    DatabaseTalker.getInstance().doesMasterAccountExistForName(name.getText().toString().trim(), MasterAccountFragment.this, 3, AsyncTask.THREAD_POOL_EXECUTOR);
                    return;
                }
                account.name = name.getText().toString().trim();
                if(!editingMasterAccount) {
                    if (isCustomerAccount) {
                        MasterAccountType type = new MasterAccountType();
                        type.id = "1";
                        account.accountType = type;
                    } else if (isSupplierAccount) {
                        MasterAccountType type = new MasterAccountType();
                        type.id = "2";
                        account.accountType = type;
                    } else {
                        account.accountType = (MasterAccountType) accountType.getSelectedItem();
                    }
                }
                account.email = email.getText().toString().trim();
                account.phone = phone.getText().toString().trim();
                account.address = address.getText().toString().trim();
                account.taxationNumber = taxNumber.getText().toString().trim();
                if(openingBalance.getText().toString().trim().equals("")){
                    account.setOpeningBalance(new BigDecimal(0));
                    //account.openingBalance = new BigDecimal(0);
                }
                else{
                    //account.openingBalance = new BigDecimal(openingBalance.getText().toString().trim());
                    account.setOpeningBalance(new BigDecimal(openingBalance.getText().toString().trim()));
                }
                account.insertObject(MasterAccountFragment.this, 2, AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    };

    private void clearAllFields(){
        if(name != null) {
            name.setText("");
            email.setText("");
            phone.setText("");
            address.setText("");
            taxNumber.setText("");
            openingBalance.setText("");
            this.account = null;
            dummyFocusableView.requestFocus();
        }
    }

    @Override
    public void resetTransactionAndUIFields() {
        clearAllFields();
    }
}
