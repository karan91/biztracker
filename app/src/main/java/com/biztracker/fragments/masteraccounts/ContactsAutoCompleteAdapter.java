package com.biztracker.fragments.masteraccounts;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.contacts_reader.Contact;
import com.biztracker.contacts_reader.ContactsReader;

import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham on 24/06/16.
 */
public class ContactsAutoCompleteAdapter extends BaseAdapter implements Filterable{

    private List<Contact> mItems;
    private LayoutInflater mInflater;

    public ContactsAutoCompleteAdapter(Context context){
        mItems = new ArrayList();
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int i) {
        if(i < mItems.size() && i>=0 )
            return mItems.get(i);
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = mInflater.inflate(R.layout.contact_suggestion_item_cell,null);
        }

        TextView textView = (TextView) view.findViewById(R.id.name_view);
        Contact contact = mItems.get(i);
        if(contact.name != null){
            textView.setText(WordUtils.capitalize(contact.name));
        }else {
            textView.setText(contact.name);
        }
        //TODO: Capitalize Name


        view.setTag(contact);

        return view;
    }

    @Override
    public Filter getFilter() {
        return mContactFilter;
    }


    Filter mContactFilter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            if(resultValue == null) return "";
            Contact contact = (Contact)resultValue;
            if(contact.name == null) return "";
            return WordUtils.capitalize(contact.name);
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if(charSequence != null && charSequence.length() > 0) {
                List<Contact> contacts = ContactsReader.lookupContactsForText(charSequence.toString());

                if(contacts != null && contacts.size() > 0) {
                    FilterResults mFilterResults = new FilterResults();
                    mFilterResults.count = contacts.size();
                    mFilterResults.values = contacts;
                    return mFilterResults;
                }
            }

            return new FilterResults();
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if(filterResults.count > 0){
                mItems = (List<Contact>)filterResults.values;
                notifyDataSetChanged();
            }else{
                mItems = new ArrayList();
                //notifyinvalidate
                notifyDataSetInvalidated();
            }
        }
    };

}
