package com.biztracker.fragments.masteraccounts;

/**
 * Created by shubham on 21/06/16.
 */
public class CustomerAccountEditingFragment extends CustomerAccountFragment {

    public CustomerAccountEditingFragment(){
    }

    @Override
    protected boolean isEditingFragment() {
        return true;
    }
}
