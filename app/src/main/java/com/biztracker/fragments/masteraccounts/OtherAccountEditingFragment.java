package com.biztracker.fragments.masteraccounts;

/**
 * Created by shubham on 21/06/16.
 */
public class OtherAccountEditingFragment extends OtherAccountFragment {

    public OtherAccountEditingFragment(){
    }

    @Override
    protected boolean isEditingFragment() {
        return true;
    }
}
