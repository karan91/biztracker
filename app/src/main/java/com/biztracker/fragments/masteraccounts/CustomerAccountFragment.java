package com.biztracker.fragments.masteraccounts;

/**
 * Created by shubham on 02/05/16.
 */
public class CustomerAccountFragment extends MasterAccountFragment {
    public CustomerAccountFragment(){
        mFragmentTag = "CustomerAccountFragment";
        this.setAsCustomerAccount();
    }
}
