package com.biztracker.fragments.masteraccounts;

/**
 * Created by shubham on 02/05/16.
 */
public class OtherAccountFragment extends MasterAccountFragment {
    public OtherAccountFragment(){
        mFragmentTag = "OtherAccountFragment";
        this.setAsOtherAccount();
    }
}
