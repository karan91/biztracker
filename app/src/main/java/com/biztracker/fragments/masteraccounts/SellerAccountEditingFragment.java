package com.biztracker.fragments.masteraccounts;

/**
 * Created by shubham on 21/06/16.
 */
public class SellerAccountEditingFragment extends SellerAccountFragment {

    public SellerAccountEditingFragment(){
    }

    @Override
    protected boolean isEditingFragment() {
        return true;
    }
}
