package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

/**
 * Created by karan on 10/04/16.
 */
public class SimpleItemTouchHelper {

    ItemTouchHelperAdapter adapter;
    public SimpleItemTouchHelper(ItemTouchHelperAdapter adapter){
        this.adapter =  adapter;
    }
    public interface ItemTouchHelperAdapter {
        boolean onItemMove(int fromPosition, int toPosition);
        void onItemDismiss(int position);
    }
}
