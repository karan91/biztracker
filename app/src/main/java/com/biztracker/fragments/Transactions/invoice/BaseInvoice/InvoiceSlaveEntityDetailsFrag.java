package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.Spannable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.common.Utilities;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.SlaveEntity;
import com.biztracker.fragments.SuperFragment;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by karan on 25/03/16.
 */
public class InvoiceSlaveEntityDetailsFrag extends SuperFragment implements AdapterView.OnItemClickListener, TextView.OnEditorActionListener, TextWatcher {
    SlaveEntity slaveEntity;
    View rootView;
    ListView productListView;
    ArrayList<Product> products;
    HashMap<Product, Boolean> productSelectedMap;
    List<SlaveEntity> slaveEntities;
    CustomAdapter adapter;
    Button doneButton;
    BigDecimal tempPercentValue;
    SlaveEntityDetailsFragListener delegate;

    private TextView saveView;

    EditText taxDiscountRateView;
    TextView taxDiscountNameView;
    TextView totalTaxDiscountView;

    private boolean changed = false;

    @Override
    public boolean validateUIFields() {
        return false;
    }

    public void setSlaveEntity(SlaveEntity slaveEntity) {
        this.slaveEntity = slaveEntity;
        if(taxDiscountNameView != null){
            taxDiscountNameView.setText(slaveEntity.name);
        }
        if(taxDiscountRateView != null){
            taxDiscountRateView.setText(slaveEntity.getValue().toString());
        }
    }

    public void setProducts(ArrayList<Product> products){
        if(this.products == null){
            this.products = new ArrayList<>();
        }
        else{
            this.products.clear();
        }
        this.products.addAll(products);
        productSelectedMap = new HashMap<>();
        for(Product p : products){
            if(slaveEntity.productList == null){
                productSelectedMap.put(p, false);
            }
            else{
                productSelectedMap.put(p, slaveEntity.productList.contains(p));
            }
        }
    }

    public void setSlaveEntities(List<SlaveEntity> slaveEntities) {
        this.slaveEntities = slaveEntities;
    }

    private void setTotalValueHolder(){
        BigDecimal total = new BigDecimal(0);
        for(Product p : products){
            boolean selection = productSelectedMap.get(p);
            if(selection){
                BigDecimal productValueAtInstance = slaveEntity.getInvoiceValueOfProduct(p);
                total = total.add(slaveEntity.getTentativeValueForProductValue(productValueAtInstance));
            }
        }
        totalTaxDiscountView.setText(Utilities.getFormattedValue(total, slaveEntity.nature));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView == null) {
            rootView = inflater.inflate(R.layout.invoice_slave_entity_details_frag, container, false);
            productListView = (ListView) rootView.findViewById(R.id.invoice_product_list);
            productListView.setOnItemClickListener(this);
            adapter = new CustomAdapter(getActivity(), R.layout.selectable_product_layout, this.products);
            this.productListView.setAdapter(adapter);

            taxDiscountRateView = (EditText)rootView.findViewById(R.id.tax_discount_rate);
            taxDiscountRateView.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            taxDiscountRateView.setOnEditorActionListener(this);
            taxDiscountNameView = (TextView)rootView.findViewById(R.id.tax_discount_name);
            totalTaxDiscountView = (TextView)rootView.findViewById(R.id.total_tax_discount_value);
            if(this.slaveEntity != null){
                taxDiscountNameView.setText(slaveEntity.name);
                tempPercentValue = slaveEntity.getValue().add(new BigDecimal(0));
                taxDiscountRateView.setText(slaveEntity.getValue().toString());
            }

            saveView = (TextView) rootView.findViewById(R.id.save_btn);
            taxDiscountRateView.addTextChangedListener(this);
            rootView.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNativeBackPress();
                }
            });


            rootView.findViewById(R.id.clear_all_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChanged();
                    setCheckedForAllProducts(false);
                }
            });

            rootView.findViewById(R.id.select_all_btn).setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    onChanged();
                    setCheckedForAllProducts(true);
                }
            });

            rootView.findViewById(R.id.save_btn).setOnClickListener(onSaveClickListener);


        }
        else{
            container.removeView(rootView);
            this.adapter.notifyDataSetChanged();
        }
        setTotalValueHolder();

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    private void setCheckedForAllProducts(boolean checked){
        if(this.productSelectedMap == null){
            productSelectedMap = new HashMap<>();
        }
        for(Product p: products){
            productSelectedMap.put(p,checked);
        }
        adapter.notifyDataSetChanged();
        setTotalValueHolder();
    }

    public void onChanged(){
        if(this.changed){
        }else{
            this.changed = true;
            if(this.saveView != null) saveView.setVisibility(View.VISIBLE);
        }
    }

    public View.OnClickListener onSaveClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {

            changed = false;
            if(saveView!=null) saveView.setVisibility(View.GONE);

            if(slaveEntity.productList == null){
                slaveEntity.productList = new ArrayList<>();
            }
            for(Product p : products){
                if(productSelectedMap.get(p)){
                    if(!slaveEntity.productList.contains(p)){
                        slaveEntity.productList.add(p);
                    }
                }
                else{
                    slaveEntity.productList.remove(p);
                }
            }
            if(!slaveEntity.parentList.contains(slaveEntity)){
                slaveEntity.parentList.add(slaveEntity);
            }
            delegate.onSlaveEntityAddOrUpdated(slaveEntity);
            tempPercentValue = null;
            navigateToParentFragment();
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onChanged();
        Product p = products.get(position);
        boolean previousSelection = productSelectedMap.get(p);
        productSelectedMap.put(p, !previousSelection);
        this.adapter.notifyDataSetChanged();
        setTotalValueHolder();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(tempPercentValue != null){
            slaveEntity.setPercentageValue(tempPercentValue);
        }
        if(taxDiscountRateView.hasFocus()){
            Utilities.hideKeyboard(getActivity());
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        //v.clearFocus();
        Utilities.hideKeyboard(getActivity());
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable v) {
        onChanged();
        BigDecimal value;
        if(v.toString().trim().equals("")){
            value = new BigDecimal(0);
        }
        else{
            value = new BigDecimal(v.toString().trim());
        }
        slaveEntity.setPercentageValue(value);
        this.adapter.notifyDataSetChanged();
        setTotalValueHolder();
    }

    private class CustomAdapter extends ArrayAdapter<Product> {
        Context context;

        public CustomAdapter(Context context, int resource, List<Product> objects) {
            super(context, resource, objects);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ProductHolder holder;
            Product p = products.get(position);
            if(convertView == null){
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                convertView = inflater.inflate(R.layout.selectable_product_layout, parent, false);
                holder = new ProductHolder();
                holder.productTv = (TextView) convertView.findViewById(R.id.product_tv);
                holder.selectedCheckBox = (CheckBox) convertView.findViewById(R.id.product_check);
                holder.productValue = (TextView)convertView.findViewById(R.id.product_value);
                holder.calculatedTextValue = (TextView)convertView.findViewById(R.id.calculated_tax_value);
                convertView.setTag(holder);
            }

            else{
                holder = (ProductHolder) convertView.getTag();
            }
            holder.selectedCheckBox.setTag(position);
            holder.productTv.setText(p.name);
            boolean selection = productSelectedMap.get(p);
            holder.selectedCheckBox.setChecked(selection);

            BigDecimal productValueAtInstance = slaveEntity.getInvoiceValueOfProduct(p);
            holder.productValue.setText("Value: " + productValueAtInstance.toString());
            if(selection) {
                Spannable slaveEntityValueForProduct = slaveEntity.getFormattedTentativeValueForProductValue(productValueAtInstance);
                holder.calculatedTextValue.setText(slaveEntityValueForProduct);
            }
            else{
                holder.calculatedTextValue.setText("-");
            }
            return convertView;
        }
    }

    static class ProductHolder{
        CheckBox selectedCheckBox;
        TextView productTv;
        TextView productValue;
        TextView calculatedTextValue;
    }

    interface SlaveEntityDetailsFragListener{
        void onSlaveEntityAddOrUpdated(SlaveEntity slaveEntity);
    }
}
