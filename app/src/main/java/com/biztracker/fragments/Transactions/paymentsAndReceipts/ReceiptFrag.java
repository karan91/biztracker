package com.biztracker.fragments.Transactions.paymentsAndReceipts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.PaymentReceiptTransaction;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.AccountListFrag;

import java.lang.ref.WeakReference;

/**
 * Created by karan on 09/06/16.
 */

public class ReceiptFrag extends PaymentReceiptFrag {

    @Override
    public void onAccountSelected(MasterAccount account, WeakReference<Object> tag) {
        if(tag.get() == debitName){
            this.transaction.creditAccount = account;
            this.debitName.setText(transaction.creditAccount.name);
        }
        else if(tag.get() == creditName){
            this.transaction.debitAccount = account;
            this.creditName.setText(transaction.debitAccount.name);
        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView)parentView.findViewById(R.id.textView5)).setText("Payment From:");
        ((TextView)parentView.findViewById(R.id.header)).setText("Receive Payment");
    }

    @Override
    public void loadTransaction(PaymentReceiptTransaction transaction) {
        super.loadTransaction(transaction);
        if(transaction != null){
            debitName.setText(transaction.creditAccount.name);
            creditName.setText(transaction.debitAccount.name);
        }
    }
}
