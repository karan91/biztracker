package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by karan on 02/01/16.
 */
public class RecyclerViewLayoutManager extends LinearLayoutManager {
    public RecyclerViewLayoutManager(Context context) {
        super(context);
    }

    @Override
    public boolean supportsPredictiveItemAnimations() {
        return true;
    }
}
