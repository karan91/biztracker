package com.biztracker.fragments.Transactions.invoice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.R;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.InvoiceFrag;

/**
 * Created by shubham on 03/06/16.
 */
public class ViewableSalesInvoiceFrag extends InvoiceFrag {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater,container,savedInstanceState);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNativeBackPress();
            }
        });
        backButton.setImageResource(R.drawable.ic_keyboard_backspace_white_24dp);
        return view;
    }

    @Override
    protected String getTitle() {
        return "Sales Invoice";
    }
}
