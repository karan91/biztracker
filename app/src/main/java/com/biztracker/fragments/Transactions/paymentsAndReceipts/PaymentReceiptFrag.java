package com.biztracker.fragments.Transactions.paymentsAndReceipts;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.biztracker.R;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.PaymentReceiptTransaction;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.fragments.InputFragment;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.AccountListFrag;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by karan on 16/04/16.
 */
public class PaymentReceiptFrag extends InputFragment implements View.OnClickListener, AccountListFrag.AccountSetDelegate, DatePickerDialog.OnDateSetListener, DatabaseTalker.queryCompleteCallBack {



    protected View parentView;
    protected PaymentReceiptTransaction transaction;

    public TextView debitName;
    public TextView creditName;
    public TextView dateView;
    public EditText noteView;
    public EditText amountView;
    protected ImageView backButton;

    @Override
    public boolean validateUIFields() {

        String amount = amountView.getText().toString().trim();
        transaction.amount = new BigDecimal(amount);
        if(transaction.debitAccount == null){
            return false;
        }
        else if(transaction.creditAccount == null){
            return false;
        }
        else if(amount.isEmpty()){
            return false;
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(parentView == null){
            parentView = inflater.inflate(R.layout.payment_reciept_layout, container, false);
            parentView.findViewById(R.id.payment_to_button).setOnClickListener(this);
            parentView.findViewById(R.id.payment_from_button).setOnClickListener(this);
            parentView.findViewById(R.id.save_btn).setOnClickListener(this);

            backButton = (ImageView)parentView.findViewById(R.id.back_btn);

            //parentView.findViewById(R.id.payment_amount_button).setOnClickListener(this);
            parentView.findViewById(R.id.date_button).setOnClickListener(this);
            debitName = (TextView) parentView.findViewById(R.id.debit_name_holder);
            creditName = (TextView) parentView.findViewById(R.id.credit_name_holder);
            dateView = (TextView) parentView.findViewById(R.id.date_view);
            noteView = (EditText) parentView.findViewById(R.id.note_area);
            amountView = (EditText) parentView.findViewById(R.id.amount_et);

            if(this.transaction == null && savedInstanceState != null && savedInstanceState.getString("transaction_id") != null){
                {
                    String transactionId = savedInstanceState.getString("transaction_id");
                    if (transactionId.equals("-1")) {
                        loadTransaction(null);
                    }
                    else{
                        DatabaseTalker.getInstance().getPaymentReceiptTransactionUsingCallback(this, 5, AsyncTask.THREAD_POOL_EXECUTOR, transactionId);
                    }
                }
            }
            else{
                loadTransaction(this.transaction);
            }
        }
        else{
            container.removeView(parentView);
        }
        return parentView;
    }

    public void setTransaction(PaymentReceiptTransaction transaction) {
        this.transaction = transaction;
    }

    public PaymentReceiptTransaction getTransaction() {
        return transaction;
    }

    @Override
    public void onClick(View v) {
        if(v.getTag().equals("3")){
            Calendar calendar = Calendar.getInstance();
            if(this.transaction == null){
                calendar.setTime(transaction.date);
            }
            else{
                calendar.setTime(new Date());
            }

            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_WEEK));
            datePickerDialog.show();
        }
        else if(v.getTag().equals("4")){
            //save tapped
            if(validateUIFields()){
                transaction.note = noteView.getText().toString().trim();
                DatabaseTalker.getInstance().insertOrUpdatePaymentReceiptTransaction(this, 1, AsyncTask.SERIAL_EXECUTOR, this.transaction);
            }
        }
        else if(v.getTag().equals("1")){
            AccountListFrag frag = new AccountListFrag();
            frag.delegate = this;
            frag.tag = new WeakReference<Object>(debitName);
            this.transitionToFragment(frag);
        }
        else if(v.getTag().equals("2")){
            AccountListFrag frag = new AccountListFrag();
            frag.delegate = this;
            frag.tag = new WeakReference<Object>(creditName);
            this.transitionToFragment(frag);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(transaction != null && transaction.id != null) outState.putString("transaction_id",transaction.id);
    }

    @Override
    public void onAccountSelected(MasterAccount account, WeakReference<Object> tag) {

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        Date date = calendar.getTime();
        this.transaction.date = date;
        dateView.setText(dayOfMonth + " " + new SimpleDateFormat("MMM").format(calendar.getTime()) + ", " + year);
    }

    public void loadTransaction(PaymentReceiptTransaction transaction){
        if(transaction == null){
            debitName.setText("-");
            creditName.setText("-");
            dateView.setText(new SimpleDateFormat("dd MMM, yyyy").format(new Date()));
            amountView.setText("0");

            this.transaction = new PaymentReceiptTransaction();
            if(this instanceof PaymentFrag){
                this.transaction.setType(Transaction.TYPE_PAYMENT);
            }
            else if(this instanceof ReceiptFrag){
                this.transaction.setType(Transaction.TYPE_RECEIPT);
            }
            this.transaction.date = new Date();

        }
        else{
            this.transaction = transaction;
            dateView.setText(new SimpleDateFormat("dd MMM, yyyy").format(transaction.date));
            amountView.setText(transaction.amount.toString());
            noteView.setText(transaction.note);
        }
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if(processId == 1 && databaseObject != null){
            this.transaction = null;
            loadTransaction(null);
            flushUIelements();
            Toast.makeText(getActivity(), "Transaction Saved", Toast.LENGTH_SHORT).show();
        }
        else if(processId == 5 && databaseObject != null){
            loadTransaction((PaymentReceiptTransaction) databaseObject);
        }
    }

    private void flushUIelements() {
        dateView.setText(new SimpleDateFormat("dd MMM, yyyy").format(new Date()));
        amountView.setText("");
        debitName.setText("-");
        creditName.setText("-");
        noteView.setText("");
    }
}
