package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

import android.os.AsyncTask;

import com.biztracker.database.DatabaseTalker;

/**
 * Created by karan on 09/06/16.
 */

public class InvoiceOnlyAccountListFrag extends AccountListFrag {

    @Override
    public void requestDataFromDatabaseForQuery(String query) {
        if(query == null || query.trim().isEmpty()){
            DatabaseTalker.getInstance().getMasterAccountsForInvoiceOnlyMode(this, 2, AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else{
            DatabaseTalker.getInstance().getMasterAccountsForInvoiceOnlyModeForQuery(this, 1, AsyncTask.THREAD_POOL_EXECUTOR, query);
        }
    }
}
