package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.common.PriceHolder;
import com.biztracker.databaseobjects.SlaveEntity;

/**
 * Created by karan on 23/04/16.
 */
public class InvoiceSlaveEntityValueInputFrag extends DialogFragment implements TextView.OnEditorActionListener {

    View rootView;
    PriceHolder valueHolder;
    TextView itemNameHolder;
    public boolean itemFromInvoiceList;
    SlaveEntity slaveEntity;
    public SlaveEntityValuesEditListener delegate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.invoice_slave_entity_value_input_layout, container, false);
        valueHolder = (PriceHolder) rootView.findViewById(R.id.price_et);
        itemNameHolder = (TextView)rootView.findViewById(R.id.product_name_tv);
        valueHolder.setImeActionLabel("Save", KeyEvent.KEYCODE_ENTER);
        valueHolder.setValue(slaveEntity.getValue());
        valueHolder.setOnEditorActionListener(this);
        itemNameHolder.setText(slaveEntity.name);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return rootView;
    }

    public void setSlaveEntity(SlaveEntity slaveEntity, boolean itemFromInvoiceList){
        this.slaveEntity = slaveEntity;
        this.itemFromInvoiceList = itemFromInvoiceList;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //showKeyboard();
    }

    public void onSaveTapped() {
        slaveEntity.value = valueHolder.getValue();
        if(this.delegate != null){
            this.delegate.onSetSlaveEntityValue(slaveEntity, itemFromInvoiceList);
        }
        this.dismiss();
    }



    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(valueHolder == v){
            onSaveTapped();
        }
        return true;
    }

    interface SlaveEntityValuesEditListener{

        void onSetSlaveEntityValue(SlaveEntity slaveEntity, boolean itemFromInvoiceList);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }
}
