package com.biztracker.fragments.Transactions.paymentsAndReceipts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.R;

/**
 * Created by karan on 11/06/16.
 */

public class ViewablePaymentFrag extends PaymentFrag {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater,container,savedInstanceState);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNativeBackPress();
            }
        });
        backButton.setImageResource(R.drawable.ic_keyboard_backspace_white_24dp);
        return view;
    }
}
