package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.biztracker.CustomTextWatcher;
import com.biztracker.R;
import com.biztracker.common.DatabaseObjectAdapter;
import com.biztracker.common.Utilities;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.DatabaseObject;
import com.biztracker.databaseobjects.SlaveEntity;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.masteraccounts.CustomerAccountFragment;
import com.biztracker.fragments.masteraccounts.MasterAccountFragment;
import com.biztracker.fragments.masteraccounts.OtherAccountFragment;
import com.biztracker.fragments.masteraccounts.SellerAccountFragment;
import com.biztracker.fragments.taxesanddiscounts.AddDiscountsFragment;
import com.biztracker.fragments.taxesanddiscounts.AddSlaveAccountFragment;
import com.biztracker.fragments.taxesanddiscounts.AddTaxChargesFragment;
import com.biztracker.fragments.universal_search.adapters.TaxesListSearchAdapter;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by karan on 20/03/16.
 */
public class InvoiceSlaveEntityListFrag extends SuperFragment implements BaseUniversalSearchAdapter.onItemClickListener, DatabaseTalker.queryCompleteCallBack, View.OnClickListener, AddSlaveAccountFragment.SlaveAccountFragmentDelegate {

    EditText slaveEntitySearch;
    SlaveEntitySelectionDelegate delegate;
    public List<SlaveEntity> parentList;
    View rootView;
    TextView noTaxView;

    private RecyclerView mRecyclerView;
    private TaxesListSearchAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView == null) {
            rootView = inflater.inflate(R.layout.invoice_slave_entity_list_layout, container, false);
            rootView.findViewById(R.id.add_button).setOnClickListener(this);
            int mHeight = 5 * ((int)getResources().getDimension(R.dimen.one_dp));
            noTaxView = (TextView) rootView.findViewById(R.id.no_slave_entity_tv);
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.invoice_slave_entity_list);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(),LinearLayoutManager.VERTICAL,false));
            DividerItemDecoration decoration = new DividerItemDecoration(20, 20);
            decoration.disableTopPadding = false;
            mRecyclerView.addItemDecoration(decoration);
            mAdapter = new TaxesListSearchAdapter(getContext());
            mAdapter.delegate = this;
            mRecyclerView.setAdapter(mAdapter);

            slaveEntitySearch = (EditText) rootView.findViewById(R.id.slave_entity_search_et);
            slaveEntitySearch.addTextChangedListener(new CustomTextWatcher(customTextWatcherCallback));

            rootView.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNativeBackPress();
                }
            });

            DatabaseTalker.getInstance().getAllSlaveEntities(this, 1, AsyncTask.THREAD_POOL_EXECUTOR);

        }
        else{
            container.removeView(rootView);
        }
        return rootView;
    }

    private CustomTextWatcher.CustomTextWatcherCallback customTextWatcherCallback = new CustomTextWatcher.CustomTextWatcherCallback() {
        @Override
        public void onSearchForText(String text) {
            requestDataFromDatabaseForQuery(text);
        }
    };

    public void requestDataFromDatabaseForQuery(String query) {
        if(query == null || query.trim().isEmpty()){
            DatabaseTalker.getInstance().getAllSlaveEntities(this, 2, AsyncTask.SERIAL_EXECUTOR);
        }
        else{
            DatabaseTalker.getInstance().getSlaveEntitiesForSearchQuery(this, 1, AsyncTask.SERIAL_EXECUTOR, query);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(slaveEntitySearch.hasFocus()){
            slaveEntitySearch.clearFocus();
            Utilities.hideKeyboard(getActivity());
        }
    }

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if((processId == 1 || processId == 2)&& databaseObject instanceof ArrayList){
            List items = (List) databaseObject;
            mAdapter.setItems(items);
            if(processId == 2) {
                if (items.isEmpty()) {
                    noTaxView.setVisibility(View.VISIBLE);
                }
            }
            if(!items.isEmpty()){
                noTaxView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onItemClicked(Object item) {
        if(slaveEntitySearch!=null) slaveEntitySearch.clearFocus();
        SlaveEntity product = (SlaveEntity)item;
        SlaveEntity productCopy = (SlaveEntity)product.getCopy();
        productCopy.parentList = this.parentList;
        if(delegate != null) delegate.didSelectSlaveEntity(productCopy);
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(InvoiceSlaveEntityListFrag.this.getActivity());
        builderSingle.setTitle("Create Account");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                InvoiceSlaveEntityListFrag.this.getActivity(),
                android.R.layout.simple_list_item_1);
        arrayAdapter.add("Discount");
        arrayAdapter.add("Tax/Charge");

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AddSlaveAccountFragment fragment = null;
                        switch (which){
                            case 0:
                                fragment = new AddDiscountsFragment();
                                break;
                            case 1:
                                fragment = new AddTaxChargesFragment();
                                break;
                        }
                        fragment.delegate = InvoiceSlaveEntityListFrag.this;
                        InvoiceSlaveEntityListFrag.this.transitionToFragment(fragment);
                    }
                });
        builderSingle.show();
    }

    @Override
    public void onSlaveAccountCreated(AddSlaveAccountFragment fragment) {
        fragment.onNativeBackPress();
    }

    interface SlaveEntitySelectionDelegate{
        void didSelectSlaveEntity(SlaveEntity slaveEntity);
    }

    public void showSlaveEntityValueInputFrag(InvoiceSlaveEntityValueInputFrag frag){
        FragmentManager manager = getActivity().getFragmentManager();
        frag.show(manager, "slave_dialog");
        this.onNativeBackPress();
    }

}
