package com.biztracker.fragments.Transactions.paymentsAndReceipts;

import android.view.View;

import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.PaymentReceiptTransaction;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.AccountListFrag;

import java.lang.ref.WeakReference;

/**
 * Created by karan on 09/06/16.
 */

public class PaymentFrag extends PaymentReceiptFrag {

    @Override
    public void onAccountSelected(MasterAccount account, WeakReference<Object> tag) {
        if(tag.get() == debitName){
            this.transaction.debitAccount = account;
            this.debitName.setText(transaction.debitAccount.name);
        }
        else if(tag.get() == creditName){
            this.transaction.creditAccount = account;
            this.creditName.setText(transaction.creditAccount.name);
        }
    }

    @Override
    public void loadTransaction(PaymentReceiptTransaction transaction) {
        super.loadTransaction(transaction);
        if(transaction != null){
            debitName.setText(transaction.debitAccount.name);
            creditName.setText(transaction.creditAccount.name);
        }
    }
}
