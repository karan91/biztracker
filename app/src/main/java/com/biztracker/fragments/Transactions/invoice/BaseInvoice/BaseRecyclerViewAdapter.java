package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.biztracker.R;
import com.biztracker.common.DateHolder;
import com.biztracker.common.MasterAccountHolder;
import com.biztracker.common.Utilities;
import com.biztracker.databaseobjects.DatabaseObject;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.SlaveEntity;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.views.InvoiceNumberEditText;
import com.biztracker.views.RobotoLightTextView;
import com.biztracker.views.RobotoMediumEditText;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by karan on 18/07/15.
 */
public class BaseRecyclerViewAdapter extends RecyclerView.Adapter<BaseRecyclerViewAdapter.BaseRecyclerViewHolder> implements SimpleItemTouchHelper.ItemTouchHelperAdapter
{
    public ArrayList<ViewTypeClass> viewTypes = new ArrayList<>();
    private static int TYPE_MAIN_HEADER = 0;
    private static int TYPE_NOTES_VIEW = 1;
    private static int TYPE_HEADING_VIEW = 2;
    private static int TYPE_PRODUCT_VIEW = 4;
    private static int TYPE_MISC_ITEM_VIEW = 5;
    private static int TYPE_TOTALS_VIEW = 6;
    InvoicedTransaction transaction;
    Activity activity;
    private AddNewItemListener eventHandler;

    private boolean isProductsTrashModeEnabled = false;
    private boolean isMiscellaneousTrashModeEnabled = false;

    public void addStaticViewType(int viewType) {
        ViewTypeClass vc = new ViewTypeClass();
        vc.viewType = viewType;
        vc.items = null;
        viewTypes.add(vc);
    }


    public BaseRecyclerViewAdapter(InvoicedTransaction transaction, Activity activity, AddNewItemListener eventHandler){

        this.transaction = transaction;
        this.activity = activity;
        setItemViewTypes();
        this.eventHandler = eventHandler;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder baseRecyclerViewHolder, int i) {
        ViewTypeClass viewType = getViewTypeClass(i);
        Object object = null;
        if(viewType.viewType == TYPE_MAIN_HEADER)    object = transaction;

        else if(viewType.viewType == TYPE_HEADING_VIEW){
            int index = viewTypes.indexOf(viewType);
            if(index == 1)  object = activity.getResources().getString(R.string.heading_product);
            else if(index == 3) object = "TAXES & DISCOUNTS";
        }

        else if(viewType.viewType == TYPE_PRODUCT_VIEW){
            object = getObjectForPosition(i);
        }
        else if(viewType.viewType == TYPE_MISC_ITEM_VIEW){
            object = getObjectForPosition(i);
        }
        else if(viewType.viewType == TYPE_TOTALS_VIEW){
            object = transaction.getTotalValueOfInvoice();
        }
        baseRecyclerViewHolder.position = i;
        baseRecyclerViewHolder.setItem(object);
    }

    public void addDynamicViewType(int viewType,List items){
        ViewTypeClass vc = new ViewTypeClass();
        vc.viewType = viewType;
        vc.items = items;
        viewTypes.add(vc);
    }

    @Override
    public int getItemCount() {
        int count = 0;
        for(ViewTypeClass viewTypeClass : viewTypes){
            if(viewTypeClass.items == null) count++;
            else    count+=viewTypeClass.items.size();
        }
        return count;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int listViewItemType) {
        View view;
        if (listViewItemType == TYPE_MAIN_HEADER) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.invoice_heading_layout, viewGroup, false);
            return new BaseRecyclerViewAdapter.MainHeaderViewHolder(view);

        } else if (listViewItemType == TYPE_HEADING_VIEW) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_view, viewGroup, false);
            return new BaseRecyclerViewAdapter.HeadingViewHolder(view);
        }
        else if(listViewItemType == TYPE_PRODUCT_VIEW){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_layout, viewGroup, false);
            return new BaseRecyclerViewAdapter.ProductViewHolder(view);
        }
        else if(listViewItemType == TYPE_MISC_ITEM_VIEW){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.misc_item_layout, viewGroup, false);
            return new BaseRecyclerViewAdapter.MiscellaneousViewHolder(view);
        }
        else if(listViewItemType == TYPE_TOTALS_VIEW){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.totals_layout, viewGroup, false);
            return new TotalViewHolder(view);
        }
        return null;
    }

    public void setItemViewTypes(){
        addStaticViewType(TYPE_MAIN_HEADER);
        addStaticViewType(TYPE_HEADING_VIEW);
        addDynamicViewType(TYPE_PRODUCT_VIEW, transaction.productList);
        addStaticViewType(TYPE_HEADING_VIEW);
        addDynamicViewType(TYPE_MISC_ITEM_VIEW,transaction.slaveEntities);
        //addStaticViewType(TYPE_TOTALS_VIEW);
    }

    @Override
    public int getItemViewType(int position) {
        ViewTypeClass viewTypeClass = getViewTypeClass(position);

        if(viewTypeClass != null)   return viewTypeClass.viewType;

        return -1;
    }

    public ViewTypeClass getViewTypeClass(int position){
        int index = 0;
        for(ViewTypeClass viewType : viewTypes){
            if(viewType.items == null){
                if(index == position)   return viewType;
                index++;
            }
            else{
                for(Object o : viewType.items){
                    if(index == position)   return viewType;
                    index++;
                }
            }
        }
        return null;
    }

    public Object getObjectForPosition(int position){
        for(ViewTypeClass viewType : viewTypes){
            if(viewType.items == null){

                position--;
            }
            else{
                for(Object o : viewType.items){
                    if(position == 0){
                        return o;
                    }
                    position--;
                }
            }
        }
        return -1;
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Object fromItem = getObjectForPosition(fromPosition);
        Object toItem = getObjectForPosition(toPosition);

        if(fromItem instanceof SlaveEntity && toItem instanceof SlaveEntity){
            int index1 = transaction.slaveEntities.indexOf(fromItem);
            int index2 = transaction.slaveEntities.indexOf(toItem);
            if (index1 < index2) {
                for (int i = index1; i < index2; i++) {
                    Collections.swap(transaction.slaveEntities, i, i + 1);
                }
            } else {
                for (int i = index1; i > index2; i--) {
                    Collections.swap(transaction.slaveEntities, i, i - 1);
                }
            }
            notifyItemMoved(fromPosition, toPosition);
            notifyItemChanged(fromPosition);
            notifyItemChanged(toPosition);
            return true;
        }
        return false;
    }

    @Override
    public void onItemDismiss(final int position) {
        final DatabaseObject product = (DatabaseObject) getObjectForPosition(position);
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setTitle("Delete Item");
        alert.setMessage(product.name);
        DialogInterface.OnClickListener deleteItemListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch(which){
                    case AlertDialog.BUTTON_POSITIVE:
                        if(product instanceof Product){
                            transaction.removeProduct((Product)product);
                        }
                        else if(product instanceof SlaveEntity){
                            transaction.slaveEntities.remove(product);
                        }
                        notifyItemRemoved(position);
                        for(int i = position;i<getItemCount();i++){
                            notifyItemChanged(i);
                        }
                        BaseRecyclerViewAdapter.this.eventHandler.onItemDeletedBySwiping();
                        break;

                    case AlertDialog.BUTTON_NEGATIVE:
                        notifyItemChanged(position);
                        break;
                }
            }
        };
        alert.setPositiveButton("Yes", deleteItemListener);
        alert.setNegativeButton("No", deleteItemListener);
        alert.show();
    }

    public static abstract class BaseRecyclerViewHolder extends RecyclerView.ViewHolder{
        public Object item;
        int position;
        public BaseRecyclerViewHolder(View itemView) {
            super(itemView);
        }
        public void setItem(Object _item){
            item = _item;
        }
    }

    private class ViewTypeClass{
        int viewType;
        List items = null;
        String heading;
    }

    /** VIEW HOLDERS */
    //MAIN HEADING VIEW HOLDER
    public class MainHeaderViewHolder extends BaseRecyclerViewHolder implements TextView.OnEditorActionListener, View.OnClickListener, DatePickerDialog.OnDateSetListener {

        MasterAccountHolder accountLabel;
        DateHolder dateLabel;
        InvoiceNumberEditText invoiceNo;
        InvoicedTransaction transaction;

        public MainHeaderViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(mainHeaderViewTapListener);
            accountLabel = (MasterAccountHolder) itemView.findViewById(R.id.parties_tv);
            dateLabel = (DateHolder) itemView.findViewById(R.id.date_label);
            dateLabel.setOnClickListener(this);
            invoiceNo = (InvoiceNumberEditText) itemView.findViewById(R.id.invoice_no);
            invoiceNo.setOnEditorActionListener(this);
        }

        @Override
        public void setItem(Object _item) {
            super.setItem(_item);
            this.transaction = (InvoicedTransaction) _item;
            InvoicedTransaction transaction = (InvoicedTransaction)_item;
            MasterAccount activeAccount = transaction.getActiveAccount();
            this.dateLabel.setDate(transaction.date);
            if(transaction.getType() == Transaction.TYPE_SALES) {
                this.accountLabel.setMasterAccount(transaction.debitAccount, "");
            }
            else if(transaction.getType() == Transaction.TYPE_PURCHASE){
                this.accountLabel.setMasterAccount(transaction.creditAccount, "");
            }
            invoiceNo.transaction = this.transaction;
            invoiceNo.setText(transaction.transactionNumber);
        }

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            this.transaction.transactionNumber = v.getText().toString().trim();
            v.clearFocus();
            Utilities.hideKeyboard(BaseRecyclerViewAdapter.this.activity);
            return true;
        }

        @Override
        public void onClick(View v) {
            Log.d("this", "happening");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(transaction.date);

            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(BaseRecyclerViewAdapter.this.activity, this, year, month, day);
            dialog.show();
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            transaction.date = calendar.getTime();
            this.dateLabel.setText(transaction.getPrettyDate());
        }
    }


    //HEADING VIEW HOLDER
    public class HeadingViewHolder extends BaseRecyclerViewHolder{

        TextView headingView;
        View addButton;
        View trashButton;

        public HeadingViewHolder(View itemView) {
            super(itemView);
            headingView = (TextView) itemView.findViewById(R.id.heading_view_tv);
            addButton = itemView.findViewById(R.id.add_button);
            addButton.setOnClickListener(addButtonListener);
            trashButton = itemView.findViewById(R.id.trash_button);
            trashButton.setOnClickListener(removeItemButtonListener);
        }

        @Override
        public void setItem(Object _item) {
            super.setItem(_item);
            String text = (String)_item;
            this.headingView.setText(text);
            this.addButton.setTag(text);
            this.trashButton.setTag(text);

            if(text.equals(activity.getResources().getString(R.string.heading_product))){
                if(isProductsTrashModeEnabled){
                    this.trashButton.setBackgroundColor(0xff7aafd2);
                }else{
                    this.trashButton.setBackgroundColor(0xff242A32);
                }
            }
            else if(text.equals("TAXES & DISCOUNTS")){
                if(isMiscellaneousTrashModeEnabled){
                    this.trashButton.setBackgroundColor(0xff7aafd2);
                }else{
                    this.trashButton.setBackgroundColor(0xff242A32);
                }
            }
        }
    }

    //PRODUCT VIEW HOLDER
    public class ProductViewHolder extends BaseRecyclerViewHolder implements View.OnClickListener{

        TextView productNameLabel, priceUnitRateLabel, totalLabel;
        View parentView;
        View removeItemView;

        public ProductViewHolder(View itemView) {
            super(itemView);
            parentView = itemView;
            productNameLabel = (TextView) itemView.findViewById(R.id.item_name_tv);
            priceUnitRateLabel = (TextView) itemView.findViewById(R.id.price_unit_rate_tv);
            totalLabel = (TextView) itemView.findViewById(R.id.total_tv);
            removeItemView = itemView.findViewById(R.id.remove_item_button);
            removeItemView.setOnClickListener(this);
            itemView.setOnClickListener(productOrOtherItemsTapListener);
        }

        @Override
        public void setItem(Object _item) {
            super.setItem(_item);
            Product product = (Product)_item;
            productNameLabel.setText(product.name);
            priceUnitRateLabel.setText(product.entityQty + " " + product.unit.displayName + " @ " + product.entityValue);
            if(product.entityValue != null && product.entityQty != null)
            totalLabel.setText(product.getFormattedInvoiceValue());
            parentView.setTag(_item);
            if(isProductsTrashModeEnabled){
                totalLabel.setVisibility(View.INVISIBLE);
                removeItemView.setVisibility(View.VISIBLE);
            }else{
                totalLabel.setVisibility(View.VISIBLE);
                removeItemView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onClick(View view) {
            transaction.removeProduct((Product) this.item);
            notifyItemRemoved(position);
            for(int i=position; i<getItemCount(); i++){
                notifyItemChanged(i);
            }
            BaseRecyclerViewAdapter.this.eventHandler.onItemDeletedBySwiping();
        }
    }

    //MISCELLANEOUS VIEW HOLDER
    public class MiscellaneousViewHolder extends BaseRecyclerViewHolder implements View.OnClickListener{
        TextView entityNameLabel, valueLabel, percentValueLabel;
        View parentView;
        View removeItemView;

        public MiscellaneousViewHolder(View itemView) {
            super(itemView);
            parentView = itemView;
            entityNameLabel = (TextView) itemView.findViewById(R.id.entity_name);
            valueLabel = (TextView) itemView.findViewById(R.id.value_field);
            percentValueLabel = (TextView) itemView.findViewById(R.id.percent_value);
            removeItemView = itemView.findViewById(R.id.remove_item_button);
            removeItemView.setOnClickListener(this);
            itemView.setOnClickListener(productOrOtherItemsTapListener);
        }

        @Override
        public void setItem(Object item) {
            super.setItem(item);
            SlaveEntity slaveEntity = (SlaveEntity)item;
            if(slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_PERCENT){
                NumberFormat format = NumberFormat.getCurrencyInstance();
                entityNameLabel.setText(slaveEntity.name);
                valueLabel.setText(slaveEntity.getFormattedInvoiceValue());
                percentValueLabel.setText(slaveEntity.getValue().toString() + "%");
            }
            else{
                entityNameLabel.setText(slaveEntity.name);
                valueLabel.setText(slaveEntity.getFormattedValue());
                percentValueLabel.setText("");
            }
            parentView.setTag(item);

            if(isMiscellaneousTrashModeEnabled){
                valueLabel.setVisibility(View.INVISIBLE);
                removeItemView.setVisibility(View.VISIBLE);
            }else{
                valueLabel.setVisibility(View.VISIBLE);
                removeItemView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onClick(View view) {
            transaction.slaveEntities.remove((SlaveEntity) this.item);
            notifyItemRemoved(position);
            for(int i=position; i<getItemCount(); i++){
                notifyItemChanged(i);
            }
            BaseRecyclerViewAdapter.this.eventHandler.onItemDeletedBySwiping();
        }
    }

    public class TotalViewHolder extends BaseRecyclerViewHolder{



        ImageButton barcodeScannerStarter;
        TextView totalsView;
        View parentView;
        public TotalViewHolder(View itemView) {
            super(itemView);
            parentView = itemView;
            totalsView = (TextView) parentView.findViewById(R.id.totals_tv);
            barcodeScannerStarter = (ImageButton) parentView.findViewById(R.id.barcode_starter);
            barcodeScannerStarter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            barcodeScannerStarter.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_DOWN){
                        eventHandler.onStartBarcodeFeed();
                    }
                    else if(event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL){
                        eventHandler.onEndBarcodeFeed();
                    }
                    return false;
                }
            });
        }

        @Override
        public void setItem(Object _item) {
            super.setItem(_item);
            BigDecimal netValue = (BigDecimal)_item;
            this.totalsView.setText(netValue.toString());
        }
    }

    View.OnClickListener removeItemButtonListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            String tag = (String) v.getTag();
            if(tag.equals(activity.getResources().getString(R.string.heading_product))){
                isProductsTrashModeEnabled = !isProductsTrashModeEnabled;
                notifyDataSetChanged();
            }
            else if(tag.equals("TAXES & DISCOUNTS")){
                isMiscellaneousTrashModeEnabled = !isMiscellaneousTrashModeEnabled;
                notifyDataSetChanged();
            }
        }
    };

    View.OnClickListener addButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String tag = (String) v.getTag();
            if(tag.equals(activity.getResources().getString(R.string.heading_product))){
                if(eventHandler != null){
                    eventHandler.onAddOrEditProduct(null);
                }
            }
            else if(tag.equals("TAXES & DISCOUNTS")){
                if(eventHandler != null){
                    eventHandler.onAddOrEditSlaveEntity(null);
                }
            }
        }
    };

    View.OnClickListener productOrOtherItemsTapListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Object obj = v.getTag();

            if(eventHandler == null) return;
            if(obj instanceof Product){
                eventHandler.onAddOrEditProduct((Product) obj);
            }
            else if(obj instanceof SlaveEntity){
                eventHandler.onAddOrEditSlaveEntity((SlaveEntity) obj);
            }
        }
    };

    View.OnClickListener mainHeaderViewTapListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(eventHandler == null)    return;
            Date date = ((DateHolder)v.findViewById(R.id.date_label)).getDate();
            MasterAccount account = ((MasterAccountHolder)v.findViewById(R.id.parties_tv)).getMasterAccount();
            eventHandler.onMainHeaderTapped(account, date);
        }
    };

    public interface AddNewItemListener{

        void onAddOrEditProduct(Product product);
        void onAddOrEditSlaveEntity(SlaveEntity slaveEntity);
        void onMainHeaderTapped(MasterAccount masterAccount, Date date);
        void onStartBarcodeFeed();
        void onEndBarcodeFeed();
        void onItemDeletedBySwiping();
    }
}
