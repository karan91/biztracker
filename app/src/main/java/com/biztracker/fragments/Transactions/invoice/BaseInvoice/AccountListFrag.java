package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.biztracker.CustomTextWatcher;
import com.biztracker.R;
import com.biztracker.common.Utilities;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.Product;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.masteraccounts.CustomerAccountFragment;
import com.biztracker.fragments.masteraccounts.MasterAccountFragment;
import com.biztracker.fragments.masteraccounts.OtherAccountFragment;
import com.biztracker.fragments.masteraccounts.SellerAccountFragment;
import com.biztracker.fragments.universal_search.adapters.AccountListSearchAdapter;
import com.biztracker.fragments.universal_search.adapters.ProductListSearchAdapter;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by karan on 03/12/15.
 */
public class AccountListFrag extends SuperFragment implements DatabaseTalker.queryCompleteCallBack,
        BaseUniversalSearchAdapter.onItemClickListener, View.OnClickListener, MasterAccountFragment.MasterAccountFragmentDelegate {

    List<MasterAccount> accounts;
    EditText accountSearch;
    public AccountSetDelegate delegate;
    View rootView;
    public WeakReference<Object> tag;
    private int mHeight = 10;
    private RecyclerView mRecyclerView;
    private AccountListSearchAdapter mAccountAdapter;
    private LinearLayout emptyLayout;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView == null) {
            rootView = inflater.inflate(R.layout.invoice_account_list, container, false);
            rootView.findViewById(R.id.add_button).setOnClickListener(this);
            emptyLayout = (LinearLayout) rootView.findViewById(R.id.no_accounts_layout);
            accountSearch = (EditText) rootView.findViewById(R.id.account_search_et);
            accounts = new ArrayList<>();

            mAccountAdapter = new AccountListSearchAdapter(getActivity().getApplicationContext());
            mAccountAdapter.delegate = this;

            mHeight = 5 * ((int)getResources().getDimension(R.dimen.one_dp));
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.invoice_account_list);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(),LinearLayoutManager.VERTICAL,false));
            DividerItemDecoration decoration = new DividerItemDecoration(mHeight, mHeight);
            decoration.disableTopPadding = false;
            mRecyclerView.addItemDecoration(decoration);
            mRecyclerView.setAdapter(mAccountAdapter);

            rootView.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNativeBackPress();
                }
            });
            CustomTextWatcher watcher = new CustomTextWatcher(textWatcherCallback);
            watcher.delayInMillis = 100;
            accountSearch.addTextChangedListener(watcher);

            requestDataFromDatabaseForQuery(null);
        }
        else{
            container.removeView(rootView);
        }
        return rootView;
    }



    private CustomTextWatcher.CustomTextWatcherCallback textWatcherCallback = new CustomTextWatcher.CustomTextWatcherCallback() {
        @Override
        public void onSearchForText(String text) {
            requestDataFromDatabaseForQuery(text);
        }
    };


    public void requestDataFromDatabaseForQuery(String query) {
        if(query == null || query.trim().isEmpty()){
            DatabaseTalker.getInstance().getAllMasterAccounts(this,2, AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else{
            DatabaseTalker.getInstance().getMasterAccountsForQuery(this, 1, AsyncTask.THREAD_POOL_EXECUTOR, query);
        }
    }


    @Override
    public boolean validateUIFields() {
        return false;
    }


    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if((processId == 1 || processId == 2)&& databaseObject instanceof ArrayList){

            List<MasterAccount> items = (List<MasterAccount>)databaseObject;
            if(processId == 2){
                if(items.isEmpty()){
                    emptyLayout.setVisibility(View.VISIBLE);
                }
            }
            if(!items.isEmpty()){
                emptyLayout.setVisibility(View.GONE);
            }
            accounts = items;
            if(mAccountAdapter != null) mAccountAdapter.setItems(accounts);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(accountSearch.hasFocus()) {
            accountSearch.clearFocus();
            Utilities.hideKeyboard(getActivity());
        }
    }

    @Override
    public void onItemClicked(Object item) {
        MasterAccount copiedAccount = (MasterAccount) ((MasterAccount)item).getCopy();
        this.onNativeBackPress();
        if(delegate != null)
            delegate.onAccountSelected(copiedAccount, tag);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.add_button){
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(AccountListFrag.this.getActivity());
            builderSingle.setTitle("Create Account");

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    AccountListFrag.this.getActivity(),
                    android.R.layout.simple_list_item_1);
            arrayAdapter.add("Customer");
            arrayAdapter.add("Supplier");
            arrayAdapter.add("Other Account");

            builderSingle.setAdapter(
                    arrayAdapter,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MasterAccountFragment fragment = null;
                            switch (which){
                                case 0:
                                    fragment = new CustomerAccountFragment();
                                    break;
                                case 1:
                                    fragment = new SellerAccountFragment();
                                    break;
                                case 2:
                                    fragment = new OtherAccountFragment();
                                    break;
                            }
                            fragment.delegate = AccountListFrag.this;
                            AccountListFrag.this.transitionToFragment(fragment);
                        }
                    });
            builderSingle.show();
        }
    }

    @Override
    public void onMasterAccountCreated(MasterAccountFragment fragment) {
        fragment.onNativeBackPress();
    }


    public interface AccountSetDelegate{
        void onAccountSelected(MasterAccount account, WeakReference<Object> tag);
    }

}
