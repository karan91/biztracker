package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.biztracker.R;
import com.biztracker.activities.MainActivity;
import com.biztracker.barcodescanner.wrapper.BarcodeScanner;
import com.biztracker.common.Utilities;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.PaymentReceiptTransaction;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.SlaveEntity;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.databaseobjects.TransactionType;
import com.biztracker.dialogs.DialogProductDetailsEdit;
import com.biztracker.fragments.InputFragment;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.additems.ProductFragment;
import com.biztracker.prints.PDFInvoiceViewAndPrintFragment;
import com.biztracker.prints.PDFPaymentFragment;
import com.biztracker.prints.PDFReceiptFragment;
import com.google.android.gms.vision.barcode.Barcode;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Karan on 11/07/15.
 */


public class InvoiceFrag extends InputFragment implements BaseRecyclerViewAdapter.AddNewItemListener,
        BarcodeScanner.BarcodeDetectionListener, DatabaseTalker.queryCompleteCallBack, InvoiceProductListFrag.ItemSelectedDelgate, ComponentCallbacks2,
        DialogInterface.OnDismissListener,
        InvoiceSlaveEntityListFrag.SlaveEntitySelectionDelegate, InvoiceSlaveEntityDetailsFrag.SlaveEntityDetailsFragListener,
        View.OnTouchListener, InvoiceSlaveEntityValueInputFrag.SlaveEntityValuesEditListener, View.OnClickListener,AccountListFrag.AccountSetDelegate,
        DatePickerDialog.OnDateSetListener{


    public InvoicedTransaction invoiceTransaction;
    private RecyclerView recyclerView;
    private BarcodeScanner barcodeScanner;
    private ViewGroup rootView;
    private View parentView;
    private AccountListFrag invoiceAccountFrag;
    private InvoiceProductListFrag invoiceProductListFrag;
    private InvoiceSlaveEntityListFrag invoiceSlaveEntityListFrag;
    private SimpleItemTouchHelper itemTouchHelper;
    private TextView saveButton;
    private TextView headerTextHolder;
    private TextView totalsView;
    protected ImageView backButton;

    private boolean isBarcodeStarted = false;

    public InvoiceFrag(){
        mFragmentTag = "InvoiceFragment";
    }

    protected String getTitle(){
        return "";
    }

    ItemTouchHelper.Callback complexSwipeCallback = new ItemTouchHelper.Callback() {
        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            if(viewHolder instanceof BaseRecyclerViewAdapter.MiscellaneousViewHolder) {
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(dragFlags, swipeFlags);
            }
            else if (viewHolder instanceof BaseRecyclerViewAdapter.ProductViewHolder){
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(0, swipeFlags);
            }
            return 0;
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return itemTouchHelper.adapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        }

        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            itemTouchHelper.adapter.onItemDismiss(viewHolder.getAdapterPosition());
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return true;
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return true;
        }
    };

    @Override
    public void onTrimMemory(int level) {
        //super.onTrimMemory(level);
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            if(barcodeScanner != null){
                barcodeScanner.active = false;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(parentView == null) {
            //getReadWritePermission();
            parentView = inflater.inflate(R.layout.invoice_frag_layout, container, false);
            saveButton = (TextView) parentView.findViewById(R.id.save_btn);
            saveButton.setOnClickListener(this);
            rootView = (ViewGroup) parentView.findViewById(R.id.root_layout);
            rootView.setOnTouchListener(this);
            recyclerView = (RecyclerView) parentView.findViewById(R.id.root_recycler_view);
            recyclerView.addItemDecoration(new DividerItemDecoration((int)getResources().getDimension(R.dimen.one_dp)*3, (int)getResources().getDimension(R.dimen.one_dp)*5,false));
            ItemTouchHelper touchHelper = new ItemTouchHelper(complexSwipeCallback);
            touchHelper.attachToRecyclerView(recyclerView);
            //barcodeScanner = (BarcodeScanner) parentView.findViewById(R.id.barcode_stream);
            backButton = (ImageView) parentView.findViewById(R.id.back_btn);
            loadTotalsView();
            loadInvoiceTransaction(savedInstanceState);
            headerTextHolder = (TextView) parentView.findViewById(R.id.header_text_view);
            headerTextHolder.setText(getTitle());
            barcodeScanner = ((MainActivity)getActivity()).barcodeScanner;

            Log.d("BizApp","InvoiceFrag: savedInstanceState: "+savedInstanceState);

            parentView.findViewById(R.id.trash_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(invoiceTransaction != null){
                        invoiceTransaction.deleteObject(new DatabaseTalker.queryCompleteCallBack() {
                            @Override
                            public void onReceivedDatabaseObject(Object databaseObject, int processId) {
                                if(processId == 134){
                                    onNativeBackPress();
                                }
                            }
                        },134,AsyncTask.SERIAL_EXECUTOR);
                    }
                }
            });
        }
        else{
            container.removeView(parentView);
        }
        return parentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(invoiceTransaction != null) outState.putString("invoice_transaction_id",invoiceTransaction.id);
    }

    private void loadTotalsView() {
        TextView barcodeScannerStarter;
        totalsView = (TextView) parentView.findViewById(R.id.totals_tv);
        barcodeScannerStarter = (TextView) parentView.findViewById(R.id.barcode_starter);
        barcodeScannerStarter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    onStartBarcodeFeed();
                    Log.d("barcode", "barcode action down");
                }
                else if(event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL){
                    onEndBarcodeFeed();
                    Log.d("barcode", "barcode action up");
                }
                return true;
            }
        });
    }

    @Override
    public void onItemDeletedBySwiping() {
        this.updateTotalsView();
    }

    private void loadInvoiceTransaction(Bundle savedInstanceState){

        if(savedInstanceState != null){
            String transactionID = savedInstanceState.getString("invoice_transaction_id");
            if(transactionID != null){
                //Reload
                if(transactionID.equals("-1") || transactionID == null){
                    loadInvoiceTransaction(null);
                    return;
                }
                DatabaseTalker.getInstance().getInvoiceTransaction(this, 5, AsyncTask.THREAD_POOL_EXECUTOR, transactionID);
            }
        }
        else if(invoiceTransaction == null) {
            Bundle b = this.getArguments();
            if(b == null) return;
            Log.d("BizApp","Bundle: "+b.toString());
            String transactionType = b.getString("transaction_type");
            if(transactionType.equals(TransactionType.TRANSACTION_TYPE_SALES)){
                invoiceTransaction = InvoicedTransaction.getNewInvoicedTransaction(Transaction.TYPE_SALES);
            }
            else if(transactionType.equals(TransactionType.TRANSACTION_TYPE_PURCHASE)){
                invoiceTransaction = InvoicedTransaction.getNewInvoicedTransaction(Transaction.TYPE_PURCHASE);
            }
            this.onReceivedDatabaseObject(invoiceTransaction, 5);
        }
        else{
            if(invoiceTransaction.isLoaded()){
                this.onReceivedDatabaseObject(invoiceTransaction, 5);
            }
            else{
                DatabaseTalker.getInstance().getInvoiceTransaction(this, 5, AsyncTask.THREAD_POOL_EXECUTOR, this.invoiceTransaction.id);
            }
        }
    }


    private void getReadWritePermission(){
        boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    112);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 112){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getActivity(), "Granted", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean validateUIFields() {
        if(invoiceTransaction.debitAccount == null || invoiceTransaction.creditAccount == null){
            Toast.makeText(getActivity(), "Choose Customer first", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(invoiceTransaction.productList.size() == 0){
            Toast.makeText(getActivity(), "Enter Products first", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void resetTransactionAndUIFields() {
        invoiceTransaction = null;
        loadInvoiceTransaction(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isBarcodeStarted == false) {
            isBarcodeStarted = true;
            //startBarCodeScanner();
        }
    }

    private void startBarCodeScanner() {
       // if (!barcodeScanner.active) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if(!barcodeScanner.active){
                        barcodeScanner.activateBarcodeScanner(getActivity());
                    }
                    barcodeScanner.startCameraSource(InvoiceFrag.this.getActivity());
                }
            }).start();
       // }
    }

    private void stopBarcodeScanner(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(barcodeScanner != null) {
                    barcodeScanner.active = false;
                    barcodeScanner.stop();
                    barcodeScanner.release();
                }
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (barcodeScanner != null) {
            //stopBarcodeScanner();
        }
    }

    private void setInvoiceTransaction(InvoicedTransaction transaction){
        this.invoiceTransaction = transaction;
        this.recyclerView.setLayoutManager(new RecyclerViewLayoutManager(getActivity()));
        this.recyclerView.setAdapter(new BaseRecyclerViewAdapter(invoiceTransaction, getActivity(), this));
        itemTouchHelper = new SimpleItemTouchHelper((BaseRecyclerViewAdapter)this.recyclerView.getAdapter());
        this.updateTotalsView();
    }

    @Override
    public void onAddOrEditProduct(Product product) {
        if(product == null){
            if(invoiceProductListFrag == null)  {
                invoiceProductListFrag = new InvoiceProductListFrag();
                invoiceProductListFrag.delegate = this;
            }
            this.transitionToFragment(invoiceProductListFrag);
        }
        else{
            this.showProductDataFragment(product, true);
        }
    }

    @Override
    public void onAddOrEditSlaveEntity(SlaveEntity slaveEntity) {
        if(slaveEntity == null){
            if(invoiceSlaveEntityListFrag == null)  {
                invoiceSlaveEntityListFrag = new InvoiceSlaveEntityListFrag();
                invoiceSlaveEntityListFrag.delegate = this;
            }
            invoiceSlaveEntityListFrag.parentList = invoiceTransaction.slaveEntities;
            this.transitionToFragment(invoiceSlaveEntityListFrag);
        }
        else{
            if(slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_PERCENT){
                InvoiceSlaveEntityDetailsFrag invoiceSlaveEntityDetailsFrag = new InvoiceSlaveEntityDetailsFrag();
                invoiceSlaveEntityDetailsFrag.delegate = this;
                invoiceSlaveEntityDetailsFrag.setSlaveEntity(slaveEntity);
                if(slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_PERCENT) {
                    invoiceSlaveEntityDetailsFrag.setProducts(this.invoiceTransaction.productList);
                }
                invoiceSlaveEntityDetailsFrag.setSlaveEntities(invoiceTransaction.slaveEntities);
                this.transitionToFragment(invoiceSlaveEntityDetailsFrag);
            }
            else if(slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_ABSOLUTE){
                InvoiceSlaveEntityValueInputFrag frag = new InvoiceSlaveEntityValueInputFrag();
                frag.delegate = InvoiceFrag.this;
                frag.setSlaveEntity(slaveEntity, true);
                FragmentManager manager = getActivity().getFragmentManager();
                frag.show(manager, "dialog");
            }
        }
    }

    @Override
    public void onMainHeaderTapped(MasterAccount masterAccount, Date date) {
        if(invoiceAccountFrag == null){
            invoiceAccountFrag = new InvoiceOnlyAccountListFrag();
            invoiceAccountFrag.delegate = this;
        }
        this.transitionToFragment(invoiceAccountFrag);
    }

    @Override
    public void onAccountSelected(MasterAccount account, WeakReference<Object> tag) {
        if(invoiceTransaction.getType() == Transaction.TYPE_SALES) {
            invoiceTransaction.setDebitAccount(account);
        }
        else if(invoiceTransaction.getType() == Transaction.TYPE_PURCHASE){
            invoiceTransaction.setCreditAccount(account);
        }
        this.recyclerView.getAdapter().notifyDataSetChanged();  //Update Item At Index: 1
    }

    private Product getProductFromBarcodeInLocalList(Barcode barcode){
        String barcodeValue = barcode.rawValue;
        for(Product p : invoiceTransaction.productList){
            if(p.barCodeValue != null) {
                if (p.barCodeValue.equals(barcodeValue)) {
                    return p;
                }
            }
        }
        return null;
    }

    @Override
    public void shouldContinueGivingBarCodes(Barcode barcode) {

        barcodeScanner.setBarcodeDetectionListener(null);
        Product p = getProductFromBarcodeInLocalList(barcode);
        if(p == null)   DatabaseTalker.getInstance().getProductFromBarcode(barcode, this, 12);
        else    onReceivedDatabaseObject(p, 11);
    }

    @Override
    public void onReceivedDatabaseObject(final Object databaseObject, int processId) {
        if(processId == 12){
            onEndBarcodeFeed();
            if(((Product)databaseObject).name != null){
                Product p = (Product)databaseObject;
                showProductDataFragment(p, false);
            }
            else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Item Not Found");
                alert.setMessage("Do you want to create a new item with this barcode?");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ProductFragment fragment = new ProductFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("barcode", ((Product) databaseObject).barCodeValue);
                        fragment.setArguments(bundle);
                        InvoiceFrag.this.transitionToFragment(fragment);
                    }
                });
                alert.setNegativeButton("No", null);
                alert.show();
            }
        }
        else if(processId == 11){
            onEndBarcodeFeed();
            if(databaseObject != null){
                Product p = (Product)databaseObject;
                showProductDataFragment(p, true);
            }
            else {
                this.barcodeScanner.setBarcodeDetectionListener(this);
            }
        }
        else if(processId == 5){
            if(databaseObject instanceof InvoicedTransaction){
                InvoicedTransaction transaction = (InvoicedTransaction) databaseObject;
                this.setInvoiceTransaction(transaction);
            }
        }
        else if(processId == 6){
            if(databaseObject == this.invoiceTransaction) {
                this.invoiceTransaction.originalValue = invoiceTransaction.getNetValue();
                openPostSaveMenu();
            }
        }
    }

    private void openPostSaveMenu(){
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Invoice Saved");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_list_item_1);
        arrayAdapter.add("Share");
        arrayAdapter.add("Print");
        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PDFInvoiceViewAndPrintFragment fragment = new PDFInvoiceViewAndPrintFragment();
                        fragment.setTransaction(InvoiceFrag.this.invoiceTransaction);
                        if(which != 0){
                            fragment.showPrintUI = true;
                        }
                        else{
                            fragment.showShareUI = true;
                        }
                        InvoiceFrag.this.transitionToFragment(fragment);

                    }
                });
        builderSingle.show();
    }

    /*
    public void onSetHeaderView(Date date, MasterAccount party) {
        if(invoiceTransaction.getType() == Transaction.TYPE_SALES) {
            invoiceTransaction.setDebitAccount(party);
        }
        else if(invoiceTransaction.getType() == Transaction.TYPE_PURCHASE){
            invoiceTransaction.setCreditAccount(party);
        }
        invoiceTransaction.date = date;
        this.recyclerView.getAdapter().notifyDataSetChanged();
    }*/

    private void updateTotalsView(){
        String netValue = invoiceTransaction.getTotalFormattedValue();
        this.totalsView.setText(netValue);
    }

    @Override
    public void didSelectItem(Product product) {
        this.showProductDataFragment(product, false);
    }

    private void showProductDataFragment(Product product, final boolean itemFromInvoiceList) {

        DialogProductDetailsEdit dialog = new DialogProductDetailsEdit(getActivity());
        dialog.setProduct(product, invoiceTransaction.getType());

        dialog.mCallback = new DialogProductDetailsEdit.DialogProductDetailsEditCallback() {
            @Override
            public void onCancel(Product product, DialogProductDetailsEdit dialog) {
                Utilities.hideKeyboard(getActivity());
            }

            @Override
            public void onSet(Product product, DialogProductDetailsEdit dialog) {
                Utilities.hideKeyboard(getActivity());
                InvoiceFrag.this.onSetProductValues(product, itemFromInvoiceList);
            }
        };

        dialog.show();

    }

    public void onSetProductValues(Product product, boolean itemFromInvoiceList) {
        if(!itemFromInvoiceList){
            this.invoiceTransaction.productList.add(product);
            if(this.invoiceProductListFrag != null) {
                this.invoiceProductListFrag.onBackPressed();
            }
        }
        this.recyclerView.getAdapter().notifyDataSetChanged();
        this.updateTotalsView();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {

    }

    @Override
    public void onFragmentOpened() {
        super.onFragmentOpened();
        this.invoiceProductListFrag = null;
        this.invoiceSlaveEntityListFrag = null;
    }

    @Override
    public void didSelectSlaveEntity(SlaveEntity slaveEntity) {
        if(slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_PERCENT){
            //invoiceSlaveEntityListFrag.onBackPressed();
            InvoiceSlaveEntityDetailsFrag invoiceSlaveEntityDetailsFrag = new InvoiceSlaveEntityDetailsFrag();
            invoiceSlaveEntityDetailsFrag.delegate = this;
            invoiceSlaveEntityDetailsFrag.setSlaveEntity(slaveEntity);
            invoiceSlaveEntityDetailsFrag.setProducts(this.invoiceTransaction.productList);
            invoiceSlaveEntityDetailsFrag.setSlaveEntities(invoiceTransaction.slaveEntities);
            invoiceSlaveEntityListFrag.transitionToFragment(invoiceSlaveEntityDetailsFrag);
            invoiceSlaveEntityDetailsFrag.previousFragment = this;
        }
        else if(slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_ABSOLUTE){
            //invoiceSlaveEntityListFrag.onBackPressed();
            InvoiceSlaveEntityValueInputFrag frag = new InvoiceSlaveEntityValueInputFrag();
            frag.delegate = this;
            frag.setSlaveEntity(slaveEntity, false);
            invoiceSlaveEntityListFrag.showSlaveEntityValueInputFrag(frag);
        }
    }

    @Override
    public void onSlaveEntityAddOrUpdated(SlaveEntity slaveEntity) {
        if(invoiceTransaction.slaveEntities.contains(slaveEntity)){
        }
        else{
            invoiceTransaction.slaveEntities.add(slaveEntity);
        }
        this.recyclerView.getAdapter().notifyDataSetChanged();
        this.updateTotalsView();
    }

    @Override
    public void onStartBarcodeFeed() {
        Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(50);
        barcodeScanner.setVisibility(View.VISIBLE);
        barcodeScanner.setBarcodeDetectionListener(this);
    }

    @Override
    public void onEndBarcodeFeed() {
        barcodeScanner.setVisibility(View.GONE);
        barcodeScanner.setBarcodeDetectionListener(null);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.d("MotionEvent: ", event.toString());
        return false;
    }

    @Override
    public void onSetSlaveEntityValue(SlaveEntity slaveEntity, boolean itemFromInvoiceList) {
        if(!itemFromInvoiceList) {
            invoiceTransaction.slaveEntities.add(slaveEntity);
        }
        this.recyclerView.getAdapter().notifyDataSetChanged();
        this.updateTotalsView();
    }

    @Override
    public void onClick(View v) {
        if(this.validateUIFields()){
            this.barcodeScanner.setBarcodeDetectionListener(null);
            if(invoiceTransaction.id.equals("-1")){
                invoiceTransaction.insertObject(this, 6, AsyncTask.THREAD_POOL_EXECUTOR);
            }
            else{
                invoiceTransaction.updateObject(this, 6, AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    public void showDatePickerDialog(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(invoiceTransaction.date);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(),InvoiceFrag.this,year,month,day);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year,monthOfYear,dayOfMonth);
        invoiceTransaction.date = calendar.getTime();
        //TODO: notify dataset changed
    }
}