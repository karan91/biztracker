package com.biztracker.fragments.Transactions.invoice;

import com.biztracker.fragments.Transactions.invoice.BaseInvoice.InvoiceFrag;

/**
 * Created by karan on 05/05/16.
 */
public class SalesInvoiceFrag extends InvoiceFrag {
    @Override
    protected String getTitle() {
        return "Sales Invoice";
    }
}
