package com.biztracker.fragments.Transactions.invoice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.fragments.Transactions.invoice.BaseInvoice.InvoiceFrag;

/**
 * Created by karan on 05/05/16.
 */
public class PurchaseInvoiceFrag extends InvoiceFrag {
    @Override
    protected String getTitle() {
        return "Purchase Invoice";
    }
}
