package com.biztracker.fragments.Transactions.invoice.BaseInvoice;

import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

import com.biztracker.CustomTextWatcher;
import com.biztracker.R;
import com.biztracker.common.DatabaseObjectAdapter;
import com.biztracker.common.Utilities;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.DatabaseObject;
import com.biztracker.databaseobjects.Product;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.additems.ProductFragment;
import com.biztracker.fragments.universal_search.adapters.ProductListSearchAdapter;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by karan on 03/12/15.
 */
public class InvoiceProductListFrag extends SuperFragment implements DatabaseTalker.queryCompleteCallBack,
        BaseUniversalSearchAdapter.onItemClickListener, View.OnClickListener, ProductFragment.ProductFragmentDelegate {

    List<Product> products;
    EditText productSearch;
    ItemSelectedDelgate delegate;
    View rootView;
    private TextView noProductView;
    private int mHeight = 10;
    private RecyclerView mRecyclerView;
    private ProductListSearchAdapter mProductAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView == null) {
            rootView = inflater.inflate(R.layout.invoice_product_list, container, false);
            noProductView = (TextView) rootView.findViewById(R.id.no_product_tv);
            rootView.findViewById(R.id.add_button).setOnClickListener(this);
            productSearch = (EditText) rootView.findViewById(R.id.product_search_et);
            products = new ArrayList<>();

            mProductAdapter = new ProductListSearchAdapter(getActivity().getApplicationContext());
            mProductAdapter.delegate = this;

            mHeight = 5 * ((int)getResources().getDimension(R.dimen.one_dp));
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.invoice_product_list);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(),LinearLayoutManager.VERTICAL,false));
            DividerItemDecoration decoration = new DividerItemDecoration(mHeight, mHeight);
            decoration.disableTopPadding = false;
            mRecyclerView.addItemDecoration(decoration);
            mRecyclerView.setAdapter(mProductAdapter);

            rootView.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNativeBackPress();
                }
            });
            CustomTextWatcher watcher = new CustomTextWatcher(textWatcherCallback);
            watcher.delayInMillis = 100;
            productSearch.addTextChangedListener(watcher);


            requestDataFromDatabaseForQuery(null);
        }
        else{
            container.removeView(rootView);
        }
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(productSearch.hasFocus()){
            productSearch.clearFocus();
            Utilities.hideKeyboard(getActivity());
        }
    }

    private CustomTextWatcher.CustomTextWatcherCallback textWatcherCallback = new CustomTextWatcher.CustomTextWatcherCallback() {
        @Override
        public void onSearchForText(String text) {
            requestDataFromDatabaseForQuery(text);
        }
    };

    public void requestDataFromDatabaseForQuery(String query) {
        if(query == null || query.trim().isEmpty()){
            DatabaseTalker.getInstance().getAllProducts(this,2, AsyncTask.SERIAL_EXECUTOR);
        }
        else{
            DatabaseTalker.getInstance().getProductsForSearchQuery(this, 1, AsyncTask.SERIAL_EXECUTOR, query);
        }
    }

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if((processId == 1 || processId == 2)&& databaseObject instanceof ArrayList){
            List<Product> items = (List<Product>)databaseObject;
            products = items;
            if(mProductAdapter != null) mProductAdapter.setItems(products);
            if(processId == 2) {
                if (products.isEmpty()) {
                    noProductView.setVisibility(View.VISIBLE);
                }
            }
            if(!products.isEmpty()){
                noProductView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onItemClicked(Object item) {
        Product copiedProduct = (Product) ((Product)item).getCopy();
        this.onNativeBackPress();
        if(delegate != null)    delegate.didSelectItem(copiedProduct);
    }

    @Override
    public void onClick(View v) {
        ProductFragment fragment = new ProductFragment();
        fragment.setProductFragmentDelegate(this);
        this.transitionToFragment(fragment);
    }

    @Override
    public void onProductCreated(ProductFragment fragment) {
        fragment.onNativeBackPress();
    }

    interface ItemSelectedDelgate{
        void didSelectItem(Product product);
    }
}
