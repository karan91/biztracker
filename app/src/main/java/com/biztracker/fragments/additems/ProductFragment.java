package com.biztracker.fragments.additems;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.biztracker.R;
import com.biztracker.activities.MainActivity;
import com.biztracker.barcodescanner.wrapper.BarcodeScanner;
import com.biztracker.common.DatabaseObjectAdapter;
import com.biztracker.common.PriceHolder;
import com.biztracker.common.QuantityHolder;
import com.biztracker.common.Utilities;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.DatabaseObject;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.Unit;
import com.biztracker.fragments.InputFragment;
import com.biztracker.fragments.SuperFragment;
import com.google.android.gms.vision.barcode.Barcode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karan on 25/08/15.
 */
public class ProductFragment extends InputFragment implements DatabaseTalker.queryCompleteCallBack, BarcodeScanner.BarcodeDetectionListener, View.OnClickListener {


    View view;
    public EditText name;
    public PriceHolder sellingPrice;
    public PriceHolder purchasePrice;
    public QuantityHolder initialQty;
    public AutoCompleteTextView unitTv;
    public ArrayList<DatabaseObject> unitList;
    public BarcodeScanner barcodeScanner;
    public ArrayAdapter unitsAdapter;
    public TextView saveBtn;
    public EditText barcodeValue;
    private Product product;
    private boolean productEditingMode = false;
    public ProductFragmentDelegate delegate;

    public interface ProductFragmentDelegate{
        void onProductCreated(ProductFragment fragment);
    }

    public void setProductFragmentDelegate(ProductFragmentDelegate delegate){
        this.delegate = delegate;
    }
    public ProductFragment(){
        mFragmentTag = "ProductFragment";
    }

    View.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(!hasFocus && (v == unitTv) ){
                validateFieldValueFromDatabase(v);
            }
            else if(((AutoCompleteTextView) v).getText().toString().trim().isEmpty()){
                ((AutoCompleteTextView)v).showDropDown();
            }
        }
    };

    private void validateFieldValueFromDatabase(View v) {
        if(v == unitTv){
            String unitString = unitTv.getText().toString().trim();
            for(DatabaseObject unit : unitList){
                if(unitString.equals(unit.name)){
                    product.unit = ((Unit) unit);
                    return;
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.product_addition, container, false);

        barcodeScanner = ((MainActivity)getActivity()).barcodeScanner;
        saveBtn = (TextView) view.findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(this);
        name = (EditText) view.findViewById(R.id.product_name);
        sellingPrice = (PriceHolder) view.findViewById(R.id.selling_price);
        purchasePrice = (PriceHolder) view.findViewById(R.id.purchase_price);
        initialQty = (QuantityHolder) view.findViewById(R.id.initial_qty);
        unitTv = (AutoCompleteTextView) view.findViewById(R.id.unit);
        unitList = new ArrayList<>();
        unitsAdapter = new DatabaseObjectAdapter(getActivity(), android.R.layout.simple_list_item_1, unitList, null);
        unitTv.setAdapter(unitsAdapter);
        unitTv.setThreshold(1);
        barcodeValue = (EditText) view.findViewById(R.id.product_id);

        view.findViewById(R.id.barcode_scanner_btn).setOnTouchListener(mBarcodeTouchListener);

        if(savedInstanceState != null){
            String productId = savedInstanceState.getString("product_id");
            if(productId != null && productId.contentEquals("-1")==false){
                DatabaseTalker.getInstance().getProductFromId(productId, this, 5);
            }
        }else {
            if (product != null) {
                loadProduct();
            }
        }

        if(this.getArguments() != null){
            String barcode = this.getArguments().getString("barcode");
            if(barcode != null) {
                barcodeValue.setText(barcode);
            }
        }
        queryTypeAndUnitList();
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(product != null) outState.putString("product_id",product.id);
    }

    @Override
    public boolean validateUIFields() {


        if(name.getText().toString().trim().equals("") || (unitTv.getText().toString().trim().equals(""))){
            Toast.makeText(getActivity(), "Name or Unit field cannot be empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void queryTypeAndUnitList() {
        DatabaseTalker.getInstance().getAllUnits(this, 3, AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private View.OnTouchListener mBarcodeTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                showBarCodeScanner();
                Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(50);
            }else if(event.getAction() == MotionEvent.ACTION_CANCEL){
                hideBarCodeScanner();
            }else if(event.getAction() == MotionEvent.ACTION_UP){
                hideBarCodeScanner();
            }
            return true;
        }
    };

    private void showBarCodeScanner(){
        view.findViewById(R.id.parent_product_layout).requestFocus();
        Utilities.hideKeyboard(getActivity());
        if(barcodeScanner != null){
            barcodeScanner.setBarcodeDetectionListener(this);
            barcodeScanner.setVisibility(View.VISIBLE);
        }
    }

    private void hideBarCodeScanner(){
        if(barcodeScanner != null){
            barcodeScanner.setVisibility(View.GONE);
            barcodeScanner.setBarcodeDetectionListener(null);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        hideBarCodeScanner();
    }

    private void loadProduct(){
        name.setText(product.name);
        sellingPrice.setValue(product.sellingPrice);
        purchasePrice.setValue(product.purchasePrice);
        unitTv.setText(product.unit.displayName);
        initialQty.setValue(product.openingUnits);
        if(product.barCodeValue != null) shouldContinueGivingBarCodes(null);
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if(processId == 1){
            Product p = (Product)databaseObject;
            if(p.name != null){
                product = p;
                loadProduct();
            }
        }
        else if(processId == 2){
            if(productEditingMode && databaseObject != null){
                View view = this.getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                Toast.makeText(getActivity(), "Product Updated", Toast.LENGTH_SHORT).show();
                this.onNativeBackPress();
            }
            else if(databaseObject != null) {
                View view = this.getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                Toast.makeText(getActivity(), "Product saved!", Toast.LENGTH_SHORT).show();
                this.clearFields();
                if(this.getArguments() != null){
                    String barcode = this.getArguments().getString("barcode");
                    if(barcode != null){
                        this.onNativeBackPress();
                    }
                }
                if(this.delegate != null){
                    this.delegate.onProductCreated(this);
                }
            }
        }
        else if(processId == 3){
            if(databaseObject != null){
                unitList.addAll((List)databaseObject);
                unitsAdapter.notifyDataSetChanged();
                setLastUsedUnit();
            }
        }

        else if(processId == 5 && databaseObject != null){
            this.product = (Product) databaseObject;
            this.loadProduct();
        }
    }

    @Override
    public void shouldContinueGivingBarCodes(Barcode barcode) {
        barcodeScanner.setBarcodeDetectionListener(null);
        barcodeScanner.setVisibility(View.GONE);
        if(barcode == null){
            barcodeValue.setText(product.barCodeValue);
        }
        else{
            DatabaseTalker.getInstance().getProductFromBarcode(barcode, this, 1);
            barcodeValue.setText(barcode.rawValue);
        }
    }

    @Override
    public void onClick(View v) {
        if(validateUIFields()) {
            if (product == null) {
                product = new Product();
                product.closingUnits = initialQty.getValue();
                product.openingUnits = initialQty.getValue();
            }
            else{
                BigDecimal delta = product.closingUnits.subtract(product.openingUnits);
                product.openingUnits = initialQty.getValue();
                product.closingUnits = product.openingUnits.add(delta);
            }
            product.name = name.getText().toString().trim();
            product.sellingPrice = sellingPrice.getValue();
            product.purchasePrice = purchasePrice.getValue();
            for(DatabaseObject unit : unitList){
                if(unit.name.equals(unitTv.getText().toString().trim())){
                    product.unit = (Unit) unit;
                    break;
                }
            }
            if(product.unit == null) {
                product.unit = new Unit();
                product.unit.displayName = unitTv.getText().toString().trim();
                product.unit.name = unitTv.getText().toString().trim();
                updateLastUnitUsed();
            }
            product.barCodeValue = barcodeValue.getText().toString();
            product.insertObject(this, 2, AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    public void setProduct(Product product){
        this.product = product;
        this.productEditingMode = true;
    }

    private void clearFields(){
        view.findViewById(R.id.parent_product_layout).requestFocus();
        name.setText("");
        sellingPrice.setText("");
        purchasePrice.setText("");
        initialQty.setText("");
        //unitTv.setText("");
        barcodeValue.setText("");
        product = null;
    }

    private void updateLastUnitUsed(){
        SharedPreferences preferences = getActivity().getSharedPreferences("unit_prefs", Context.MODE_PRIVATE);
        preferences.edit().putString("unit_last_used", product.unit.name).commit();
    }

    private void setLastUsedUnit(){
        SharedPreferences preferences = getActivity().getSharedPreferences("unit_prefs", Context.MODE_PRIVATE);
        String unit = preferences.getString("unit_last_used", null);
        if(unit != null){
            for(DatabaseObject object : unitList){
                if(object.name.equals(unit)){
                    unitTv.setText(unit);
                    break;
                }
            }
        }
    }

    @Override
    public void resetTransactionAndUIFields() {
        product = null;
        clearFields();
    }
}