package com.biztracker.fragments.additems;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.biztracker.R;

/**
 * Created by shubham on 11/06/16.
 */
public class ProductEditingFragment extends ProductFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ImageView backButton = (ImageView) view.findViewById(R.id.back_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNativeBackPress();
            }
        });
        backButton.setImageResource(R.drawable.ic_keyboard_backspace_white_24dp);
        ((TextView)view.findViewById(R.id.header)).setText("Edit Product");
        return view;
    }
}
