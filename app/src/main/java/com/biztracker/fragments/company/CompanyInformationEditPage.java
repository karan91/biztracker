package com.biztracker.fragments.company;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.biztracker.R;
import com.biztracker.activities.MainActivity;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.Company;
import com.biztracker.fragments.registration.RegistrationObject;
import com.biztracker.fragments.registration.fragments.CurrencyChooserFrag;
import com.biztracker.fragments.registration.fragments.SetupFragment;
import com.biztracker.views.RobotoMediumEditText;

import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by shubham on 14/06/16.
 */
public class CompanyInformationEditPage extends SetupFragment implements DatePickerDialog.OnDateSetListener,
        DatabaseTalker.queryCompleteCallBack,CurrencyChooserFrag.CurrencySelectionListener, View.OnClickListener {
    View parentView;
    protected EditText personName, companyName, phoneNumber, emailField, taxationField;
    protected RobotoMediumEditText addressField,businessWebsiteField;
    private TextView financialYearView;
    private TextView currencyView;

    private String taxationString = "";
    private int financialYear = 2016;
    private int financialMonth = 4;
    private int financialDay = 1;

    private Currency mCurrency = Currency.getInstance(Locale.getDefault());

    private final static String months[] = new String[]{
            "Jan","Feb","March","April","May","June","July","Aug","Sept","Oct","Nov","Dec"
    };

    private LinearLayout llayout;

    protected View nextButton;

    String personNameString = "", companyNameString = "", phoneNumberString = "", addressString = "", emailFieldString = "", businessWebsiteString = "";

    @Override
    public boolean validateUIFields() {
        personNameString = personName.getText().toString().trim();
        companyNameString = companyName.getText().toString().trim();
        phoneNumberString = phoneNumber.getText().toString().trim();
        addressString = addressField.getText().toString().trim();
        emailFieldString = emailField.getText().toString().trim();
        businessWebsiteString = businessWebsiteField.getText().toString().trim();
        taxationString = taxationField.getText().toString().trim();
        if(personNameString.isEmpty() || companyNameString.isEmpty()){
            return false;
        }

        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(parentView == null){
            parentView = inflater.inflate(R.layout.company_information_edit_layout,container,false);
            parentView.findViewById(R.id.back_button).setOnClickListener(this);
            llayout = (LinearLayout) parentView.findViewById(R.id.llayout);

            personName = (EditText) llayout.findViewById(R.id.person_name_et);
            companyName = (EditText) llayout.findViewById(R.id.b_title_et);
            phoneNumber = (EditText) llayout.findViewById(R.id.contact_et);
            emailField = (EditText) llayout.findViewById(R.id.company_email_et);

            businessWebsiteField = (RobotoMediumEditText) llayout.findViewById(R.id.website_et);
            addressField = (RobotoMediumEditText) llayout.findViewById(R.id.address_et);

            taxationField = (EditText)llayout.findViewById(R.id.taxation_et);
            financialYearView = (TextView)parentView.findViewById(R.id.financial_year);
            currencyView = (TextView) parentView.findViewById(R.id.currency);


            nextButton = parentView.findViewById(R.id.next_btn);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onSaveClicked();
                }
            });


            mRegistrationObject = new RegistrationObject(getContext());

            //personName.addTextChangedListener(mUserNameTextWatcher);
            //companyName.addTextChangedListener(mCompanyNameTextWatcher);
            //addressField.addTextChangedListener(mAddressTextWatcher);

            if(mRegistrationObject != null){
                personNameString = mRegistrationObject.userFullName;
                companyNameString = mRegistrationObject.businessName;
                phoneNumberString = mRegistrationObject.contactNumber;
                addressString = mRegistrationObject.businessAddress;
                emailFieldString = mRegistrationObject.businessEmail;
                businessWebsiteString = mRegistrationObject.businessWebsite;
                taxationString = mRegistrationObject.taxationNumber;

                financialYear = (int)mRegistrationObject.financialYear;
                financialMonth = (int)mRegistrationObject.financialMonth;
                financialDay = (int)mRegistrationObject.financialDay;

                personName.setText(mRegistrationObject.userFullName);
                companyName.setText(mRegistrationObject.businessName);
                phoneNumber.setText(mRegistrationObject.contactNumber);
                addressField.setText(mRegistrationObject.businessAddress);
                emailField.setText(mRegistrationObject.businessEmail);
                businessWebsiteField.setText(mRegistrationObject.businessWebsite);
                taxationField.setText(mRegistrationObject.taxationNumber);
                String dateString = getDateStringFrom((int)mRegistrationObject.financialYear,(int)mRegistrationObject.financialMonth,(int)mRegistrationObject.financialDay);
                financialYearView.setText(dateString);
            }

            financialYearView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatePickerDialog dialog = new DatePickerDialog(getActivity(),CompanyInformationEditPage.this,
                            financialYear,financialMonth - 1,financialDay);
                    dialog.show();
                }
            });

            parentView.findViewById(R.id.currency_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CurrencyChooserFrag frag = new CurrencyChooserFrag();
                    frag.delegate = CompanyInformationEditPage.this;
                    frag.show(getActivity().getSupportFragmentManager(), "currency_chooser");
                }
            });

            String currencyCode = mRegistrationObject.currencyCode;
            Currency currency = Currency.getInstance(currencyCode);
            if(currency != null){
                mCurrency = currency;
                currencyView.setText(currency.getSymbol());
            }else{
                currencyView.setText(currencyCode);
            }

        }else{
            container.removeView(parentView);
        }

        return parentView;
    }

    public String getDateStringFrom(int finYear,int finMonth, int finDay){
        financialYear = finYear;
        financialMonth = finMonth;
        financialDay = finDay;
        String dateString = String.format("%s %d, %d",months[finMonth - 1],finDay,finYear);
        return dateString;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if(financialYearView != null){
            financialYearView.setText(getDateStringFrom(year,monthOfYear + 1,dayOfMonth));
        }
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if(processId == 1 && databaseObject != null){
            View view = this.getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            Toast.makeText(getActivity(), "Company Updated", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCurrencySelected(String currencyCode) {
        Currency currency = Currency.getInstance(currencyCode);
        currencyView.setText(currency.getSymbol());
       mCurrency = currency;
    }

    public void onSaveClicked(){

        if(validateUIFields()) {

            mRegistrationObject.userFullName = personNameString;
            mRegistrationObject.businessName = companyNameString;
            mRegistrationObject.businessEmail = emailFieldString;
            mRegistrationObject.businessWebsite = businessWebsiteString;
            mRegistrationObject.businessAddress = addressString;
            mRegistrationObject.contactNumber = phoneNumberString;
            mRegistrationObject.taxationNumber = taxationString;

            mRegistrationObject.financialDay = (long) financialDay;
            mRegistrationObject.financialMonth = (long) financialMonth;
            mRegistrationObject.financialYear = (long) financialYear;
            mRegistrationObject.currencyCode = mCurrency.getCurrencyCode();

            mRegistrationObject.storeInPreferences(getContext());

            Company company = Company.company;
            company.personsName = personNameString;
            company.businessName = companyNameString;
            company.emailAddress = emailFieldString;
            company.businessWebsite = businessWebsiteString;
            company.address = addressString;
            company.phone = phoneNumberString;
            company.taxationNumber = taxationString;
            company.startingDate = financialYearView.getText().toString();
            company.currency = mCurrency;
            company.updateObject(this, 1, AsyncTask.THREAD_POOL_EXECUTOR);
        }
        //Save

        ((MainActivity)getActivity()).companyNameHolder.setText(companyNameString);

        onNativeBackPress();
    }

    @Override
    public void onClick(View v) {
        onNativeBackPress();
    }
}
