package com.biztracker.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.biztracker.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karan on 04/10/15.
 */
public class HomeScreen extends SuperFragment{


    ListView listView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_screen, container, false);
        listView = (ListView) v.findViewById(R.id.parent_list_view);
        List<String> objects = new ArrayList<>();
        objects.add("Product");
        objects.add("Master account");
        objects.add("Date Importer");
        objects.add("New Invoice");
        objects.add("Product list");
        objects.add("Universal Search");
        objects.add("Discount/Taxes");
        listView.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, objects));
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.save).setVisible(false);
    }

    @Override
    public boolean validateUIFields() {
        return false;
    }

}
