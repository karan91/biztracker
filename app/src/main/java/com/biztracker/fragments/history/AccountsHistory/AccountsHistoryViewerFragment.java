package com.biztracker.fragments.history.AccountsHistory;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.activities.BaseActivity;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.Company;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.PaymentReceiptTransaction;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.dialogs.ContactNoAdditionDialog;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.DividerItemDecoration;
import com.biztracker.fragments.Transactions.invoice.ViewablePurchaseInvoiceFrag;
import com.biztracker.fragments.Transactions.invoice.ViewableSalesInvoiceFrag;
import com.biztracker.fragments.Transactions.paymentsAndReceipts.ViewablePaymentFrag;
import com.biztracker.fragments.Transactions.paymentsAndReceipts.ViewableReceiptFrag;
import com.biztracker.fragments.masteraccounts.CustomerAccountEditingFragment;
import com.biztracker.fragments.masteraccounts.CustomerAccountFragment;
import com.biztracker.fragments.masteraccounts.MasterAccountFragment;
import com.biztracker.fragments.masteraccounts.OtherAccountEditingFragment;
import com.biztracker.fragments.masteraccounts.OtherAccountFragment;
import com.biztracker.fragments.masteraccounts.SellerAccountEditingFragment;
import com.biztracker.fragments.masteraccounts.SellerAccountFragment;
import com.biztracker.fragments.universal_search.adapters.AccountInvoicesListSearchAdapter;
import com.biztracker.fragments.universal_search.adapters.InvoiceListSearchAdapter;
import com.biztracker.fragments.universal_search.adapters.base.BaseUniversalSearchAdapter;
import com.biztracker.permissions.PermissionRequester;
import com.biztracker.prints.PDFInvoiceViewAndPrintFragment;
import com.biztracker.prints.PDFPaymentFragment;
import com.biztracker.prints.PDFReceiptFragment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham on 12/04/16.
 */
public class AccountsHistoryViewerFragment extends SuperFragment implements DatabaseTalker.queryCompleteCallBack,
        InvoiceListSearchAdapter.InvoiceCellActionCallback, BaseUniversalSearchAdapter.onItemClickListener{
    View parentView;
    RecyclerView recyclerView;
    TextView balanceView;
    TextView noTransactionsView;
    public MasterAccount masterAccount;
    List<InvoicedTransaction> transactionList;

    private AccountInvoicesListSearchAdapter mAdapter;

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(parentView != null){
            masterAccount.refreshObjectFromDatabase(this, 6, AsyncTask.THREAD_POOL_EXECUTOR);
        }
        parentView = inflater.inflate(R.layout.account_history_layout, container, false);
        noTransactionsView = (TextView) parentView.findViewById(R.id.no_transactions_yet);
        transactionList = new ArrayList<>();

        recyclerView = (RecyclerView) parentView.findViewById(R.id.accounts_history_rv);
        mAdapter = new AccountInvoicesListSearchAdapter(getActivity().getApplicationContext(),this);
        recyclerView.setAdapter(mAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(),LinearLayoutManager.VERTICAL,false));
        DividerItemDecoration decoration = new DividerItemDecoration(20, 20);
        decoration.disableTopPadding = false;
        recyclerView.addItemDecoration(decoration);

        balanceView = (TextView) parentView.findViewById(R.id.account_balance_tv);

        parentView.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNativeBackPress();
            }
        });


        if(masterAccount != null){
            loadMasterAccountInView();
        }else if(savedInstanceState != null){
            String masterAccountId = savedInstanceState.getString("master_account_id");
            if(masterAccountId != null){
                DatabaseTalker.getInstance().getMasterAccount(masterAccountId, this, 5, AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }


        return parentView;
    }

    public void loadMasterAccountInView(){
        if(parentView != null && masterAccount != null){
            ((TextView)parentView.findViewById(R.id.account_name)).setText(masterAccount.name);
            DatabaseTalker.getInstance().getAllTransactionsForMasterAccount(this, 1, AsyncTask.THREAD_POOL_EXECUTOR, masterAccount);
            balanceView.setText(masterAccount.getFormattedClosingBalance());
            parentView.findViewById(R.id.edit_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //TODO: Show Edit Account Screen
                    onEditClicked();
                }
            });
            mAdapter.masterAccount = this.masterAccount;
            parentView.findViewById(R.id.action_call).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(masterAccount.phone != null && masterAccount.phone.isEmpty() == false){
                        callOnPhone(masterAccount.phone);
                    }else{
                        ContactNoAdditionDialog dialog = new ContactNoAdditionDialog(getActivity());
                        dialog.mCallback = mContactAdditionCallback;
                        dialog.show();
                    }
                }
            });
        }
    }

    public void onEditClicked(){
        MasterAccountFragment masterAccountFragment;
        if(masterAccount.accountType.name.equals("Customer")){
            masterAccountFragment = new CustomerAccountEditingFragment();
        }
        else if(masterAccount.accountType.name.equals("Supplier")){
            masterAccountFragment = new SellerAccountEditingFragment();

        }
        else{
            masterAccountFragment = new OtherAccountEditingFragment();
        }
        masterAccountFragment.setMasterAccount(masterAccount);
        AccountsHistoryViewerFragment.this.transitionToFragment(masterAccountFragment);
    }


    public ContactNoAdditionDialog.ContactNoAdditionDialogCallback mContactAdditionCallback = new ContactNoAdditionDialog.ContactNoAdditionDialogCallback() {
        @Override
        public void onAddPhoneNo() {
            onEditClicked();
        }
    };

    public void callOnPhone(final String phone){

        BaseActivity activity = (BaseActivity) getActivity();
        activity.requestPermission(PermissionRequester.Permissions.CALL_USER, new PermissionRequester.OnPermissionRequesterCompleted() {
            @Override
            public void onPermissionGranted(String permission) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + phone));
                try {
                    getContext().startActivity(intent);
                }catch (Exception ex){
                    Log.e("BizApp","Exception: "+ex);
                }
            }

            @Override
            public void onPermissionCancelled(String permission) {
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(masterAccount != null && masterAccount.id != null) outState.putString("master_account_id",masterAccount.id);
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if(processId == 1 && databaseObject != null){
            BaseUniversalSearchAdapter searchAdapter = (BaseUniversalSearchAdapter)recyclerView.getAdapter();
            List<Transaction> items = (List<Transaction>)databaseObject;
            if (searchAdapter != null) {
                searchAdapter.delegate = AccountsHistoryViewerFragment.this;
                if(items.isEmpty()){
                    noTransactionsView.setVisibility(View.VISIBLE);
                }
                else{
                    noTransactionsView.setVisibility(View.GONE);
                }
                searchAdapter.setItems(items);
            } else {
                Log.e("UniSearchFrag", "onReceivedDatabaseObject - Search Adapter is null: " + this);
            }

            balanceView.setText(masterAccount.getFormattedClosingBalance());
        }

        else if(processId == 5 && databaseObject != null){
            this.masterAccount = (MasterAccount) databaseObject;
            loadMasterAccountInView();
        }

        else if(processId == 6 && databaseObject != null){
            balanceView.setText(masterAccount.getFormattedClosingBalance());
        }
    }

    @Override
    public void onViewItem(Transaction transaction) {
        if(transaction.getType() == Transaction.TYPE_PURCHASE){
            ViewablePurchaseInvoiceFrag fragment = new ViewablePurchaseInvoiceFrag();
            fragment.invoiceTransaction = (InvoicedTransaction) transaction;
            transitionToFragment(fragment);
        }
        else if(transaction.getType() == Transaction.TYPE_SALES){
            ViewableSalesInvoiceFrag fragment = new ViewableSalesInvoiceFrag();
            fragment.invoiceTransaction = (InvoicedTransaction) transaction;
            transitionToFragment(fragment);
        }
        else if(transaction.getType() == Transaction.TYPE_PAYMENT){
            ViewablePaymentFrag fragment = new ViewablePaymentFrag();
            fragment.setTransaction((PaymentReceiptTransaction)transaction);
            transitionToFragment(fragment);
        }
        else if(transaction.getType() == Transaction.TYPE_RECEIPT){
            ViewableReceiptFrag fragment = new ViewableReceiptFrag();
            fragment.setTransaction((PaymentReceiptTransaction)transaction);
            transitionToFragment(fragment);
        }
    }


    @Override
    public void onPrintItem(Transaction transaction) {
        if(transaction instanceof InvoicedTransaction){
            PDFInvoiceViewAndPrintFragment fragment = new PDFInvoiceViewAndPrintFragment();
            fragment.showPrintUI = true;
            fragment.setTransaction((InvoicedTransaction) transaction);
            this.transitionToFragment(fragment);
        }
        else if(transaction instanceof PaymentReceiptTransaction){
            if(transaction.getType() == Transaction.TYPE_PAYMENT){
                PDFPaymentFragment fragment = new PDFPaymentFragment();
                fragment.showPrintUI = true;
                fragment.setTransaction((PaymentReceiptTransaction) transaction);
                this.transitionToFragment(fragment);
            }
            else if(transaction.getType() == Transaction.TYPE_RECEIPT){
                PDFReceiptFragment fragment = new PDFReceiptFragment();
                fragment.showPrintUI = true;
                fragment.setTransaction((PaymentReceiptTransaction) transaction);
                this.transitionToFragment(fragment);
            }
        }
    }

    @Override
    public void onShareItem(Transaction transaction) {
        if(transaction instanceof InvoicedTransaction){
            PDFInvoiceViewAndPrintFragment fragment = new PDFInvoiceViewAndPrintFragment();
            fragment.showShareUI = true;
            fragment.setTransaction((InvoicedTransaction) transaction);
            this.transitionToFragment(fragment);
        }
        else if(transaction instanceof PaymentReceiptTransaction){
            if(transaction.getType() == Transaction.TYPE_PAYMENT){
                PDFPaymentFragment fragment = new PDFPaymentFragment();
                fragment.showShareUI = true;
                fragment.setTransaction((PaymentReceiptTransaction) transaction);
                this.transitionToFragment(fragment);
            }
            else if(transaction.getType() == Transaction.TYPE_RECEIPT){
                PDFReceiptFragment fragment = new PDFReceiptFragment();
                fragment.showShareUI = true;
                fragment.setTransaction((PaymentReceiptTransaction) transaction);
                this.transitionToFragment(fragment);
            }
        }
    }

    @Override
    public void onItemClicked(Object item) {

    }
}
