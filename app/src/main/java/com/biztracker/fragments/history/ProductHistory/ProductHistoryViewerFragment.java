package com.biztracker.fragments.history.ProductHistory;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.Company;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.DividerItemDecoration;
import com.biztracker.fragments.Transactions.invoice.ViewablePurchaseInvoiceFrag;
import com.biztracker.fragments.Transactions.invoice.ViewableSalesInvoiceFrag;
import com.biztracker.fragments.additems.ProductEditingFragment;
import com.biztracker.fragments.additems.ProductFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham on 11/04/16.
 */
public class ProductHistoryViewerFragment extends SuperFragment implements ProductHistoryAdapter.ProductHistoryTapListener, DatabaseTalker.queryCompleteCallBack{

    View parentView;
    RecyclerView recyclerView;
    TextView balanceView;
    public Product product;
    TextView noTransactionView;

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.product_history_layout, container, false);
        recyclerView = (RecyclerView) parentView.findViewById(R.id.product_history_rv);

        parentView.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNativeBackPress();
            }
        });
        noTransactionView = (TextView) parentView.findViewById(R.id.no_transactions_yet);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        DividerItemDecoration decoration = new DividerItemDecoration(20, 20);
        decoration.disableTopPadding = false;
        recyclerView.addItemDecoration(decoration);
        container.removeView(parentView);

        if(product != null){
            setProductHistoryView();
        }else if(savedInstanceState != null){
            String productId = savedInstanceState.getString("product_id");
            if(productId != null){
                //DatabaseTalker.getInstance().getMasterAccount(masterAccountId, this, 5, AsyncTask.THREAD_POOL_EXECUTOR);
                DatabaseTalker.getInstance().getProductFromId(productId, this, 5);
            }

        }

        return parentView;
    }

    private void setProductHistoryView(){
        if(parentView != null){
            product.productHistoryList = new ArrayList<>();
            ProductHistoryAdapter adapter = new ProductHistoryAdapter(getActivity(), product.productHistoryList);
            recyclerView.setAdapter(adapter);
            adapter.delegate = this;

            ((TextView) parentView.findViewById(R.id.product_name)).setText(product.name);

            parentView.findViewById(R.id.edit_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProductEditingFragment productFragment = new ProductEditingFragment();
                    productFragment.setProduct(product);
                    ProductHistoryViewerFragment.this.transitionToFragment(productFragment);
                    //Show Product Editing Fragment
                }
            });

            balanceView = (TextView) parentView.findViewById(R.id.account_balance_tv);
            balanceView.setText(product.closingUnits + " " + product.unit.displayName);
            DatabaseTalker.getInstance().getProductHistoryList(product, this, 1, AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(product != null && product.id != null) outState.putString("product_id",product.id);
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if (processId == 1 && databaseObject != null) {
            List<ProductHistoryBean> trans = (List<ProductHistoryBean>) databaseObject;
            product.setProductHistoryList(trans);
            recyclerView.getAdapter().notifyDataSetChanged();
            if(trans.isEmpty()){
                noTransactionView.setVisibility(View.VISIBLE);
            }
            else{
                noTransactionView.setVisibility(View.GONE);
            }
        }

        else if(processId == 5 && databaseObject != null){
            this.product = (Product) databaseObject;
            setProductHistoryView();
        }
    }

    @Override
    public void onProductHistoryBeanTapped(ProductHistoryBean productHistoryBean) {
        InvoicedTransaction transaction = new InvoicedTransaction(productHistoryBean.transactionId);
        transaction.setType(productHistoryBean.transactionType);
        onViewItem(transaction);
    }

    public void onViewItem(InvoicedTransaction transaction) {
        Log.d("BizApp","View");
        if(transaction.getType() == Transaction.TYPE_PURCHASE){
            ViewablePurchaseInvoiceFrag fragment = new ViewablePurchaseInvoiceFrag();
            fragment.invoiceTransaction = transaction;
            transitionToFragment(fragment);
        }
        else if(transaction.getType() == Transaction.TYPE_SALES){
            ViewableSalesInvoiceFrag fragment = new ViewableSalesInvoiceFrag();
            fragment.invoiceTransaction = transaction;
            transitionToFragment(fragment);
        }
    }
}