package com.biztracker.fragments.history.ProductHistory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by karan on 17/04/16.
 */
public class ProductHistoryAdapter extends RecyclerView.Adapter{

    public Context context;
    public List<ProductHistoryBean> productHistoryList;
    ProductHistoryTapListener delegate;

    public ProductHistoryAdapter(Context context, List<ProductHistoryBean> list){
        this.context = context;
        this.productHistoryList = list;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductHistoryViewHolder(LayoutInflater.from(context).inflate(R.layout.product_history_invoice_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProductHistoryBean productHistoryBean = this.productHistoryList.get(position);
        ((ProductHistoryViewHolder)holder).setItem(productHistoryBean);
    }

    @Override
    public int getItemCount() {
        return this.productHistoryList.size();
    }

    class ProductHistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView masterAccountHolder;
        TextView quantityTextView;
        TextView perUnitPriceTextView;
        TextView transactionTypeViewHolder;
        TextView transactionDateHolder;
        ProductHistoryBean productHistoryBean;
        View parentView;

        public ProductHistoryViewHolder(View itemView) {
            super(itemView);
            masterAccountHolder = (TextView) itemView.findViewById(R.id.master_account_name_holder);
            quantityTextView = (TextView) itemView.findViewById(R.id.quantity_field);
            perUnitPriceTextView = (TextView)itemView.findViewById(R.id.price_field);
            transactionTypeViewHolder = (TextView) itemView.findViewById(R.id.transaction_label_tag);
            transactionDateHolder = (TextView) itemView.findViewById(R.id.product_history_date_tv);
            this.parentView = itemView;
            this.parentView.setOnClickListener(this);
        }
        public void setItem(ProductHistoryBean _item) {
            this.productHistoryBean = _item;
            masterAccountHolder.setText(productHistoryBean.masterAccountName);
            quantityTextView.setText(productHistoryBean.getQuantityText());
            perUnitPriceTextView.setText(productHistoryBean.getPricePerUnitText());
            transactionTypeViewHolder.setText(productHistoryBean.getTransactionTypeViewTxt());
            parentView.setTag(_item);

            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            String formattedDate = "";
            try {
                Date date = format.parse(this.productHistoryBean.transactionDate);
                format = new SimpleDateFormat("dd-MMM-yyyy");
                formattedDate = format.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            transactionDateHolder.setText(formattedDate);
        }

        @Override
        public void onClick(View v) {
            if(ProductHistoryAdapter.this.delegate != null){
                ProductHistoryAdapter.this.delegate.onProductHistoryBeanTapped(productHistoryBean);
            }
        }
    }

    interface ProductHistoryTapListener{
        void onProductHistoryBeanTapped(ProductHistoryBean productHistoryBean);
    }
}
