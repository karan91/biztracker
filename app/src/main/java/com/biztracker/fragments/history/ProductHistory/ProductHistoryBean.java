package com.biztracker.fragments.history.ProductHistory;

import android.database.Cursor;

import com.biztracker.databaseobjects.Company;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.databaseobjects.TransactionType;
import com.biztracker.databaseobjects.Unit;

import java.lang.ref.SoftReference;
import java.math.BigDecimal;

/**
 * Created by karan on 17/04/16.
 */
public class ProductHistoryBean{

    public String transactionDate;
    public int transactionType;
    public String masterAccountName;
    public String transactionId;
    public BigDecimal transactionPrice;
    public BigDecimal transactionQuantity;
    public SoftReference<ProductHistoryDelegate> delegate;

    public ProductHistoryBean(Cursor c) {
        this.transactionType = c.getInt(c.getColumnIndex("FOREIGN_TRANSACTION_TYPES_ID"));
        this.transactionId = c.getString(c.getColumnIndex("FOREIGN_MASTER_TRANSACTION_LIST_ID"));
        this.transactionPrice = new BigDecimal(c.getDouble(c.getColumnIndex("MASTER_TRANSACTION_DETAILS_PRICE")));
        this.transactionQuantity = new BigDecimal(c.getDouble(c.getColumnIndex("MASTER_TRANSACTION_DETAILS_QUANTITY")));
        //TODO: Format date to specific format
        this.transactionDate = String.valueOf(c.getInt(c.getColumnIndex("TRANSACTION_LIST_DATE")));
    }



    public String getQuantityText(){
        return this.transactionQuantity.toString() + this.delegate.get().getProductUnit().displayName;
    }

    public String getPricePerUnitText(){
        return Company.company.currency.getSymbol() + " " + this.transactionPrice.toString() ;
    }

    public String getTransactionTypeViewTxt(){
        if(transactionType == Transaction.TYPE_SALES){
            return "SOLD";
        }
        else if(transactionType == Transaction.TYPE_PURCHASE){
            return "PURCHASED";
        }
        return "";
    }

    public interface ProductHistoryDelegate{
        Unit getProductUnit();
    }
}
