package com.biztracker.fragments.history.AccountsHistory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.Transaction;

import java.util.List;

/**
 * Created by karan on 03/02/16.
 */
public class UniSearchAdapter extends RecyclerView.Adapter<UniSearchAdapter.BaseRecyclerViewHolder> {

    List<Transaction> transactionList;
    Context context;
    public OnTransactionTappedListener delegate;

    public UniSearchAdapter(List<Transaction> transactions, Context context){
        this.context = context;
        this.transactionList = transactions;
    }
    @Override
    public UniSearchAdapter.BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = LayoutInflater.from(context).inflate(R.layout.uni_search_transaction_cell, parent, false);
        InvoiceViewHolder holder = new InvoiceViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(delegate != null){
                    delegate.onTransactionTapped((InvoicedTransaction) view.getTag());
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(UniSearchAdapter.BaseRecyclerViewHolder holder, int position) {
        InvoicedTransaction transaction = (InvoicedTransaction) transactionList.get(position);
        holder.setItem(transaction);
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    abstract class BaseRecyclerViewHolder extends RecyclerView.ViewHolder{

        public Object item;
        public View parentView;

        public BaseRecyclerViewHolder(View itemView) {
            super(itemView);
            this.parentView = itemView;
        }
        public void setItem(Object _item){
            item = _item;
            parentView.setTag(_item);
        }
    }

    private class InvoiceViewHolder extends BaseRecyclerViewHolder{

        TextView activeAccountLabel, dateLabel, totalValueLabel, transactionTypeTag;
        public InvoiceViewHolder(View itemView) {
            super(itemView);
            activeAccountLabel = (TextView) parentView.findViewById(R.id.active_account_label);
            dateLabel = (TextView) parentView.findViewById(R.id.date_label);
            totalValueLabel = (TextView) parentView.findViewById(R.id.total_label);
            transactionTypeTag = (TextView) parentView.findViewById(R.id.transaction_label_tag);
        }

        @Override
        public void setItem(Object item) {
            super.setItem(item);
            InvoicedTransaction transaction = (InvoicedTransaction)item;
            this.activeAccountLabel.setText(transaction.getActiveAccount().name);
            this.dateLabel.setText(transaction.getPrettyDate());
            if(transaction.getType() == Transaction.TYPE_SALES){
                transactionTypeTag.setText("SALE");
            }
            else if(transaction.getType() == Transaction.TYPE_PURCHASE){
                transactionTypeTag.setText("PURCHASE");
            }
            totalValueLabel.setText(transaction.getTotalValueOfInvoice().toString());
        }
    }

    public interface OnTransactionTappedListener{
        void onTransactionTapped(InvoicedTransaction transaction);
    }
}
