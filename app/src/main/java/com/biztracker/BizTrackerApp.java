package com.biztracker;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.biztracker.fragments.registration.BizTrackerInstallSettings;
import com.biztracker.utilities.Session;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by shubham on 09/07/15.
 */
public class BizTrackerApp extends Application {

    private static Context mAppContext;

    @Override
    public void onCreate() {
        super.onCreate();
        BizTrackerApp.mAppContext = getApplicationContext();

        Session.getInstance();
        Fabric.with(this, new Crashlytics());

        new BizTrackerInstallSettings(getApplicationContext());
    }

    public static Context getAppContext(){
        Log.d("BizApp","Context: "+BizTrackerApp.mAppContext);
        return BizTrackerApp.mAppContext;
    }

    public static Boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


}
