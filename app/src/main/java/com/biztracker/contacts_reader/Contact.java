package com.biztracker.contacts_reader;

import android.util.Log;

/**
 * Created by shubham on 10/04/16.
 */
public class Contact {

    public final static int CONTACT_TYPE_PHONE = 12;
    public final static int CONTACT_TYPE_EMAIL_ONLY = 13;

    public String CONTACT_ID;   //Not Used
    public int CONTACT_TYPE = CONTACT_TYPE_PHONE;

    public String email;
    public String phoneNo;
    public String androidContactID;
    public String name;
    public boolean isUsed = false;


    public void display(){
        if(CONTACT_TYPE == CONTACT_TYPE_EMAIL_ONLY){
            Log.d("Contact","EmailContact: "+email+" Name: "+name);
        }else{
            if(email!=null){
                Log.d("Contact","PhoneContact: "+phoneNo+" Name: "+name +" Email: "+email);
            }else{
                Log.d("Contact","PhoneContact: "+phoneNo+" Name: "+name);
            }
        }
    }
}
