package com.biztracker.contacts_reader;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.biztracker.activities.MainActivity;
import com.biztracker.permissions.PermissionRequester;
import com.biztracker.utilities.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by shubham on 10/04/16.
 */
public class ContactsReader {

    private Pattern pattern;
    private Matcher matcher;
    private Context mContext;

    private static final String USERNAME_PATTERN = "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$";

    private List<Contact> allContacts;
    private Map<String,Contact> allContactsMap;

    public ContactsReader(MainActivity activity){
        mContext = activity.getApplicationContext();
        pattern = Pattern.compile(USERNAME_PATTERN);

        activity.requestPermission(PermissionRequester.Permissions.CONTACTS, new PermissionRequester.OnPermissionRequesterCompleted() {
            @Override
            public void onPermissionGranted(String permission) {
                allContacts = fetchAllContacts(mContext);
            }
            @Override
            public void onPermissionCancelled(String permission) {
            }
        });
    }


    public static void onRequestGranted(){
        Session.getInstance().mContactsReader.internalOnRequestGranted();
    }

    private void internalOnRequestGranted(){
        this.allContacts = fetchAllContacts(mContext);
    }

    public static List<Contact> lookupContactsForText(String text){
        if(text.length() == 0){
            return null;
        }

        text = text.toLowerCase();

        char firstChar = text.charAt(0);
        if((firstChar >= '0' && firstChar <= '9') || firstChar == '+'){
            return Session.getInstance().mContactsReader.internalLookUpContactsForNumber(text);
        }else if(firstChar >= 'a' && firstChar <='z'){
            return Session.getInstance().mContactsReader.internalLookUpContactsFoName(text);
        }else{
            return null;
        }
    }

    private List<Contact> internalLookUpContactsForNumber(String phoneNumber){

        if(allContacts == null){
            //No Contacts Exist
            return null;
        }

        if(phoneNumber.length() == 0){
            //You May Return All Contacts
            return null;
        }

        //E64 Format
        String e64PhoneNumber = "+91"+phoneNumber;  //For India

        List<Contact> filtered = new ArrayList<>();
        for(Contact contact : allContacts){
            if(contact.CONTACT_TYPE == Contact.CONTACT_TYPE_PHONE) {
                //Check if PhoneNo Starts With E64FormatNumber or Normal PhoneNumber
                if (contact.phoneNo.startsWith(e64PhoneNumber) || contact.phoneNo.startsWith(phoneNumber)) {
                    filtered.add(contact);
                    if (filtered.size() > 6) {
                        break;
                    }
                }
            }
        }

        return filtered;
    }



    private List<Contact> internalLookUpContactsFoName(String name){

        if(allContacts == null){
            //No Contacts Exist
            return null;
        }

        if(name.length() == 0){
            //You May Return All Contacts
            return null;
        }

        String[] projection_queries = new String[]{
                                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                                                    };

        Cursor users = mContext.getApplicationContext().getContentResolver().query(
                                    Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI,name),
                                    projection_queries,null,null,null);

        List<Contact> filtered = new ArrayList<>();

        while(users.moveToNext()){
            String contactid = users.getString(users.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            Contact contact = allContactsMap.get(contactid);
            if(contact!=null)   filtered.add(contact);
            if(filtered.size() > 6) break;
        }
        users.close();  //Close Query
        return filtered;
    }


    private List<Contact> fetchAllContacts(Context context){

        List<Contact> phoneticContacts = new ArrayList<>();
        HashMap<String,Contact> phoneContactMap = new HashMap<>();

        Cursor phones = context.getApplicationContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        while (phones.moveToNext())
        {
            String contactid = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String normalizedNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));



            Contact contact = phoneContactMap.get(contactid);
            if(contact == null) {
                contact = new Contact();
                contact.email = null;
                if (normalizedNumber == null || normalizedNumber.contentEquals("null")) {
                    if (phoneNumber == null || phoneNumber.contentEquals("null")) {
                        continue;
                    }
                    contact.phoneNo = phoneNumber;
                } else {
                    contact.phoneNo = normalizedNumber;
                }
                if(isValidName(name) == false) continue;

                contact.name = name.toLowerCase();              //Add LowerCase Name
                contact.androidContactID = contactid;
                contact.CONTACT_TYPE = Contact.CONTACT_TYPE_PHONE;

                phoneticContacts.add(contact);
                phoneContactMap.put(contact.androidContactID, contact);
            }else{
                //Contact Already Exist -> Removes Duplicacy
            }
        }
        phones.close();

        List<Contact> emailContacts = new ArrayList<>();

        Cursor emails = context.getApplicationContext().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,null,null,null);
        while(emails.moveToNext()){
            String contactid = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
            String email = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
            String name = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DISPLAY_NAME_PRIMARY));

            Contact contact = phoneContactMap.get(contactid);
            if(contact != null){
                contact.email = email;
            /*}else if(isValidName(name) == true){
                contact = new Contact();
                contact.email = email;
                contact.name = name.toLowerCase();              //Add LowerCase Name
                contact.androidContactID = contactid;
                contact.CONTACT_TYPE = Contact.CONTACT_TYPE_EMAIL_ONLY;
                emailContacts.add(contact);
                //Ignore Email Contacts
            */
            }
        }
        emails.close();

        List<Contact> allContacts = new ArrayList<>();
        allContacts.addAll(phoneticContacts);
        allContacts.addAll(emailContacts);

        allContactsMap = phoneContactMap;

        return allContacts;
    }

    public boolean isValidName(String name){
        if(name == null) return false;
        matcher = pattern.matcher(name);
        return matcher.matches();
    }

}
