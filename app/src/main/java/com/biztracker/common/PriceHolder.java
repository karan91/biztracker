package com.biztracker.common;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;


import java.util.Currency;
import java.util.Locale;

/**
 * Created by Karan on 08/11/15.
 */
public class PriceHolder extends QuantityHolder {


    public PriceHolder(Context context) {
        super(context);
        //addTextChangeListener();
    }

    public PriceHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
        //addTextChangeListener();
    }

    public PriceHolder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //addTextChangeListener();
    }

    public void addTextChangeListener() {
        this.addTextChangedListener(new TextWatcher() {
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void afterTextChanged(Editable arg0) {
               /* String str = PriceHolder.this.getText().toString();
                if (str.isEmpty()) return;
                String str2 = PerfectDecimal(str, 3, 2);

                if (!str2.equals(str)) {
                    PriceHolder.this.setText(str2);
                    int pos = PriceHolder.this.getText().length();
                    PriceHolder.this.setSelection(pos);
                }*/
            }
        });
    }

    public String PerfectDecimal(String str, int MAX_BEFORE_POINT, int MAX_DECIMAL){
        if(str.charAt(0) == '.') str = "0"+str;
        int max = str.length();

        String rFinal = "";
        boolean after = false;
        int i = 0, up = 0, decimal = 0; char t;
        while(i < max){
            t = str.charAt(i);
            if(t != '.' && after == false){
                up++;
                if(up > MAX_BEFORE_POINT) return rFinal;
            }else if(t == '.'){
                after = true;
            }else{
                decimal++;
                if(decimal > MAX_DECIMAL)
                    return rFinal;
            }
            rFinal = rFinal + t;
            i++;
        }return rFinal;
    }

}
