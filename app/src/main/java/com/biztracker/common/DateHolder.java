package com.biztracker.common;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Karan on 15/11/15.
 */
public class DateHolder extends TextView {

    Date date;
    public DateHolder(Context context) {
        super(context);
    }

    public DateHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DateHolder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setDate(Date date){
        this.date = date;
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String dateString = formatter.format(date);
        this.setText(dateString);
    }

    public Date getDate(){
        return date;
    }

}
