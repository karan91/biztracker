package com.biztracker.common;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.TextView;

import com.biztracker.CustomTextWatcher;
import com.biztracker.databaseobjects.DatabaseObject;

import java.util.ArrayList;

/**
 * Created by Karan on 02/10/15.
 */
public class DatabaseObjectAdapter extends ArrayAdapter{
    private ArrayList<DatabaseObject> items;
    private ArrayList<DatabaseObject> suggestions;
    private int viewResourceId;
    private EditText editText;



    public DatabaseObjectAdapter(Context context, int viewResourceId, ArrayList<DatabaseObject> items, EditText editText) {
        super(context, viewResourceId, items);
        this.items = items;
        this.suggestions = new ArrayList<>();
        this.suggestions.addAll(items);
        this.viewResourceId = viewResourceId;
        this.editText = editText;
        if(this.editText != null)   this.editText.addTextChangedListener(mTextWatcher);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        TextView customerNameLabel = (TextView) v.findViewById(android.R.id.text1);
        Object obj = suggestions.get(position);
        String name = ((DatabaseObject)obj).name;
        customerNameLabel.setText(name);
        return v;
    }

    @Override
    public int getCount() {
        return suggestions.size();
    }

    public void updateDataSourceList(ArrayList<DatabaseObject> list){
        this.suggestions.clear();
        this.suggestions.addAll(list);
        this.notifyDataSetChanged();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        TextView customerNameLabel = (TextView) v.findViewById(android.R.id.text1);
        Object obj = items.get(position);
        try {
            String name = ((DatabaseObject) obj).name;
            customerNameLabel.setText(name);
        }
        catch(Exception e){}
        return v;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return suggestions.get(position);
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((DatabaseObject)(resultValue)).name;
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                if(constraint.length() == 0)
                    suggestions.addAll(items);
                else {
                    for (DatabaseObject customer : items) {
                        if (customer.name.toLowerCase().contains(constraint.toString().toLowerCase())) {
                            suggestions.add(customer);
                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            notifyDataSetChanged();
        }
    };

    private CustomTextWatcher mTextWatcher = new CustomTextWatcher(new CustomTextWatcher.CustomTextWatcherCallback() {
        @Override
        public void onSearchForText(String text) {
            DatabaseObjectAdapter.this.getFilter().filter(text);
        }
    });

    public DatabaseObject getItemAtIndex(int index){
        DatabaseObject object = suggestions.get(index);
        return object;
    }
}
