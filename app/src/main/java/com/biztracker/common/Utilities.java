package com.biztracker.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.inputmethod.InputMethodManager;

import com.biztracker.databaseobjects.Company;

import java.io.File;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import static com.biztracker.databaseobjects.SlaveEntity.NATURE_ADDITIVE;
import static com.biztracker.databaseobjects.SlaveEntity.NATURE_SUBTRACTIVE;

/**
 * Created by karan on 15/04/16.
 */
public class Utilities {

    public static boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }
    public static Spannable getFormattedValue(BigDecimal value, int valueNature) {
        value = value.abs();
        NumberFormat format = NumberFormat.getInstance();
        String valueString = value.toString();
        if (valueString.contains(".") && !valueString.endsWith(".00")) {
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
        } else {
            format.setMaximumFractionDigits(0);
        }
        format.setCurrency(Company.company.currency);
        String formattedValue = format.format(value);
        formattedValue = format.getCurrency().getSymbol() + formattedValue;
        Spannable spannable = new SpannableString(formattedValue);

        int color = Color.parseColor("#FFFFFF");
        if (valueNature == NATURE_SUBTRACTIVE) {
            color = Color.parseColor("#E63C5A");
        } else if (valueNature == NATURE_ADDITIVE) {
            color = Color.parseColor("#6CC483");
        }
        spannable.setSpan(new ForegroundColorSpan(color), 0, formattedValue.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }
    public static void showKeyboard(Context context){
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public static void hideKeyboard(Activity activity){
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void setLocaleForFixingCrash(){
        try{
            Currency.getInstance(Locale.getDefault());
        }
        catch (Exception e){
            Locale.setDefault(Locale.US);
        }
    }
}
