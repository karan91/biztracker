package com.biztracker.common;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.views.RobotoMediumTextView;

/**
 * Created by Karan on 16/11/15.
 */
public class MasterAccountHolder extends RobotoMediumTextView {

    MasterAccount masterAccount;
    public MasterAccountHolder(Context context) {
        super(context);
    }

    public MasterAccountHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MasterAccountHolder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setMasterAccount(MasterAccount masterAccount, String initials){
        this.masterAccount = masterAccount;
        String masterAccountName;
        if(masterAccount == null)   masterAccountName = "Tap to select Account";
        else    masterAccountName = masterAccount.name;
        this.setText(masterAccountName);
    }
    public MasterAccount getMasterAccount(){
        return masterAccount;
    }
}
