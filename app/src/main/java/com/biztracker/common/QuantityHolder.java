package com.biztracker.common;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;

import com.biztracker.views.RobotoMediumEditText;

import java.math.BigDecimal;

/**
 * Created by Karan on 08/11/15.
 */
public class QuantityHolder extends RobotoMediumEditText {

    public QuantityHolder(Context context) {
        super(context);
        setProperties();
    }

    public QuantityHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
        setProperties();
    }

    public QuantityHolder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setProperties();
    }

    private void setProperties(){
        this.setMaxLines(1);
        this.setSelectAllOnFocus(true);
        this.setLines(1);
        this.setFocusable(true);
        this.setFocusableInTouchMode(true);
        this.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_NUMBER_FLAG_DECIMAL);
    }



    public BigDecimal getValue(){
        if(this.getText().toString().equals("")){
            this.setText("0");
        }
        return new BigDecimal(this.getText().toString());
    }

    public void setValue(BigDecimal value){
        if(value == null)
            this.setText("0");
        else {
            String plainString;
            plainString = value.toPlainString();
            this.setText(plainString);
        }
    }

}
