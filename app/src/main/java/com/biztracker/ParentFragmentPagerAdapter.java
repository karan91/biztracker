package com.biztracker;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by shubham on 09/07/15.
 */
public class ParentFragmentPagerAdapter extends FragmentPagerAdapter {
    private int fragmentCount = 5;
    public ParentFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Bundle b = new Bundle();
        SampleChildFragment f = new SampleChildFragment();
        b.putString("string", "Fragment position is : "+position);
        f.setArguments(b);
        return f;
    }

    @Override
    public int getCount() {
        return fragmentCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Position "+position;
    }
}
