package com.biztracker.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.MasterAccountType;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.SlaveEntity;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.databaseobjects.Unit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Karan on 11/07/15.
 */
public class DatabaseModel extends SQLiteOpenHelper {


    private static DatabaseModel sInstance;
    public static final String TABLE_TRANSACTIONAL_ENTITIES = "TRANSACTIONAL_ENTITIES";
    public static final String TABLE_UNITS = "UNITS";
    public static final String TABLE_MASTER_ACCOUNTS = "MASTER_ACCOUNTS";
    public static final String TABLE_SLAVE_ACCOUNTS = "SLAVE_ACCOUNTS";
    public static final String TABLE_MASTER_ACCOUNT_TYPES = "MASTER_ACCOUNT_TYPES";
    public static final String TABLE_SLAVE_ACCOUNT_TYPES = "SLAVE_ACCOUNT_TYPES";
    public static final String TABLE_MASTER_TRANSACTION_DETAILS = "MASTER_TRANSACTION_DETAILS";
    public static final String TABLE_SLAVE_TRANSACTION_DETAILS = "SLAVE_TRANSACTION_DETAILS";
    public static final String TABLE_COMPANY_DETAILS = "COMPANY_DETAILS";
    public static final String TABLE_TRANSACTION_LIST = "TRANSACTION_LIST";

    private static final int DATABASE_VERSION = 1;
    public static String companyName;

    public static synchronized SQLiteDatabase getReadableDatabase(Context context) {


        if (sInstance == null) {
            sInstance = new DatabaseModel(context.getApplicationContext(), companyName + ".bt");
        }
        return sInstance.getReadableDatabase();
    }

    public static synchronized SQLiteDatabase getWritableDatabase(Context context) {


        if (sInstance == null) {
            sInstance = new DatabaseModel(context.getApplicationContext(), companyName + ".bt");
        }
        return sInstance.getWritableDatabase();
    }

    /**
     * Constructor should be private to prevent direct instantiation.
     * make call to static method "getInstance()" instead.
     */
    private DatabaseModel(Context context, String companyName) {
        super(context, companyName, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {


        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_UNITS +" (UNITS_ID TEXT PRIMARY KEY, UNITS_NAME TEXT, UNITS_BILLED_NAME TEXT)");

        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_TRANSACTIONAL_ENTITIES +" (TRANSACTIONAL_ENTITIES_ID TEXT PRIMARY KEY, TRANSACTIONAL_ENTITIES_ITEM_NAME TEXT, " +
                                "TRANSACTIONAL_ENTITIES_PURCHASE_PRICE_PER_UNIT REAL, TRANSACTIONAL_ENTITIES_SALES_PRICE_PER_UNIT REAL, TRANSACTIONAL_ENTITIES_OPENING_QTY REAL, " +
                                "TRANSACTIONAL_ENTITIES_CLOSING_QTY REAL, TRANSACTIONAL_ENTITIES_BARCODE_ID TEXT, FOREIGN_UNITS_ID TEXT, " +
                                "FOREIGN KEY(FOREIGN_UNITS_ID) REFERENCES "+ TABLE_UNITS+"(UNITS_ID))");

        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_MASTER_ACCOUNT_TYPES +" (MASTER_ACCOUNT_TYPES_ID TEXT PRIMARY KEY, MASTER_ACCOUNT_TYPES_DEFAULT_NATURE TEXT" +
                                ", MASTER_ACCOUNT_TYPES_NAME TEXT, MASTER_ACCOUNT_TYPES_BILLABLE INT)");

        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_MASTER_ACCOUNTS +" (MASTER_ACCOUNTS_ID TEXT PRIMARY KEY, MASTER_ACCOUNTS_NAME TEXT, " +
                                "FOREIGN_MASTER_ACCOUNT_TYPES_ID TEXT, MASTER_ACCOUNTS_EMAIL TEXT, MASTER_ACCOUNTS_PHONE TEXT, MASTER_ACCOUNTS_ADDRESS TEXT, " +
                                "MASTER_ACCOUNTS_TAXATION_NUMBER TEXT, MASTER_ACCOUNTS_OPENING_BALANCE REAL, MASTER_ACCOUNTS_CLOSING_BALANCE REAL, " +
                                "FOREIGN KEY(FOREIGN_MASTER_ACCOUNT_TYPES_ID) REFERENCES "+TABLE_MASTER_ACCOUNT_TYPES+"(MASTER_ACCOUNT_TYPES_ID))");

        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_SLAVE_ACCOUNT_TYPES +" (SLAVE_ACCOUNT_TYPES_ID TEXT PRIMARY KEY, SLAVE_ACCOUNT_TYPES_NAME TEXT, " +
                                "SLAVE_ACCOUNT_TYPES_NATURE TEXT)");

        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_SLAVE_ACCOUNTS + " (SLAVE_ACCOUNTS_ID TEXT PRIMARY KEY, SLAVE_ACCOUNTS_NAME TEXT, " +
                                "FOREIGN_SLAVE_ACCOUNT_TYPES_ID TEXT, SLAVE_ACCOUNTS_DEFAULT_VALUE TEXT, SLAVE_ACCOUNTS_CALCULATED_AS INT, SLAVE_ACCOUNTS_NATURE INT, " +
                                "FOREIGN KEY(FOREIGN_SLAVE_ACCOUNT_TYPES_ID) REFERENCES " + TABLE_SLAVE_ACCOUNT_TYPES + "(SLAVE_ACCOUNT_TYPES_ID))");

        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_TRANSACTION_LIST +"(TRANSACTION_LIST_ID TEXT PRIMARY KEY, FOREIGN_TRANSACTION_TYPES_ID TEXT, " +
                                "TRANSACTION_LIST_DATE INT, FOREIGN_DEBIT_MASTER_ACCOUNTS_ID TEXT, FOREIGN_CREDIT_MASTER_ACCOUNTS_ID TEXT, TRANSACTION_LIST_NARRATION TEXT, " +
                                "TRANSACTION_LIST_AMOUNT REAL, TRANSACTION_LIST_TRANSACTION_NUMBER TEXT, FOREIGN KEY(FOREIGN_CREDIT_MASTER_ACCOUNTS_ID) REFERENCES "+
                                TABLE_MASTER_ACCOUNT_TYPES + "(MASTER_ACCOUNT_TYPES_ID), FOREIGN KEY(FOREIGN_DEBIT_MASTER_ACCOUNTS_ID) REFERENCES "+TABLE_MASTER_ACCOUNT_TYPES+
                                "(MASTER_ACCOUNT_TYPES_ID))");

        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_MASTER_TRANSACTION_DETAILS +" (FOREIGN_MASTER_TRANSACTION_LIST_ID TEXT, FOREIGN_TRANSACTIONAL_ENTITIES_ID TEXT, " +
                                "MASTER_TRANSACTION_DETAILS_QUANTITY REAL, MASTER_TRANSACTION_DETAILS_PRICE REAL, FOREIGN KEY(FOREIGN_MASTER_TRANSACTION_LIST_ID) REFERENCES "
                                +TABLE_TRANSACTION_LIST+"(TRANSACTION_LIST_ID), FOREIGN KEY(FOREIGN_TRANSACTIONAL_ENTITIES_ID) REFERENCES "+TABLE_TRANSACTIONAL_ENTITIES+
                                "(TRANSACTIONAL_ENTITIES_ID))");

        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_SLAVE_TRANSACTION_DETAILS +" (FOREIGN_SLAVE_ACCOUNTS_ID TEXT, FOREIGN_TRANSACTION_LIST_ID TEXT, SLAVE_TRANSACTION_DETAILS_VALUE REAL," +
                "SLAVE_TRANSACTION_DETAILS_PRODUCT_IDS STRING)");
        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_COMPANY_DETAILS +"(COMPANY_DETAILS_ID INTEGER PRIMARY KEY AUTOINCREMENT, COMPANY_DETAILS_NAME TEXT, COMPANY_DETAILS_STARTING_FROM TEXT, COMPANY_DETAILS_ADDRESS TEXT, " +
                "COMPANY_DETAILS_BUSINESS_NAME TEXT, COMPANY_DETAILS_PHONE TEXT, COMPANY_DETAILS_EMAIL TEXT, COMPANY_DETAILS_TAXATION_NUMBER TEXT, COMPANY_DETAILS_CURRENCY TEXT)");
        this.loadSampleInvoice(sqLiteDatabase);
    }

    private void loadSampleInvoice(SQLiteDatabase sqLiteDatabase){

        this.loadMasterAccountTypes(sqLiteDatabase);
        this.loadMasterAccounts(sqLiteDatabase);
        this.insertSampleUnits(sqLiteDatabase);
        //this.insertSampleProducts(sqLiteDatabase);
        //this.insertSampleSlaveEntities(sqLiteDatabase);
        //this.insertSampleSaleTransaction(sqLiteDatabase);
    }

    private void insertSampleSlaveEntities(SQLiteDatabase sqLiteDatabase) {
        SlaveEntity slaveEntity = new SlaveEntity();
        slaveEntity.name = "Cash Discount";
        slaveEntity.id = "1";
        slaveEntity.nature = SlaveEntity.NATURE_SUBTRACTIVE;
        slaveEntity.calculatedAs = SlaveEntity.CALCULATED_AS_PERCENT;
        slaveEntity.default_value = new BigDecimal(20.0);
        sqLiteDatabase.insert(TABLE_SLAVE_ACCOUNTS, null, slaveEntity.prepareContentValues());

        SlaveEntity slaveEntity2 = new SlaveEntity();
        slaveEntity2.name = "VAT";
        slaveEntity2.id = "2";
        slaveEntity2.nature = SlaveEntity.NATURE_ADDITIVE;
        slaveEntity2.calculatedAs = SlaveEntity.CALCULATED_AS_PERCENT;
        slaveEntity2.default_value = new BigDecimal(12.5);
        sqLiteDatabase.insert(TABLE_SLAVE_ACCOUNTS, null, slaveEntity2.prepareContentValues());

        SlaveEntity slaveEntity3 = new SlaveEntity();
        slaveEntity3.name = "Transportation";
        slaveEntity3.id = "3";
        slaveEntity3.nature = SlaveEntity.NATURE_ADDITIVE;
        slaveEntity3.calculatedAs = SlaveEntity.CALCULATED_AS_ABSOLUTE;
        slaveEntity3.default_value = new BigDecimal(20.0);
        sqLiteDatabase.insert(TABLE_SLAVE_ACCOUNTS, null, slaveEntity3.prepareContentValues());
    }

    private void loadMasterAccountTypes(SQLiteDatabase sqLiteDatabase) {
        MasterAccountType type = new MasterAccountType();
        type.name = "Customer";
        type.id = "1";
        type.defaultNature = "dr";
        type.billable = 1;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Supplier";
        type.id = "2";
        type.defaultNature = "cr";
        type.billable = 1;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Bank";
        type.id = "3";
        type.defaultNature = "dr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Cash";
        type.id = "4";
        type.defaultNature = "dr";
        type.billable = 1;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Purchases";
        type.id = "5";
        type.defaultNature = "dr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Purchases Returned";
        type.id = "6";
        type.defaultNature = "cr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Sales";
        type.id = "7";
        type.defaultNature = "cr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Sales Returned";
        type.id = "8";
        type.defaultNature = "dr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Expenses";
        type.id = "9";
        type.defaultNature = "dr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Income";
        type.id = "10";
        type.defaultNature = "cr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Loans Taken";
        type.id = "11";
        type.defaultNature = "cr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Loans/Advance Given";
        type.id = "12";
        type.defaultNature = "dr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());

        type = new MasterAccountType();
        type.name = "Investments";
        type.id = "13";
        type.defaultNature = "dr";
        type.billable = 0;
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNT_TYPES, null, type.prepareContentValues());
    }

    private void loadMasterAccounts(SQLiteDatabase sqLiteDatabase){

        MasterAccount account;
//        account.name = "Customer";
//        account.id = "1";
        MasterAccountType type;
//        type.id = "1";
//        account.accountType = type;
//        account.email = "";
//        account.taxationNumber = "";
//        account.openingBalance = new BigDecimal(0);
//        account.closingBalance = new BigDecimal(0);
//        account.phone = "";
//        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNTS, null, account.prepareContentValues());
//
//        account = new MasterAccount();
//        account.name = "Supplier";
//        account.id = "2";
//        type = new MasterAccountType();
//        type.id = "2";
//        account.accountType = type;
//        account.email = "";
//        account.taxationNumber = "";
//        account.openingBalance = new BigDecimal(0);
//        account.closingBalance = new BigDecimal(0);
//        account.phone = "";
//        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNTS, null, account.prepareContentValues());

        account = new MasterAccount();
        account.name = "Sales";
        account.id = "3";
        type = new MasterAccountType();
        type.id = "7";
        account.accountType = type;
        account.email = "";
        account.taxationNumber = "";
        account.openingBalance = new BigDecimal(0);
        account.closingBalance = new BigDecimal(0);
        account.phone = "";
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNTS, null, account.prepareContentValues());

//        account = new MasterAccount();
//        account.name = "Sales Returned Account";
//        account.id = "4";
//        type = new MasterAccountType();
//        type.id = "8";
//        account.accountType = type;
//        account.email = "";
//        account.taxationNumber = "";
//        account.openingBalance = new BigDecimal(0);
//        account.closingBalance = new BigDecimal(0);
//        account.phone = "";
//        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNTS, null, account.prepareContentValues());

        account = new MasterAccount();
        account.name = "Purchase";
        account.id = "5";
        type = new MasterAccountType();
        type.id = "5";
        account.accountType = type;
        account.email = "";
        account.taxationNumber = "";
        account.openingBalance = new BigDecimal(0);
        account.closingBalance = new BigDecimal(0);
        account.phone = "";
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNTS, null, account.prepareContentValues());

//        account = new MasterAccount();
//        account.name = "Purchase Returned Account";
//        account.id = "6";
//        type = new MasterAccountType();
//        type.id = "6";
//        account.accountType = type;
//        account.email = "";
//        account.taxationNumber = "";
//        account.openingBalance = new BigDecimal(205);
//        account.closingBalance = new BigDecimal(205);
//        account.phone = "";
//        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNTS, null, account.prepareContentValues());

//        /*account = new MasterAccount();
//        account.name = "Expense Account";
//        account.id = "7";
//        type = new MasterAccountType();
//        type.id = "9";
//        account.accountType = type;
//        account.email = "";
//        account.taxationNumber = "";
//        account.openingBalance = new BigDecimal(205);
//        account.closingBalance = new BigDecimal(205);
//        account.phone = "";
//        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNTS, null, account.prepareContentValues());*/

//        account = new MasterAccount();
//        account.name = "Income Account";
//        account.id = "8";
//        type = new MasterAccountType();
//        type.id = "10";
//        account.accountType = type;
//        account.email = "";
//        account.taxationNumber = "";
//        account.openingBalance = new BigDecimal(205);
//        account.closingBalance = new BigDecimal(205);
//        account.phone = "";
//        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNTS, null, account.prepareContentValues());

        account = new MasterAccount();
        account.name = "Cash";
        account.id = "9";
        type = new MasterAccountType();
        type.id = "4";
        account.accountType = type;
        account.email = "";
        account.taxationNumber = "";
        account.openingBalance = new BigDecimal(0);
        account.closingBalance = new BigDecimal(0);
        account.phone = "";
        sqLiteDatabase.insert(TABLE_MASTER_ACCOUNTS, null, account.prepareContentValues());

    }

    private void insertSampleUnits(SQLiteDatabase sqLiteDatabase){

        Unit unit = new Unit();
        unit.displayName = "Pcs";
        unit.name = "Pcs";
        unit.id = "1";
        sqLiteDatabase.insert(TABLE_UNITS, null, unit.prepareContentValues());

        unit = new Unit();
        unit.displayName = "Kgs";
        unit.name = "Kgs";
        unit.id = "2";
        sqLiteDatabase.insert(TABLE_UNITS, null, unit.prepareContentValues());
    }

    private void insertSampleProducts(SQLiteDatabase sqLiteDatabase){

        Product p = new Product();
        p.name = "Product 1";
        p.id = "1";
        p.openingUnits = new BigDecimal(100);
        p.closingUnits = new BigDecimal(100);
        Unit u = new Unit();
        u.id = "1";
        p.unit = u;
        p.purchasePrice = new BigDecimal(200);
        p.sellingPrice = new BigDecimal(400);
        sqLiteDatabase.insert(TABLE_TRANSACTIONAL_ENTITIES, null, p.prepareContentValues());

        p = new Product();
        p.name = "Product 2";
        p.openingUnits = new BigDecimal(200);
        p.closingUnits = new BigDecimal(200);
        u = new Unit();
        u.id = "2";
        p.id = "2";
        p.unit = u;
        p.purchasePrice = new BigDecimal(300);
        p.sellingPrice = new BigDecimal(430);
        sqLiteDatabase.insert(TABLE_TRANSACTIONAL_ENTITIES, null, p.prepareContentValues());
    }

    private void insertSampleSaleTransaction(SQLiteDatabase sqLiteDatabase){
        InvoicedTransaction transaction = new InvoicedTransaction();
        transaction.id = transaction.generateUniqueId();
        transaction.setType(Transaction.TYPE_PURCHASE);
        MasterAccount account = new MasterAccount();
        account.id = "5";
        transaction.debitAccount = account;
        account = new MasterAccount();
        account.id = "2";
        transaction.creditAccount = account;
        transaction.transactionNumber = "1234";
        Date date = new Date();
        transaction.date = date;
        transaction.note = "This is my first invoice";

        Product p = new Product();
        p.id = "1";
        p.entityQty = new BigDecimal(10);
        p.entityValue = new BigDecimal(200);
        Product p2 = new Product();
        p2.id = "2";
        p2.entityValue = new BigDecimal(390);
        p2.entityQty = new BigDecimal(10);
        transaction.productList = new ArrayList<>();
        transaction.productList.add(p);
        transaction.productList.add(p2);

        SlaveEntity slaveEntity1 = new SlaveEntity();
        slaveEntity1.id = "1";
        slaveEntity1.default_value = new BigDecimal(10.0);
        slaveEntity1.calculatedAs = SlaveEntity.CALCULATED_AS_PERCENT;
        slaveEntity1.productList = transaction.productList;

        SlaveEntity slaveEntity2 = new SlaveEntity();
        slaveEntity2.calculatedAs = SlaveEntity.CALCULATED_AS_PERCENT;
        slaveEntity2.id = "2";
        slaveEntity2.default_value = new BigDecimal(10.0);
        slaveEntity2.productList = transaction.productList;

        SlaveEntity slaveEntity3 = new SlaveEntity();
        slaveEntity3.id = "3";
        slaveEntity3.calculatedAs = SlaveEntity.CALCULATED_AS_ABSOLUTE;
        slaveEntity3.default_value = new BigDecimal(200);

        transaction.slaveEntities = new ArrayList<>();
        transaction.slaveEntities.add(slaveEntity1);
        transaction.slaveEntities.add(slaveEntity2);
        transaction.slaveEntities.add(slaveEntity3);

        sqLiteDatabase.insert(TABLE_TRANSACTION_LIST, null, transaction.prepareContentValues());
        List<ContentValues> list = transaction.prepareProductDetailsContentValues();
        for(ContentValues cv : list){
            sqLiteDatabase.insert(TABLE_MASTER_TRANSACTION_DETAILS, null, cv);
        }

        String arithmeticOperator;
        if(transaction.getType() == Transaction.TYPE_SALES){
            arithmeticOperator = "-";
        }
        else{
            arithmeticOperator = "+";
        }
        for(Product product : transaction.productList){
            sqLiteDatabase.execSQL("UPDATE TRANSACTIONAL_ENTITIES SET TRANSACTIONAL_ENTITIES_CLOSING_QTY = TRANSACTIONAL_ENTITIES_CLOSING_QTY " + arithmeticOperator + product.entityQty.toString() + " WHERE TRANSACTIONAL_ENTITIES_ID = '"
                    + product.id + "'");
        }
        list = transaction.prepareSlaveEntitiesContentValues();
        for(ContentValues cv : list){
            sqLiteDatabase.insert(TABLE_SLAVE_TRANSACTION_DETAILS, null, cv);
        }
        BigDecimal totalInvoiceValue = new BigDecimal("6041");
        if(transaction.getType() == Transaction.TYPE_SALES) {
            sqLiteDatabase.execSQL("UPDATE MASTER_ACCOUNTS SET MASTER_ACCOUNTS_CLOSING_BALANCE = MASTER_ACCOUNTS_CLOSING_BALANCE + " + totalInvoiceValue.toString() + " WHERE MASTER_ACCOUNTS_ID = '"
                    + transaction.debitAccount.id + "'");
            sqLiteDatabase.execSQL("UPDATE MASTER_ACCOUNTS SET MASTER_ACCOUNTS_CLOSING_BALANCE = MASTER_ACCOUNTS_CLOSING_BALANCE - " + totalInvoiceValue.toString() + " WHERE MASTER_ACCOUNTS_ID = '"
                    + transaction.creditAccount.id + "'");
        }
        else if(transaction.getType() == Transaction.TYPE_PURCHASE){
            sqLiteDatabase.execSQL("UPDATE MASTER_ACCOUNTS SET MASTER_ACCOUNTS_CLOSING_BALANCE = MASTER_ACCOUNTS_CLOSING_BALANCE + " + totalInvoiceValue.toString() + " WHERE MASTER_ACCOUNTS_ID = '"
                    + transaction.creditAccount.id + "'");
            sqLiteDatabase.execSQL("UPDATE MASTER_ACCOUNTS SET MASTER_ACCOUNTS_CLOSING_BALANCE = MASTER_ACCOUNTS_CLOSING_BALANCE + " + totalInvoiceValue.toString() + " WHERE MASTER_ACCOUNTS_ID = '"
                    + transaction.debitAccount.id + "'");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
