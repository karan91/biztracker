package com.biztracker.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.biztracker.BizTrackerApp;
import com.biztracker.databaseobjects.Company;
import com.biztracker.databaseobjects.DatabaseObject;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.MasterAccount;
import com.biztracker.databaseobjects.MasterAccountType;
import com.biztracker.databaseobjects.PaymentReceiptTransaction;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.SlaveEntity;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.databaseobjects.Unit;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.InvoiceOnlyAccountListFrag;
import com.biztracker.fragments.history.ProductHistory.ProductHistoryBean;
import com.google.android.gms.vision.barcode.Barcode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * Created by Karan on 09/09/15.
 */
public class DatabaseTalker {

    private static DatabaseTalker instance;

    public void insertOrUpdateProduct(final Product product, queryCompleteCallBack callBack, int processId, Executor executor) {
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public DatabaseObject asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                /*if (product.type.id == -1) {
                    product.type.insertObject(null, 0, null);
                }*/
                if(product.unit.id.equals("-1")){
                    product.unit.insertObject(null, 0, null);
                }
                ContentValues cv = product.prepareContentValues();
                Cursor c = database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + " WHERE TRANSACTIONAL_ENTITIES_" +
                        "ID = ?", new String[]{product.id});
                if(c.moveToFirst()){
                    database.update(DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES, cv, "TRANSACTIONAL_ENTITIES_ID = ?", new String[]{product.id});
                    product.id = c.getString(c.getColumnIndex(DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + "_ID"));
                    return product;
                }
                else
                {
                    if (database.insert(DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES, null, cv) == -1){
                        return null;
                    }
                    product.id = cv.getAsString("TRANSACTIONAL_ENTITIES_ID");
                    return product;
                }
            }
        };
        task.execute();
    }

    public void getNewInvoicedTransaction(final int type, queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                return InvoicedTransaction.getNewInvoicedTransaction(type);
            }
        };
        task.execute();
    }

    public void loadCompany(String companyName, Company company){
        DatabaseModel.companyName = "company";
        SQLiteDatabase database = DatabaseModel.getWritableDatabase(BizTrackerApp.getAppContext());
        Cursor c = database.rawQuery("SELECT * FROM COMPANY_DETAILS", null);

        if(c.moveToFirst()){
            company.loadFromCursor(c);
        }
    }

    public void createNewCompany(final Company company, queryCompleteCallBack callBack, int processId, Executor executor){

        TaskManager manager = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                DatabaseModel.companyName = "company";
                SQLiteDatabase database = DatabaseModel.getWritableDatabase(BizTrackerApp.getAppContext());
                long rowId = database.insert(DatabaseModel.TABLE_COMPANY_DETAILS, null, company.prepareContentValues());
                if(rowId != -1){
                    return company;
                }
                else{
                    return null;
                }
            }
        };
        manager.execute();
    }

    public void updateCompany(final Company company, queryCompleteCallBack callBack, int processId, Executor executor){

        TaskManager manager = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                DatabaseModel.companyName = "company";
                SQLiteDatabase database = DatabaseModel.getWritableDatabase(BizTrackerApp.getAppContext());
                long rowId = database.update(DatabaseModel.TABLE_COMPANY_DETAILS, company.prepareContentValues(), "COMPANY_DETAILS_ID=" + company.id, null);
                if(rowId != -1){
                    return company;
                }
                else{
                    return null;
                }
            }
        };
        manager.execute();
    }

    public void insertOrUpdateUnit(final Unit unit, queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public DatabaseObject asyncOperation() {
                ContentValues cv = unit.prepareContentValues();
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_UNITS + " WHERE UNITS_NAME = ?", new String[]{unit.name});
                if(c.moveToFirst()){
                    long result = database.update(DatabaseModel.TABLE_UNITS, cv, "UNITS_NAME = ?", new String[]{unit.name});
                    if(result == 0) return null;
                    else{
                        unit.id = c.getString(c.getColumnIndex("UNITS_ID"));
                        return unit;
                    }
                }
                else{
                    long result = database.insert(DatabaseModel.TABLE_UNITS, null, cv);
                    if(result == -1)  return null;
                    else{
                        unit.id = cv.getAsString("UNITS_ID");
                        return unit;
                    }
                }
            }
        };
        task.execute();
    }

    public void getProductsForSearchQuery(queryCompleteCallBack callBack, int processId, Executor executor, final String searchQuery){
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {

                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());

                Cursor c =database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + " JOIN " +
                        DatabaseModel.TABLE_UNITS + " ON " +DatabaseModel.TABLE_UNITS + ".UNITS_ID = " +
                        DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + ".FOREIGN_UNITS_ID WHERE TRANSACTIONAL_ENTITIES_ITEM_NAME LIKE '%"+
                        searchQuery+"%'", null);
                List<Product> types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(Product.getForInventoryMode(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void getSlaveEntitiesForSearchQuery(queryCompleteCallBack callback, int processId, Executor executor, final String query) {
        TaskManager task = new TaskManager(callback, processId, executor) {
            @Override
            public Object asyncOperation() {

                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());

                Cursor c = database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_SLAVE_ACCOUNTS + " WHERE SLAVE_ACCOUNTS_NAME LIKE '%"+query+"%'", null);
                List<SlaveEntity> types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(SlaveEntity.getForInventoryMode(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void insertInvoiceTransaction(final InvoicedTransaction transaction, queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                return insertInvoiceTransaction(transaction, false);
            }
        };
        task.execute();
    }

    public void updateInvoiceTransaction(final InvoicedTransaction transaction, queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                return insertInvoiceTransaction(transaction, true);
            }
        };
        task.execute();
    }

    private InvoicedTransaction insertInvoiceTransaction(InvoicedTransaction transaction, boolean update){
        ContentValues cv;
        cv = transaction.prepareContentValues();
        SQLiteDatabase database = DatabaseModel.getWritableDatabase(BizTrackerApp.getAppContext());
        if(update){
            deleteInvoiceFromDatabase(transaction, database);
        }
        database.insert(DatabaseModel.TABLE_TRANSACTION_LIST, null, cv);
        insertTransactionalEntitiesInInvoiceTransaction(transaction, database);
        insertSlaveEntitiesForTransaction(transaction, database);
        updateAccountBalancesForTransaction(transaction, database, false);
        updateSalesPricesForProducts(transaction.productList);
        return transaction;
    }

    private void updateAccountBalancesForTransaction(Transaction transaction, SQLiteDatabase database, boolean rollback){

        BigDecimal value;
        if(rollback){
            value = transaction.originalValue;
        }
        else{
            value = transaction.getNetValue();
        }
        String arithmeticSign;
        if(transaction.debitAccount.accountType.defaultNature.equals(MasterAccountType.NATURE_DEBIT)){
            arithmeticSign = !rollback?"+":"-";
        }
        else{
            arithmeticSign = !rollback?"-":"+";
        }
        database.execSQL("UPDATE MASTER_ACCOUNTS SET MASTER_ACCOUNTS_CLOSING_BALANCE = MASTER_ACCOUNTS_CLOSING_BALANCE " + arithmeticSign + value.toString() + " WHERE MASTER_ACCOUNTS_ID = '"
                + transaction.debitAccount.id + "';");

        if(transaction.creditAccount.accountType.defaultNature.equals(MasterAccountType.NATURE_CREDIT)){
            arithmeticSign = !rollback?"+":"-";
        }
        else{
            arithmeticSign = !rollback?"-":"+";
        }

        database.execSQL("UPDATE MASTER_ACCOUNTS SET MASTER_ACCOUNTS_CLOSING_BALANCE = MASTER_ACCOUNTS_CLOSING_BALANCE " + arithmeticSign + " " + value.toString() + " WHERE MASTER_ACCOUNTS_ID = '"
                + transaction.creditAccount.id + "';");
    }

    private void insertSlaveEntitiesForTransaction(InvoicedTransaction transaction, SQLiteDatabase database){
        for(SlaveEntity slaveEntity : transaction.slaveEntities){
            ContentValues cv = slaveEntity.prepareContentValuesForTransaction(transaction.id);
            long result = database.insert(DatabaseModel.TABLE_SLAVE_TRANSACTION_DETAILS, null, cv);
        }
    }

    public void getInvoicesForSearchQuery(queryCompleteCallBack callBack, int processId, Executor executor, final String query){
        TaskManager manager = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                List<String> invoiceIds = new ArrayList<>();
                Cursor c = database.rawQuery("SELECT TRANSACTION_LIST_ID FROM TRANSACTION_LIST JOIN MASTER_ACCOUNTS ON " +
                        "(MASTER_ACCOUNTS.MASTER_ACCOUNTS_ID = TRANSACTION_LIST.FOREIGN_CREDIT_MASTER_ACCOUNTS_ID) WHERE FOREIGN_TRANSACTION_TYPES_ID != 1 AND MASTER_ACCOUNTS_NAME LIKE '%" + query + "%'", null);
                while (c.moveToNext()){
                    String invoiceId = c.getString(c.getColumnIndex("TRANSACTION_LIST_ID"));
                    invoiceIds.add(invoiceId);
                }
                c = database.rawQuery("SELECT TRANSACTION_LIST_ID FROM TRANSACTION_LIST JOIN MASTER_ACCOUNTS ON " +
                        "(MASTER_ACCOUNTS.MASTER_ACCOUNTS_ID = TRANSACTION_LIST.FOREIGN_DEBIT_MASTER_ACCOUNTS_ID) WHERE FOREIGN_TRANSACTION_TYPES_ID != 2 AND MASTER_ACCOUNTS_NAME LIKE '%" + query + "%'", null);
                while (c.moveToNext()){
                    String invoiceId = c.getString(c.getColumnIndex("TRANSACTION_LIST_ID"));
                    if(!invoiceIds.contains(invoiceId)){
                        invoiceIds.add(invoiceId);
                    }
                }
                c = database.rawQuery("SELECT DISTINCT FOREIGN_MASTER_TRANSACTION_LIST_ID FROM MASTER_TRANSACTION_DETAILS JOIN " +
                        "TRANSACTIONAL_ENTITIES ON TRANSACTIONAL_ENTITIES.TRANSACTIONAL_ENTITIES_ID = MASTER_TRANSACTION_DETAILS.FOREIGN_TRANSACTIONAL_ENTITIES_ID " +
                        "WHERE TRANSACTIONAL_ENTITIES.TRANSACTIONAL_ENTITIES_ITEM_NAME LIKE '%"+query+"%'", null);
                while (c.moveToNext()){
                    String invoiceId = c.getString(c.getColumnIndex("FOREIGN_MASTER_TRANSACTION_LIST_ID"));
                    if(!invoiceIds.contains(invoiceId)){
                        invoiceIds.add(invoiceId);
                    }
                }
                List<InvoicedTransaction> invoices = new ArrayList<>();
                for(String invoiceId : invoiceIds){
                    InvoicedTransaction transaction = getInvoicedTransactionFromId(invoiceId);
                    invoices.add(transaction);
                }
                return invoices;
            }
        };
        manager.execute();
    }

    private void insertTransactionalEntitiesInInvoiceTransaction(InvoicedTransaction transaction, SQLiteDatabase database){
        for(Product product : transaction.productList){
            ContentValues cv = product.prepareContentValuesForTransaction(transaction.id);
            double result = database.insert(DatabaseModel.TABLE_MASTER_TRANSACTION_DETAILS, null, cv);
        }
        updateClosingQuantities(transaction, database, false);
    }

    private void deleteInvoiceFromDatabase(InvoicedTransaction transaction, SQLiteDatabase database){
        int result = database.delete(DatabaseModel.TABLE_TRANSACTION_LIST, "TRANSACTION_LIST_ID = '" + transaction.id + "'", null);
        if(result != 0){
            //delete products and update their closing quatity balances
            updateClosingQuantities(transaction, database, true);
            result = database.delete(DatabaseModel.TABLE_MASTER_TRANSACTION_DETAILS, "FOREIGN_MASTER_TRANSACTION_LIST_ID = '" + transaction.id + "'", null);
            //delete all slave entities for transaction
            result = database.delete(DatabaseModel.TABLE_SLAVE_TRANSACTION_DETAILS, "FOREIGN_TRANSACTION_LIST_ID = '" + transaction.id + "'", null);
            updateAccountBalancesForTransaction(transaction, database, true);
        }
    }

    public void deleteInvoiceTransaction(final InvoicedTransaction transaction, queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager manager = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getWritableDatabase(BizTrackerApp.getAppContext());
                deleteInvoiceFromDatabase(transaction, database);
                return null;
            }
        };
        manager.execute();
    }

    public void refreshMasterAccountObject(final MasterAccount masterAccount, queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager manager = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                MasterAccount masterAccount1 = getMasterAccountForId(masterAccount.id);
                masterAccount.closingBalance = masterAccount1.closingBalance;
                return masterAccount;
            }
        };
        manager.execute();
    }

    private void updateClosingQuantities(InvoicedTransaction transaction, SQLiteDatabase database, boolean rollback){

        String arithmeticOperator = null;
        if(transaction.getType() == Transaction.TYPE_SALES){
            arithmeticOperator = rollback?"+":"-";
        }
        else if(transaction.getType() == Transaction.TYPE_PURCHASE){
            arithmeticOperator = rollback?"-":"+";
        }
        if(rollback) {
            Cursor c = database.rawQuery("SELECT MASTER_TRANSACTION_DETAILS_QUANTITY, FOREIGN_TRANSACTIONAL_ENTITIES_ID FROM MASTER_TRANSACTION_DETAILS WHERE FOREIGN_MASTER_TRANSACTION_LIST_ID = '" + transaction.id + "'", null);
            while (c.moveToNext()) {
                double quantity = c.getDouble(c.getColumnIndex("MASTER_TRANSACTION_DETAILS_QUANTITY"));
                String productId = c.getString(c.getColumnIndex("FOREIGN_TRANSACTIONAL_ENTITIES_ID"));

                database.execSQL("UPDATE TRANSACTIONAL_ENTITIES SET TRANSACTIONAL_ENTITIES_CLOSING_QTY = TRANSACTIONAL_ENTITIES_CLOSING_QTY " + arithmeticOperator + " " + quantity +
                        " WHERE TRANSACTIONAL_ENTITIES_ID = '" + productId + "';");
            }
        }
        else {
            for(Product p : transaction.productList){
                database.execSQL("UPDATE TRANSACTIONAL_ENTITIES SET TRANSACTIONAL_ENTITIES_CLOSING_QTY = TRANSACTIONAL_ENTITIES_CLOSING_QTY " + arithmeticOperator + " " + p.entityQty.toString() +
                        " WHERE TRANSACTIONAL_ENTITIES_ID = '" + p.id + "';");
            }
        }
    }




    public void updateSalesPricesForProducts(List<Product> productList){
        SQLiteDatabase database = DatabaseModel.getWritableDatabase(BizTrackerApp.getAppContext());
        for(Product p : productList){
            p.sellingPrice = p.entityValue;
            ContentValues cv = new ContentValues();
            cv.put("TRANSACTIONAL_ENTITIES_SALES_PRICE_PER_UNIT", p.sellingPrice.toPlainString());
            database.update(DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES, cv, "TRANSACTIONAL_ENTITIES_ID = ?", new String[]{String.valueOf(p.id)});
        }
    }

    public void getInvoiceTransaction(queryCompleteCallBack callBack, int processId, Executor executor, final String invoiceId){

        TaskManager manager = new TaskManager(callBack, processId, executor){

            @Override
            public InvoicedTransaction asyncOperation() {
                return getInvoicedTransactionFromId(invoiceId);
            }
        };
        manager.execute();
    }

    public void getPaymentReceiptTransactionUsingCallback(queryCompleteCallBack callBack, int processId, Executor executor, final String transactionId){

        TaskManager manager = new TaskManager(callBack, processId, executor){

            @Override
            public PaymentReceiptTransaction asyncOperation() {
                return getPaymentReceiptTransactionFromId(transactionId);
            }
        };
        manager.execute();
    }

    public PaymentReceiptTransaction getPaymentReceiptTransactionFromId(String transactionId){
        SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
        Cursor c = database.rawQuery("SELECT * FROM "+DatabaseModel.TABLE_TRANSACTION_LIST+" WHERE TRANSACTION_LIST_ID = '" + transactionId + "'", null);
        if(!c.moveToNext()){
            return null;
        }
        PaymentReceiptTransaction transaction = new PaymentReceiptTransaction(c);
        String debitAccountId = c.getString(c.getColumnIndex("FOREIGN_DEBIT_MASTER_ACCOUNTS_ID"));
        String creditAccountId = c.getString(c.getColumnIndex("FOREIGN_CREDIT_MASTER_ACCOUNTS_ID"));
        transaction.debitAccount = getMasterAccountFromId(debitAccountId, database);
        transaction.creditAccount = getMasterAccountFromId(creditAccountId, database);
        return transaction;
    }

    public InvoicedTransaction getInvoicedTransactionFromId(String invoiceId){
        SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
        Cursor c = database.rawQuery("SELECT * FROM "+DatabaseModel.TABLE_TRANSACTION_LIST+" WHERE TRANSACTION_LIST_ID = '" + invoiceId + "'", null);
        if(!c.moveToNext()){
            return null;
        }
        InvoicedTransaction transaction = new InvoicedTransaction(c);
        String debitAccountId = c.getString(c.getColumnIndex("FOREIGN_DEBIT_MASTER_ACCOUNTS_ID"));
        String creditAccountId = c.getString(c.getColumnIndex("FOREIGN_CREDIT_MASTER_ACCOUNTS_ID"));
        transaction.debitAccount = getMasterAccountFromId(debitAccountId, database);
        transaction.creditAccount = getMasterAccountFromId(creditAccountId, database);
        transaction.productList = getProductListForTransactionId(transaction.id, database);
        transaction.slaveEntities = getSlaveEntitiesForTransaction(transaction.id, database, transaction.productList);
        transaction.originalValue = transaction.getNetValue();
        return transaction;
    }

    private ArrayList<Product> getProductListForTransactionId(String id, SQLiteDatabase database){
        Cursor c = database.rawQuery("SELECT * FROM "+DatabaseModel.TABLE_MASTER_TRANSACTION_DETAILS+" JOIN "+
                DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES+" ON " +DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES+
                ".TRANSACTIONAL_ENTITIES_ID = "+DatabaseModel.TABLE_MASTER_TRANSACTION_DETAILS+".FOREIGN_TRANSACTIONAL_ENTITIES_ID JOIN " +
                DatabaseModel.TABLE_UNITS+" ON UNITS.UNITS_ID = "+DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES+
                ".FOREIGN_UNITS_ID WHERE FOREIGN_MASTER_TRANSACTION_LIST_ID = '" + id + "'", null);
        ArrayList<Product> list = new ArrayList();
        while (c.moveToNext()){
            Product p = Product.getForInvoiceTransactionMode(c);
            list.add(p);
        }
        return list;
    }

    private ArrayList<SlaveEntity> getSlaveEntitiesForTransaction(String id, SQLiteDatabase database, List<Product> productList){
        ArrayList<SlaveEntity> result = new ArrayList<>();
        Cursor c = database.rawQuery("SELECT * FROM "+DatabaseModel.TABLE_SLAVE_TRANSACTION_DETAILS + " JOIN " + DatabaseModel.TABLE_SLAVE_ACCOUNTS + " ON "
        + DatabaseModel.TABLE_SLAVE_ACCOUNTS + ".SLAVE_ACCOUNTS_ID = " + DatabaseModel.TABLE_SLAVE_TRANSACTION_DETAILS + ".FOREIGN_SLAVE_ACCOUNTS_ID WHERE FOREIGN_TRANSACTION_LIST_ID = '" + id + "'", null);
        while(c.moveToNext()){
            SlaveEntity slaveEntity = SlaveEntity.getForTransactionalMode(c, productList, result);
            result.add(slaveEntity);
        }
        return result;
    }

    private MasterAccount getMasterAccountFromId(String id, SQLiteDatabase database){
        Cursor c = database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_MASTER_ACCOUNTS+" JOIN " + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + " ON " +
                DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + "." + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES+"_ID = " + DatabaseModel.TABLE_MASTER_ACCOUNTS + ".FOREIGN_"+DatabaseModel
                .TABLE_MASTER_ACCOUNT_TYPES + "_ID WHERE " + DatabaseModel.TABLE_MASTER_ACCOUNTS + "_ID = '" + id + "'", null);
        if(!c.moveToNext()){
            return null;
        }
        MasterAccount masterAccount = new MasterAccount(c);
        return masterAccount;
    }

    public void insertOrUpdateMasterAccount(final MasterAccount masterAccount, queryCompleteCallBack callBack, int processId, Executor executor) {
        new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                ContentValues cv = masterAccount.prepareContentValues();
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_MASTER_ACCOUNTS + " WHERE MASTER_ACCOUNTS_ID = ?", new String[]{masterAccount.id});
                if(c.moveToFirst()){
                    long result = database.update(DatabaseModel.TABLE_MASTER_ACCOUNTS, cv, "MASTER_ACCOUNTS_ID = ?", new String[]{masterAccount.id});
                    if(result == 0) return null;
                    else{
                        masterAccount.id = c.getString(c.getColumnIndex("MASTER_ACCOUNTS_ID"));
                        return masterAccount;
                    }
                }
                else{
                    long result = database.insert(DatabaseModel.TABLE_MASTER_ACCOUNTS, null, cv);
                    if(result == -1)  return null;
                    else{
                        return masterAccount;
                    }
                }
            }
        }.execute();
    }

    public void doesMasterAccountExistForName(final String name, queryCompleteCallBack callBack, int processId, Executor executor){

        new TaskManager(callBack, processId, executor){

            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT MASTER_ACCOUNTS_NAME FROM MASTER_ACCOUNTS WHERE MASTER_ACCOUNTS_NAME = ?", new String[]{name});
                if(c.getCount() != 0){
                    return true;
                }
                else{
                    return false;
                }
            }
        }.execute();
    }
    public void insertOrUpdateMasterAccountType(final MasterAccountType masterAccountType, queryCompleteCallBack callBack, int processId, Executor executor) {
        TaskManager manager = new TaskManager(callBack, processId, executor) {
            @Override
            public DatabaseObject asyncOperation() {
                ContentValues cv = masterAccountType.prepareContentValues();
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + " WHERE MASTER_ACCOUNT_TYPES_NAME = ?", new String[]{masterAccountType.name});
                if(c.moveToFirst()){
                    long result = database.update(DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES, cv, "MASTER_ACCOUNT_TYPES_NAME = ?", new String[]{masterAccountType.name});
                    if(result == 0) return null;
                    else{
                        masterAccountType.id = c.getString(c.getColumnIndex("MASTER_ACCOUNT_TYPES_ID"));
                        return masterAccountType;
                    }
                }
                else{
                    long result = database.insert(DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES, null, cv);
                    if(result == -1)  return null;
                    else{
                        return masterAccountType;
                    }
                }
            }
        };
        manager.execute();
    }

    public void getInvoicedTransactionForAccount(queryCompleteCallBack callback, int processId, Executor threadPoolExecutor, final MasterAccount masterAccount) {

    TaskManager manager = new TaskManager(callback, processId, threadPoolExecutor) {
        @Override
        public Object asyncOperation() {
            SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
            Cursor c = database.rawQuery("SELECT TRANSACTION_LIST_ID, FOREIGN_TRANSACTION_TYPES_ID FROM TRANSACTION_LIST WHERE FOREIGN_DEBIT_MASTER_ACCOUNTS_ID = ? " +
                    "OR FOREIGN_CREDIT_MASTER_ACCOUNTS_ID = ?", new String[]{String.valueOf(masterAccount.id), String.valueOf(masterAccount.id)});
            List<Transaction> transactions = new ArrayList<>();
            while (c.moveToNext()){
                int transactionType = c.getInt(c.getColumnIndex("FOREIGN_TRANSACTION_TYPES_ID"));
                if(transactionType == 1 || transactionType == 2){
                    String invoiceId = c.getString(c.getColumnIndex("TRANSACTION_LIST_ID"));
                    InvoicedTransaction transaction = DatabaseTalker.this.getInvoicedTransactionFromId(invoiceId);
                   transactions.add(transaction);
                }
            }
            return transactions;
        }
    };
        manager.execute();
    }

    public void getMasterAccountsForInvoiceOnlyModeForQuery(queryCompleteCallBack callBack, int processId, Executor executor, final String query) {
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public List asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c =database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_MASTER_ACCOUNTS + " JOIN " + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + " ON " +
                        DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + "." + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES+"_ID = " + DatabaseModel.TABLE_MASTER_ACCOUNTS + ".FOREIGN_"+DatabaseModel
                        .TABLE_MASTER_ACCOUNT_TYPES + "_ID WHERE MASTER_ACCOUNTS_NAME LIKE '%"+query+"%' AND MASTER_ACCOUNT_TYPES_BILLABLE = 1", null);
                List<MasterAccount> types;
                types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(new MasterAccount(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void getMasterAccountsForInvoiceOnlyMode(queryCompleteCallBack callBack, int processId, Executor executor) {
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public List asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c =database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_MASTER_ACCOUNTS + " JOIN " + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + " ON " +
                        DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + "." + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES+"_ID = " + DatabaseModel.TABLE_MASTER_ACCOUNTS + ".FOREIGN_"+DatabaseModel
                        .TABLE_MASTER_ACCOUNT_TYPES + "_ID WHERE MASTER_ACCOUNT_TYPES_BILLABLE = 1", null);
                List<MasterAccount> types;
                types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(new MasterAccount(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void insertOrUpdatePaymentReceiptTransaction(queryCompleteCallBack callBack, int processId, Executor executor, final PaymentReceiptTransaction transaction) {
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getWritableDatabase(BizTrackerApp.getAppContext());
                ContentValues cv = transaction.prepareContentValues();
                Cursor c = database.rawQuery("SELECT * FROM TRANSACTION_LIST WHERE TRANSACTION_LIST_ID = '"+transaction.id+"'", null);
                if(c.getCount() > 0){
                    updateAccountBalancesForTransaction(transaction, database, true);
                    database.delete("TRANSACTION_LIST", "TRANSACTION_LIST_ID = '" + transaction.id + "'", null);
                }
                long result = database.insert(DatabaseModel.TABLE_TRANSACTION_LIST, null, cv);
                updateAccountBalancesForTransaction(transaction, database, false);
                if(result != -1){
                    return transaction;
                }
                else{
                    return null;
                }
            }
        };
        task.execute();
    }

    public interface queryCompleteCallBack{
        void onReceivedDatabaseObject(Object databaseObject, int processId);
    }
    public static synchronized DatabaseTalker getInstance(){
        if(instance == null){
            instance = new DatabaseTalker();
        }
        return instance;
    }

    public void insertSlaveEntity(final SlaveEntity slaveEntity, final queryCompleteCallBack callBack, int processId, Executor executor){

        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getWritableDatabase(BizTrackerApp.getAppContext());
                ContentValues cv = slaveEntity.prepareContentValues();
                //TODO check for pre-existing same entity names berfore adding
                long id = database.insert(DatabaseModel.TABLE_SLAVE_ACCOUNTS, null, cv);
                if(id == -1)    return null;
                return slaveEntity;
            }
        };
        task.execute();
    }

    public void getAllProducts(final queryCompleteCallBack callBack, int processId, Executor executor){

        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public List asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c =database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + " JOIN " + DatabaseModel.TABLE_UNITS + " ON "
                        +DatabaseModel.TABLE_UNITS + ".UNITS_ID = " + DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + ".FOREIGN_UNITS_ID", null);
                List<Product> types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(Product.getForInventoryMode(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void slaveAccountExists(final queryCompleteCallBack callBack, int processId, Executor executor, final String slaveAccountName){
        TaskManager manager = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT SLAVE_ACCOUNTS_NAME FROM SLAVE_ACCOUNTS WHERE SLAVE_ACCOUNTS_NAME = ?", new String[]{slaveAccountName});
                if(c.getCount() != 0){
                    return true;
                }
                else{
                    return false;
                }
            }
        };
        manager.execute();
    }

    public void getAllSlaveEntities(final queryCompleteCallBack callBack, int processId, Executor executor){

        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public List asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_SLAVE_ACCOUNTS, null);
                List<SlaveEntity> types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(SlaveEntity.getForInventoryMode(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void getAllMasterAccountTypes(final queryCompleteCallBack callBack, int processId, Executor executor){

        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public List asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c =database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES, null);
                List<MasterAccountType> types;
                types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(new MasterAccountType(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void getAlInvoiceEnabledMasterAccounts(final queryCompleteCallBack callBack, int processId, Executor executor){

        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public List asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c =database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_MASTER_ACCOUNTS + " JOIN " + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + " ON " +
                        DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + "." + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES+"_ID = " + DatabaseModel.TABLE_MASTER_ACCOUNTS + ".FOREIGN_"+DatabaseModel
                        .TABLE_MASTER_ACCOUNT_TYPES + "_ID WHERE MASTER_ACCOUNT_TYPES_BILLABLE = 1", null);
                List<MasterAccount> types;
                types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(new MasterAccount(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void getAllMasterAccounts(final queryCompleteCallBack callBack, int processId, Executor executor){

        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public List asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c =database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_MASTER_ACCOUNTS + " JOIN " + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + " ON " +
                        DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + "." + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES+"_ID = " + DatabaseModel.TABLE_MASTER_ACCOUNTS + ".FOREIGN_"+DatabaseModel
                        .TABLE_MASTER_ACCOUNT_TYPES + "_ID", null);
                List<MasterAccount> types;
                types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(new MasterAccount(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void getMasterAccountsForQuery(final queryCompleteCallBack callBack, int processId, Executor executor, final String query){

        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public List asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c =database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_MASTER_ACCOUNTS + " JOIN " + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + " ON " +
                        DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES + "." + DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES+"_ID = " + DatabaseModel.TABLE_MASTER_ACCOUNTS + ".FOREIGN_"+DatabaseModel
                        .TABLE_MASTER_ACCOUNT_TYPES + "_ID WHERE MASTER_ACCOUNTS_NAME LIKE '%"+query+"%'", null);
                List<MasterAccount> types;
                types = new ArrayList<>();
                while(c.moveToNext()){
                    types.add(new MasterAccount(c));
                }
                return types;
            }
        };
        task.execute();
    }

    public void getAllTransactions(queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT TRANSACTION_LIST_ID, FOREIGN_TRANSACTION_TYPES_ID FROM " + DatabaseModel.TABLE_TRANSACTION_LIST, null);
                List<Transaction> transactions = new ArrayList<>(c.getCount());
                while (c.moveToNext()){
                    String ids = c.getString(c.getColumnIndex("TRANSACTION_LIST_ID"));
                    int transactionType = c.getInt(c.getColumnIndex("FOREIGN_TRANSACTION_TYPES_ID"));
                    if(transactionType == Transaction.TYPE_RECEIPT || transactionType == Transaction.TYPE_PAYMENT){
                        PaymentReceiptTransaction transaction = getPaymentReceiptTransactionFromId(ids);
                        transactions.add(transaction);
                    }
                    else if(transactionType == Transaction.TYPE_SALES || transactionType == Transaction.TYPE_PURCHASE){
                        InvoicedTransaction transaction = getInvoicedTransactionFromId(ids);
                        transactions.add(transaction);
                    }
                }
                return transactions;
            }
        };
        task.execute();
    }

    public void getAllTransactionsForMasterAccount(queryCompleteCallBack callBack, int processId, Executor executor, final MasterAccount masterAccount){
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT TRANSACTION_LIST_ID, FOREIGN_TRANSACTION_TYPES_ID FROM " + DatabaseModel.TABLE_TRANSACTION_LIST + " WHERE FOREIGN_DEBIT_MASTER_ACCOUNTS_ID = ?" +
                        " OR FOREIGN_CREDIT_MASTER_ACCOUNTS_ID = ?", new String[]{masterAccount.id, masterAccount.id});
                List<Transaction> transactions = new ArrayList<>(c.getCount());
                while (c.moveToNext()){
                    String ids = c.getString(c.getColumnIndex("TRANSACTION_LIST_ID"));
                    int transactionType = c.getInt(c.getColumnIndex("FOREIGN_TRANSACTION_TYPES_ID"));
                    if(transactionType == Transaction.TYPE_RECEIPT || transactionType == Transaction.TYPE_PAYMENT){
                        PaymentReceiptTransaction transaction = getPaymentReceiptTransactionFromId(ids);
                        transactions.add(transaction);
                    }
                    else if(transactionType == Transaction.TYPE_SALES || transactionType == Transaction.TYPE_PURCHASE){
                        InvoicedTransaction transaction = getInvoicedTransactionFromId(ids);
                        transactions.add(transaction);
                    }
                }
                return transactions;
            }
        };
        task.execute();
    }



    public void getAllUnits(queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public List asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c =database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_UNITS, null);
                List<Unit> types = new ArrayList<>();
                    while(c.moveToNext()){
                        types.add(new Unit(c));
                    }
                return types;
            }
        };
        task.execute();
    }

    public void getProductFromBarcode(final Barcode barcodeValue, final queryCompleteCallBack callBack, final int processId){

        TaskManager task = new TaskManager(callBack, processId, AsyncTask.SERIAL_EXECUTOR) {
            @Override
            public DatabaseObject asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + " JOIN " +
                        DatabaseModel.TABLE_UNITS + " ON " + DatabaseModel.TABLE_UNITS + "."+DatabaseModel.TABLE_UNITS+"_ID=" +
                        DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + ".FOREIGN_"+DatabaseModel.TABLE_UNITS+"_ID WHERE "+
                        DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES+"_BARCODE_ID='" + barcodeValue.rawValue + "';", null);
                Product p = null;
                while(c.moveToNext()){
                    p = Product.getForInventoryMode(c);
                }
                if(p == null){
                    p = new Product();
                    p.barCodeValue = barcodeValue.rawValue;
                }
                return p;
            }
        };
        task.execute();
    }

    public void getProductFromId(final String id, final queryCompleteCallBack callBack, final int processId){

        TaskManager task = new TaskManager(callBack, processId, AsyncTask.SERIAL_EXECUTOR) {
            @Override
            public DatabaseObject asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT * FROM " + DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + " JOIN " +
                        DatabaseModel.TABLE_UNITS + " ON " + DatabaseModel.TABLE_UNITS + "."+DatabaseModel.TABLE_UNITS+"_ID=" +
                        DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES + ".FOREIGN_"+DatabaseModel.TABLE_UNITS+"_ID WHERE "+
                        DatabaseModel.TABLE_TRANSACTIONAL_ENTITIES+"_ID = '" + id + "';", null);
                Product p = null;
                while(c.moveToNext()){
                    p = Product.getForInventoryMode(c);
                }
                return p;
            }
        };
        task.execute();
    }

    public void loadProductsInInvoicedTransaction(final InvoicedTransaction transaction, queryCompleteCallBack callBack, int processId, Executor executor){

        TaskManager manager = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("select * from MASTER_TRANSACTION_DETAILS join TRANSACTIONAL_ENTITIES on TRANSACTIONAL_ENTITIES.TRANSACTIONAL_ENTITIES_ID = " +
                        "MASTER_TRANSACTION_DETAILS.FOREIGN_TRANSACTIONAL_ENTITIES_ID join UNITS on UNITS.UNITS_ID = TRANSACTIONAL_ENTITIES.FOREIGN_UNITS_ID where " +
                        "FOREIGN_MASTER_TRANSACTION_LIST_ID = ?;", new String[]{String.valueOf(transaction.id)});
                transaction.productList = new ArrayList<>(c.getCount());
                while (c.moveToNext()){
                    Product product = Product.getForDetailedInvoiceMode(c);
                    transaction.productList.add(product);
                }
                return transaction;
            }
        };
        manager.execute();
    }

    public MasterAccount getMasterAccountForId(String id){
        SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
        Cursor c = database.rawQuery("SELECT * FROM "+DatabaseModel.TABLE_MASTER_ACCOUNTS + " JOIN "+DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES
                +" ON "+ DatabaseModel.TABLE_MASTER_ACCOUNTS +".FOREIGN_MASTER_ACCOUNT_TYPES_ID = "+DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES +
                ".MASTER_ACCOUNT_TYPES_ID"+" WHERE MASTER_ACCOUNTS_ID = '" + id + "'", null);
        MasterAccount m = null;
        while (c.moveToNext()){
            m = new MasterAccount(c);
        }
        return m;
    }

    public void getMasterAccount(final String id, queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager manager = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {
                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT * FROM "+DatabaseModel.TABLE_MASTER_ACCOUNTS + " JOIN "+DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES
                        +" ON "+ DatabaseModel.TABLE_MASTER_ACCOUNTS +".FOREIGN_MASTER_ACCOUNT_TYPES_ID = "+DatabaseModel.TABLE_MASTER_ACCOUNT_TYPES +
                        ".MASTER_ACCOUNT_TYPES_ID"+" WHERE MASTER_ACCOUNTS_ID = '" + id + "'", null);
                MasterAccount m = null;
                while (c.moveToNext()){
                    m = new MasterAccount(c);
                }
                return m;
            }
        };
        manager.execute();
    }

    public int getCountForTransaction(Transaction transaction){
        SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
        Cursor c = database.rawQuery("select count (*) from TRANSACTION_LIST where FOREIGN_TRANSACTION_TYPES_ID = '" + transaction.getType()
        + "';", null);
        while (c.moveToNext()){
            return c.getInt(0);
        }
        return 0;
    }

    public void getProductHistoryList(final Product product, queryCompleteCallBack callBack, int processId, Executor executor){
        TaskManager task = new TaskManager(callBack, processId, executor) {
            @Override
            public Object asyncOperation() {

                SQLiteDatabase database = DatabaseModel.getReadableDatabase(BizTrackerApp.getAppContext());
                Cursor c = database.rawQuery("SELECT * FROM MASTER_TRANSACTION_DETAILS JOIN TRANSACTION_LIST ON " +
                        "TRANSACTION_LIST.TRANSACTION_LIST_ID = MASTER_TRANSACTION_DETAILS.FOREIGN_MASTER_TRANSACTION_LIST_ID WHERE FOREIGN_TRANSACTIONAL_ENTITIES_ID = '"
                        + product.id + "'", null);
                List<ProductHistoryBean> result = new ArrayList<>();
                while (c.moveToNext()){
                    ProductHistoryBean bean = new ProductHistoryBean(c);
                    String masterAccountId = "-1";
                    if(bean.transactionType == Transaction.TYPE_SALES){
                        masterAccountId = c.getString(c.getColumnIndex("FOREIGN_DEBIT_MASTER_ACCOUNTS_ID"));
                    }
                    else if(bean.transactionType == Transaction.TYPE_PURCHASE){
                        masterAccountId = c.getString(c.getColumnIndex("FOREIGN_CREDIT_MASTER_ACCOUNTS_ID"));
                    }
                    if(!masterAccountId.equals("-1")){
                        bean.masterAccountName = DatabaseTalker.this.getMasterAccountNameFromId(masterAccountId, database);
                    }
                    else{
                        continue;
                    }
                    result.add(bean);
                }
                return result;
            }
        };
        task.execute();
    }

    private String getMasterAccountNameFromId(String id, SQLiteDatabase database){
        Cursor c = database.rawQuery("SELECT MASTER_ACCOUNTS_NAME FROM MASTER_ACCOUNTS WHERE MASTER_ACCOUNTS_ID = '" +id + "'", null);
        while (c.moveToNext()){
            return c.getString(c.getColumnIndex("MASTER_ACCOUNTS_NAME"));
        }
        return null;
    }

    public abstract class TaskManager{
        public abstract Object asyncOperation();
        private AsyncTask task;
        private Executor executor;
        Object callback;
        int processId;
        TaskManager(final Object callback, final int processId, Executor executor){
            this.callback = callback;
            this.processId = processId;
            if(executor != null){
                this.executor = executor;
                this.task = new AsyncTask<Object, Object, Object>() {
                    @Override
                    protected Object doInBackground(Object... params) {
                        return asyncOperation();
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        if (callback != null) {
                            ((queryCompleteCallBack)callback).onReceivedDatabaseObject(o, processId);
                        }
                    }
                };
            }
        }
        public void execute(){
            if(task != null)    task.executeOnExecutor(executor);
            else{
                Object result = asyncOperation();
                if (callback != null) ((queryCompleteCallBack)callback).onReceivedDatabaseObject(result, processId);
            }
        }
    }
}
