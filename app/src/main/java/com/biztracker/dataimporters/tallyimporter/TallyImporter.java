package com.biztracker.dataimporters.tallyimporter;

import android.os.Environment;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by karan on 12/04/16.
 */
public class TallyImporter {

    private ArrayList<List<String>> importLedgerMastersFromXml(Integer report){


        XmlPullParserFactory factory = null;
        List<String> newLedgers=new ArrayList<String>();
        List<String> modLedgers=new ArrayList<String>();
        List<String> restrictedLedgers=new ArrayList<String>();

        boolean ignore;
        if(report==2)
            ignore=true;
        else
            ignore=false;
        try {
            factory = XmlPullParserFactory.newInstance();
            final XmlPullParser xpp = factory.newPullParser();
            factory.setNamespaceAware(true);
            xpp.setInput(new FileInputStream(Environment.getExternalStorageDirectory()+"/BizTracker/"+readFileName), null);
            int eventType=xpp.getEventType();
            String aname="", address="", phone="-", a_type="", address2="", email="-", tax="-", balance="0",pin="",rateOfTax="", taxType="", subTax="";
            while(eventType!=XmlPullParser.END_DOCUMENT){


                if(eventType==XmlPullParser.START_TAG){
                    String tagName=xpp.getName();

                    if(tagName.equals("LEDGER")){
                        aname=xpp.getAttributeValue(0);
                        aname = aname.replaceAll("'", "").replaceAll("\"", "");
                    }
                    else if(tagName.equals("ADDRESS")){
                        if(address.equals(""))
                            address=xpp.nextText();
                        else{
                            if(address2.equals(""))
                                address2=xpp.nextText();

                            else
                                address2=address2+", "+xpp.nextText();
                        }
                    }
                    else if(tagName.equals("LEDGERPHONE")){
                        phone=xpp.nextText();
                    }
                    else if(tagName.equals("PARENT")){
                        a_type=xpp.nextText();
                    }
                    else if(tagName.equals("EMAIL")){
                        email=xpp.nextText();
                    }
                    else if(tagName.equals("VATTINNUMBER")){
                        tax=xpp.nextText();
                    }
                    else if(tagName.equals("PINCODE")){
                        pin=xpp.nextText();
                    }
                    else if(tagName.equals("OPENINGBALANCE")){
                        balance=xpp.nextText();
                    }
                    else if(tagName.equals("RATEOFTAXCALCULATION")){
                        rateOfTax=xpp.nextText();
                    }
                    else if(tagName.equals("TAXTYPE")){
                        taxType=xpp.nextText();
                    }
                    else if(tagName.equals("SUBTAXTYPE")){
                        subTax=xpp.nextText();
                    }
                }
                if(eventType==XmlPullParser.END_TAG){

                    if(xpp.getName().equals("LEDGER")){
                        address2=address2+", "+pin;
                        if(address.equals(""))
                            address="0";
                        if(phone.equals(""))
                            phone="0";
                        if(address2.equals(""))
                            address2="0";
                        if(email.equals(""))
                            email="0";
                        if(tax.equals(""))
                            tax="0";
                        if(rateOfTax.equals(""))
                            rateOfTax="0";
                        if(taxType.equals(""))
                            taxType="0";
                        if(subTax.equals(""))
                            subTax="0";

                        boolean accountNotPresent=db.accountNotPresent(aname);

                        if(accountNotPresent){
                            if(insertLedger(aname,address, phone, a_type, address2, email, tax, balance, rateOfTax, taxType, subTax))
                                newLedgers.add(aname);
                            else{
                                restrictedLedgers.add(aname);
                            }

                        }
                        else if(!ignore && !accountNotPresent){
                            String accType = db.getAccountType(aname);
                            if(db.isAccountDebitOrCredit(accType).equals("d")){
                                if(Double.valueOf(balance)!=0)
                                    balance=String.valueOf(-Double.valueOf(balance));
                            }
                            db.change_opening_balance(aname, Double.valueOf(balance));
                            modLedgers.add(aname);
                        }
                        else if(ignore && !accountNotPresent){
                            modLedgers.add(aname);
                        }

                        aname="";
                        address="";
                        phone="";
                        a_type="";
                        address2="";
                        email="";
                        tax="";
                        balance="0";
                        pin="";
                        rateOfTax="";
                    }
                }
                eventType=xpp.next();
            }

        } catch(Exception e){

        }
        ArrayList<List<String>> returnResult=new ArrayList<List<String>>();
        returnResult.add(newLedgers);
        returnResult.add(modLedgers);
        returnResult.add(restrictedLedgers);
        return returnResult;

    }
}
