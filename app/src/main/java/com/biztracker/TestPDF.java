package com.biztracker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by karan on 04/07/16.
 */

public class TestPDF {
    View view;
    Context context;
    public String filepath;
    public Uri fileUri;

    public TestPDF(Context context,View view){
        this.context = context;
        this.view = view;
    }

    public void draw() throws IOException {
        PdfDocument document = new PdfDocument();

        // crate a page description
        PdfDocument.PageInfo pageInfo;
        pageInfo = new PdfDocument.PageInfo.Builder(2480,3508,1).create();

        // start a page
        PdfDocument.Page page = document.startPage(pageInfo);

        // draw something on the page
        View content = this.view;
        Canvas c = page.getCanvas();
        float scale = c.getWidth()/getScreenWidth();
        c.scale(scale,scale);
        Log.d("BizApp","Canvas: "+c.getWidth()+","+c.getHeight());
        content.draw(page.getCanvas());

        // finish the page
        document.finishPage(page);

        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+"/test_invoice_pdf.pdf");
        filepath = file.getAbsolutePath();

        fileUri = Uri.fromFile(file);

        file.createNewFile();
        FileOutputStream fOut = new FileOutputStream(file);
        document.writeTo(fOut);
        // close the document
        document.close();
        fOut.close();
    }

    private float getScreenWidth(){
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }


}
