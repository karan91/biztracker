package com.biztracker.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.biztracker.R;

/**
 * Created by shubham on 16/04/16.
 */
public class LockPopupDialog extends Dialog {

    public interface LockPopupDialogCallback{
        void onClickedPurchase();
        void onClickedContactUs();
    }

    public LockPopupDialogCallback mCallback = null;

    public LockPopupDialog(Context context,String lockMessage,String purchaseAction) {
        super(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        setCancelable(false);
        setContentView(R.layout.dialog_lock_popup);
        setDetails(lockMessage,purchaseAction);
    }


    public void setDetails(String lockMessage,String purchaseAction){
        findViewById(R.id.purchase_subscription).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onPurchaseActionClicked();
            }
        });

        findViewById(R.id.contact_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onContactUsClicked();
            }
        });
    }

    public void onPurchaseActionClicked(){

    }

    public void onContactUsClicked(){

    }

}
