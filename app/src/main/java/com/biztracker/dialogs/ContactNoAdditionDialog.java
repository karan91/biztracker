package com.biztracker.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import com.biztracker.R;

/**
 * Created by shubham on 24/06/16.
 */
public class ContactNoAdditionDialog extends Dialog {

    public interface ContactNoAdditionDialogCallback{
        void onAddPhoneNo();
    }

    public ContactNoAdditionDialogCallback mCallback;

    public ContactNoAdditionDialog(Context context) {
        super(context, R.style.AppDialog);
        setCancelable(false);
        setContentView(R.layout.contact_addition_dialog);
        findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCancel();
            }
        });
        findViewById(R.id.okay_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSet();
            }
        });
    }

    public void onCancel(){
        cancel();
    }

    public void onSet(){
        if(mCallback != null){
            mCallback.onAddPhoneNo();
        }
        cancel();
    }
}
