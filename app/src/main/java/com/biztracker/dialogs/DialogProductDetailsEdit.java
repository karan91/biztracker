package com.biztracker.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.common.Utilities;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.Transaction;

import java.math.BigDecimal;

/**
 * Created by shubham on 11/06/16.
 */
public class DialogProductDetailsEdit extends Dialog implements TextView.OnEditorActionListener {

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        if (v == quantityTextView){
            this.onAdd();
        }
        return false;
    }

    public interface DialogProductDetailsEditCallback{
        void onCancel(Product product,DialogProductDetailsEdit dialog);
        void onSet(Product product,DialogProductDetailsEdit dialog);
    }

    public DialogProductDetailsEditCallback mCallback;
    private Product product;
    private int transactionType;
    private Activity activity;
    private TextView pricePerUnitTextView;
    private TextView quantityTextView;
    private TextView productNameTextView;

    public DialogProductDetailsEdit(Activity context) {
        super(context,R.style.AppDialog);
        this.activity = context;
        setCancelable(false);
        setContentView(R.layout.dialog_product_details_edit_layout);

        pricePerUnitTextView = (TextView) findViewById(R.id.price_per_unit_field);
        quantityTextView = (TextView)findViewById(R.id.quantity_field);
        quantityTextView.setOnEditorActionListener(this);
        productNameTextView = (TextView)findViewById(R.id.product_name);

        findViewById(R.id.reset_btn).setOnClickListener(onClickListener);
        findViewById(R.id.discard_btn).setOnClickListener(onClickListener);
        findViewById(R.id.add_btn).setOnClickListener(onClickListener);

        if(product != null){
            setProduct(product, this.transactionType);
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Utilities.showKeyboard(getContext());
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismiss();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.reset_btn:
                    setProduct(product, transactionType);
                    break;
                case R.id.discard_btn:
                    onCancel();
                    break;
                case R.id.add_btn:
                    onAdd();
                    break;
            }
        }
    };

    public void setProduct(Product _product, int transactionType){
        product = _product;
        this.transactionType = transactionType;
        if(product != null){
            if(pricePerUnitTextView != null){
                String value = "";
                if(product.entityValue != null){
                    value = product.entityValue.toString();
                }
                else if(transactionType == Transaction.TYPE_SALES){
                    value = product.sellingPrice.toString();
                }
                else if(transactionType == Transaction.TYPE_PURCHASE){
                    value = product.purchasePrice.toString();
                }
                pricePerUnitTextView.setText(value);
            }

            if(quantityTextView != null){
                if(product.entityQty != null){
                    quantityTextView.setText(product.entityQty.toString());
                }
                else{
                    quantityTextView.setText("0");
                }
            }

            if(productNameTextView != null){
                productNameTextView.setText(product.name);
            }
        }
    }

    public void onCancel(){
        dismiss();
        if(mCallback != null){
            mCallback.onCancel(product,this);
        }
    }

    public void onAdd(){
        dismiss();

        this.product.entityQty = new BigDecimal(quantityTextView.getText().toString().trim());
        this.product.entityValue = new BigDecimal(pricePerUnitTextView.getText().toString().trim());

        if(mCallback != null){
            mCallback.onSet(product,this);
        }
    }

    public void onReset(){

    }
}
