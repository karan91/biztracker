package com.biztracker.urlconnections;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by shubham on 14/04/16.
 */
public class NetworkAsyncTask extends AsyncTask {

    public interface OnNetworkAsyncTaskCallback{
        void onTaskCompleted(JSONObject jsonObject);
        void onTaskFailed(int error);
    }

    private static String TAG = "NET_TAG";

    private OnNetworkAsyncTaskCallback mCallback;
    private String mURLString;
    private JSONObject mPostParams;
    private String mResponse;

    public NetworkAsyncTask(String _urlString,JSONObject _params,OnNetworkAsyncTaskCallback _callback){
        mURLString = _urlString;
        mPostParams = _params;
        mCallback = _callback;
    }


    @Override
    protected Object doInBackground(Object[] params) {

        try {
            URL url = new URL(mURLString);
            HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestProperty("Accept","application/json");
            conn.connect();

            OutputStream outputStream = conn.getOutputStream();
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(outputStream));
                writer.write(mPostParams.toString());
                writer.flush();
            }catch (Exception ex){
                Log.e(TAG,"Exception: "+ex);
            }finally {
                if(writer != null) writer.close();
                outputStream.close();
            }

            StringBuffer response = new StringBuffer();

            InputStream in = conn.getInputStream();
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = reader.readLine()) != null){
                    response.append(line);
                }
            }catch (Exception ex){
                Log.e(TAG, "Exception: " + ex);
            }
            finally {
                in.close();
            }

            mResponse = response.toString();

            Log.d("Response","Response: "+mResponse);

        } catch (MalformedURLException e) {
            Log.e(TAG, "Exception: " + e);
        } catch (IOException e) {
            Log.e(TAG, "Exception: " + e);
        }


        return null;
    }
}
