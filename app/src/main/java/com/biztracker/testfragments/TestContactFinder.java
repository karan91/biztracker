package com.biztracker.testfragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biztracker.R;
import com.biztracker.fragments.SuperFragment;

/**
 * Created by shubham on 11/04/16.
 */
public class TestContactFinder extends SuperFragment{

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.test_contact_finder,null);
        return view;
    }
}
