package com.biztracker.prints;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.print.PrintManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.biztracker.R;
import com.biztracker.TestPDF;
import com.biztracker.activities.MainActivity;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.Company;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.Product;
import com.biztracker.databaseobjects.SlaveEntity;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.databaseobjects.TransactionType;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.permissions.PermissionRequester;

import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham on 04/07/16.
 */
public class PDFInvoiceViewAndPrintFragment extends SuperFragment implements DatabaseTalker.queryCompleteCallBack {

    public boolean showPrintUI = false;
    public boolean showShareUI = false;

    public InvoicedTransaction transaction;
    private TextView companyName;
    private TextView companyAddress;
    private TextView phone;
    private TextView email;
    private TextView taxNumber;
    private TextView invoiceNumber;
    private TextView invoiceDate;
    private TextView sellerName;
    private TextView sellerPhone;
    private TextView sellerAddress;
    private LinearLayout productViewParent;
    private LinearLayout slaveEntityParent;

    private View printButton;
    private View shareButton;

    private View parentView;

    private Object mLock = new Object();
    private boolean mViewLoaded = false;
    private boolean mObjectLoaded = false;

    public PDFInvoiceViewAndPrintFragment() {
        super();
    }

    @Override
    public boolean validateUIFields() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_pdf_view_print, null);

        productViewParent = (LinearLayout) parentView.findViewById(R.id.product_parent_view);

        productViewParent.removeAllViewsInLayout();

        slaveEntityParent = (LinearLayout) parentView.findViewById(R.id.slave_entity_parent);
        slaveEntityParent.removeAllViewsInLayout();

        companyName = (TextView) parentView.findViewById(R.id.company_name);
        companyAddress = (TextView) parentView.findViewById(R.id.company_address);
        phone = (TextView) parentView.findViewById(R.id.company_phone);
        email = (TextView) parentView.findViewById(R.id.company_email);
        taxNumber = (TextView) parentView.findViewById(R.id.company_tax);
        invoiceNumber = (TextView) parentView.findViewById(R.id.invoice_id);
        invoiceDate = (TextView) parentView.findViewById(R.id.invoice_date);
        sellerName = (TextView) parentView.findViewById(R.id.to_name);
        sellerPhone = (TextView) parentView.findViewById(R.id.to_phone);
        sellerAddress = (TextView) parentView.findViewById(R.id.to_address);

        printButton = parentView.findViewById(R.id.action_print);
        shareButton = parentView.findViewById(R.id.action_share);
        printButton.setVisibility(View.INVISIBLE);
        shareButton.setVisibility(View.INVISIBLE);
        printButton.setOnClickListener(mClickListener);
        shareButton.setOnClickListener(mClickListener);

        loadInvoiceTransaction(savedInstanceState);

        return parentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        synchronized (mLock){
            mViewLoaded = true;
            if(mObjectLoaded){
                handleDefaultActions();
            }
        }
    }

    public void handleDefaultActions(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if(showPrintUI){
                    mClickListener.onClick(printButton);
                }else if(showShareUI){
                    mClickListener.onClick(shareButton);
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (transaction != null) outState.putString("invoice_transaction_id", transaction.id);
    }

    private void loadInvoiceTransaction(Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            String transactionID = savedInstanceState.getString("invoice_transaction_id");
            if (transactionID != null) {
                //Reload
                DatabaseTalker.getInstance().getInvoiceTransaction(this, 5, AsyncTask.THREAD_POOL_EXECUTOR, transactionID);
            }
        } else {
            if (transaction.isLoaded()) {
                this.onReceivedDatabaseObject(transaction, 5);
            } else {
                DatabaseTalker.getInstance().getInvoiceTransaction(this, 5, AsyncTask.THREAD_POOL_EXECUTOR, this.transaction.id);
            }
        }
    }

    public void setTransaction(InvoicedTransaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if (databaseObject != null) {
            this.transaction = (InvoicedTransaction) databaseObject;
            loadView();

            synchronized (mLock){
                mObjectLoaded = true;
                if(mViewLoaded){
                    handleDefaultActions();
                }
            }
        }
    }

    private void loadView() {
        companyName.setText(Company.company.businessName);
        companyAddress.setText(Company.company.address);
        phone.setText("Phone " + Company.company.phone);
        email.setText("Email " + Company.company.emailAddress);
        taxNumber.setText("Tax No. " + Company.company.taxationNumber);
        invoiceNumber.setText(transaction.transactionNumber);
        invoiceDate.setText(transaction.getPrettyDate());
        sellerName.setText(transaction.getActiveAccount().name);
        sellerPhone.setText(transaction.getActiveAccount().phone);
        sellerAddress.setText(transaction.getActiveAccount().address);


        int i = 1;
        for (Product p : transaction.productList) {
            LinearLayout layout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.pdf_invoice_list_item_layout, null);

            TextView serialHolder = (TextView) layout.findViewById(R.id.sno);
            TextView productNameHolder = (TextView) layout.findViewById(R.id.description);
            TextView unitHolder = (TextView) layout.findViewById(R.id.unit);
            TextView qtyHolder = (TextView) layout.findViewById(R.id.quantity);
            TextView unitPriceHolder = (TextView) layout.findViewById(R.id.rate);
            TextView totalPriceHolder = (TextView) layout.findViewById(R.id.total);

            serialHolder.setText(String.valueOf(i));
            productNameHolder.setText(p.name);
            unitHolder.setText(p.unit.name);
            qtyHolder.setText(p.entityQty.toString());
            unitPriceHolder.setText(p.entityValue.toString());
            totalPriceHolder.setText(p.getInvoiceValue().toString());

            productViewParent.addView(layout);

            i++;
        }

        LinearLayout layout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.pdf_invoice_list_subitem_layout, null);

        TextView itemName = (TextView) layout.findViewById(R.id.slave_entity_name);
        TextView itemValue = (TextView) layout.findViewById(R.id.slave_entity_value);

        itemName.setText("Subtotal");
        itemValue.setText(transaction.getFormattedTotalValueOfInvoiceExcludingSlaveEntities());
        slaveEntityParent.addView(layout);

        for (SlaveEntity slaveEntity : transaction.slaveEntities) {

            layout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.pdf_invoice_list_subitem_layout, null);

            itemName = (TextView) layout.findViewById(R.id.slave_entity_name);
            itemValue = (TextView) layout.findViewById(R.id.slave_entity_value);

            String itemText = slaveEntity.name;

            List<String> productIds = new ArrayList<>();
            int j = 1;
            for (Product p : transaction.productList) {
                if(slaveEntity.productList == null){
                    break;
                }
                if (slaveEntity.productList.contains(p)) {
                    productIds.add(String.valueOf(j));
                }
                j++;
            }

            if (slaveEntity.calculatedAs == SlaveEntity.CALCULATED_AS_PERCENT) {
                itemText = itemText + " @ " + slaveEntity.getValue() + "% (" + TextUtils.join(", ", productIds) + ")";
            }
            itemName.setText(itemText);
            itemValue.setText(slaveEntity.getFormattedPlainInvoiceValue());

            slaveEntityParent.addView(layout);
        }

        TextView invoiceTotal = (TextView) parentView.findViewById(R.id.invoice_total);
        invoiceTotal.setText(transaction.getTotalFormattedValue());

        TextView companyNameSignature = (TextView) parentView.findViewById(R.id.company_name_signature);
        companyNameSignature.setText("For " + Company.company.businessName);


        printButton.setVisibility(View.VISIBLE);
        shareButton.setVisibility(View.VISIBLE);

    }


    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            if(view == shareButton) {
                MainActivity activity = getMainActivity();
                if (activity != null) {
                    activity.requestPermission(PermissionRequester.Permissions.STORAGE, new PermissionRequester.OnPermissionRequesterCompleted() {
                        @Override
                        public void onPermissionGranted(String permission) {
                            MainActivity activity = getMainActivity();
                            if (activity != null) {
                                TestPDF pdf = new TestPDF(activity, getView().findViewById(R.id.pdf_page_1));
                                try {
                                    pdf.draw();
                                    if (view.getId() == R.id.action_share) {
                                        if (pdf.fileUri != null) {
                                            activity.shareIntentForFile(pdf.filepath, pdf.fileUri);
                                        } else {
                                            Toast.makeText(getContext(), "Unable To Save Pdf", Toast.LENGTH_LONG);
                                        }
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getContext(), "Unable To Save Pdf", Toast.LENGTH_LONG);
                                }
                            }
                        }

                        @Override
                        public void onPermissionCancelled(String permission) {
                            Toast.makeText(getContext(), "We need to access storage to print or share pdf", Toast.LENGTH_LONG);
                        }
                    });

                }
            }
            else if(printButton == view){
                PrintManager printManager = (PrintManager) getActivity()
                        .getSystemService(Context.PRINT_SERVICE);

                String jobName = getActivity().getString(R.string.app_name) + " Document";

                printManager.print(jobName, new InvoicePrintDocumentAdapter(getActivity(), getView().findViewById(R.id.pdf_page_1)), null);
            }
        }
    };

}
