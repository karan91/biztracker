package com.biztracker.prints;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.print.PrintManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.biztracker.R;
import com.biztracker.TestPDF;
import com.biztracker.activities.MainActivity;
import com.biztracker.database.DatabaseTalker;
import com.biztracker.databaseobjects.InvoicedTransaction;
import com.biztracker.databaseobjects.PaymentReceiptTransaction;
import com.biztracker.databaseobjects.Transaction;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.permissions.PermissionRequester;

import java.io.IOException;

/**
 * Created by shubham on 08/07/16.
 */
public abstract class PDFPaymentReceiptPrintFragment extends SuperFragment implements DatabaseTalker.queryCompleteCallBack {

    public boolean showShareUI = false;
    public boolean showPrintUI = false;

    private Object mLock = new Object();
    private boolean mViewLoaded = false;
    private boolean mObjectLoaded = false;

    public PaymentReceiptTransaction transaction;
    protected TextView mAmountTextView;
    protected TextView mNotesTextView;
    protected TextView mPaymentThroughTextView;
    protected TextView mDateTextView;

    protected TextView paymentFirstHolder, paymentSecondHolder, paymentFirstName, paymentSecondName;

    protected View shareButton;
    protected View printButton;

    @Override
    public boolean validateUIFields() {
        return false;
    }

    protected abstract int getlayoutID();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getlayoutID(),null);
        mDateTextView = (TextView) view.findViewById(R.id.invoice_date);
        mPaymentThroughTextView = (TextView) view.findViewById(R.id.payment_through);
        mNotesTextView = (TextView) view.findViewById(R.id.note_area);
        mAmountTextView = (TextView) view.findViewById(R.id.amount);

        paymentFirstHolder = (TextView) view.findViewById(R.id.payment_first_holder);
        paymentSecondHolder = (TextView) view.findViewById(R.id.payment_second_holder);
        paymentFirstName = (TextView) view.findViewById(R.id.payment_first_name);
        paymentSecondName = (TextView) view.findViewById(R.id.payment_second_name);

        shareButton = view.findViewById(R.id.action_share);
        printButton = view.findViewById(R.id.action_print);

        shareButton.setVisibility(View.INVISIBLE);
        printButton.setVisibility(View.INVISIBLE);

        shareButton.setOnClickListener(mClickListener);
        printButton.setOnClickListener(mClickListener);

        loadTransaction(savedInstanceState);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        synchronized (mLock){
            mViewLoaded = true;
            if(mObjectLoaded){
                handleDefaultActions();
            }
        }
    }

    public void handleDefaultActions(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if(showPrintUI){
                    mClickListener.onClick(printButton);
                }else if(showShareUI){
                    mClickListener.onClick(shareButton);
                }
            }
        });
    }

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            MainActivity activity = getMainActivity();
            if (activity != null) {
                activity.requestPermission(PermissionRequester.Permissions.STORAGE, new PermissionRequester.OnPermissionRequesterCompleted() {
                    @Override
                    public void onPermissionGranted(String permission) {
                        if (view.getId() == R.id.action_share) {
                            MainActivity activity = getMainActivity();
                            if (activity != null) {
                                TestPDF pdf = new TestPDF(activity, getView().findViewById(R.id.pdf_page_1));
                                try {
                                    pdf.draw();
                                    if (pdf.fileUri != null) {
                                        activity.shareIntentForFile(pdf.filepath, pdf.fileUri);
                                    } else {
                                        Toast.makeText(getContext(), "Unable To Save Pdf", Toast.LENGTH_LONG);
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getContext(), "Unable To Save Pdf", Toast.LENGTH_LONG);
                                }
                            }
                        }
                        else if(view.getId() == R.id.action_print){
                            PrintManager printManager = (PrintManager) getActivity()
                                    .getSystemService(Context.PRINT_SERVICE);

                            String jobName = getActivity().getString(R.string.app_name) + " Document";

                            printManager.print(jobName, new InvoicePrintDocumentAdapter(getActivity(), getView().findViewById(R.id.pdf_page_1)), null);
                        }
                    }

                    @Override
                    public void onPermissionCancelled(String permission) {
                        Toast.makeText(getContext(),"We need to access storage to print or share pdf",Toast.LENGTH_LONG);
                    }
                });

            }
        }
    };

    public void setTransaction(PaymentReceiptTransaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public void resetTransactionAndUIFields() {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (transaction != null) outState.putString("transaction_id", transaction.id);
    }

    private void loadTransaction(Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            String transactionID = savedInstanceState.getString("transaction_id");
            if (transactionID != null) {
                //Reload
                DatabaseTalker.getInstance().getPaymentReceiptTransactionUsingCallback(this, 5, AsyncTask.THREAD_POOL_EXECUTOR, transactionID);
            }
        } else {
            if (transaction != null) {
                this.onReceivedDatabaseObject(transaction, 5);
            } else {
                DatabaseTalker.getInstance().getPaymentReceiptTransactionUsingCallback(this, 5, AsyncTask.THREAD_POOL_EXECUTOR, this.transaction.id);
            }
        }
    }

    @Override
    public void onReceivedDatabaseObject(Object databaseObject, int processId) {
        if (databaseObject != null) {
            this.transaction = (PaymentReceiptTransaction) databaseObject;
            loadView();

            synchronized (mLock){
                mObjectLoaded = true;
                if(mViewLoaded){
                    handleDefaultActions();
                }
            }
        }
    }

    protected void loadView(){
        shareButton.setVisibility(View.VISIBLE);
        printButton.setVisibility(View.VISIBLE);
        mAmountTextView.setText(transaction.getTotalFormattedValue());
        mDateTextView.setText(transaction.getPrettyDate());
    }
}