package com.biztracker.prints;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;

/**
 * Created by karan on 09/07/16.
 */

public class PDFReceiptFragment extends PDFPaymentReceiptPrintFragment{

    private TextView mPaymentFromTextView;

    @Override
    protected int getlayoutID() {
        return R.layout.pdf_receipt_layout;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater,container,savedInstanceState);
        mPaymentFromTextView = (TextView) view.findViewById(R.id.payment_from_name);

        //paymentFirstName.setText(transaction.creditAccount.name);
        //paymentSecondName.setText(transaction.debitAccount.name);
        //paymentFirstHolder.setText("PAYMENT FROM");
        //paymentSecondHolder.setText("PAYMENT THROUGH");

        return view;
    }

    @Override
    protected void loadView() {
        super.loadView();

    }
}