package com.biztracker.prints;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.pdf.PrintedPdfDocument;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by karan on 13/07/16.
 */

public class InvoicePrintDocumentAdapter extends PrintDocumentAdapter {
    PrintedPdfDocument mPdfDocument;
    Context context;
    private View view;
    int pages;

    public InvoicePrintDocumentAdapter(Context context, View view){
        this.context = context;
        this.view = view;
    }

    @Override
    public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras) {
        mPdfDocument = new PrintedPdfDocument(context, newAttributes);

        if (cancellationSignal.isCanceled() ) {
            callback.onLayoutCancelled();
            return;
        }

        pages = 1;

        if (pages > 0) {
            // Return print information to print framework
            PrintDocumentInfo info = new PrintDocumentInfo
                    .Builder("print_output.pdf")
                    .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                    .setPageCount(pages).build();
            // Content layout reflow is complete
            callback.onLayoutFinished(info, true);
        } else {
            // Otherwise report an error to the print framework
            callback.onLayoutFailed("Page count calculation failed.");
        }
    }

    @Override
    public void onWrite(PageRange[] pageRanges, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback) {

        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(2480,3508,1).create();
        PdfDocument.Page page = mPdfDocument.startPage(pageInfo);

        // check for cancellation
        drawPage(page);
        mPdfDocument.finishPage(page);

        // Write PDF document to file
        try {
            mPdfDocument.writeTo(new FileOutputStream(
                    destination.getFileDescriptor()));
        } catch (IOException e) {
            callback.onWriteFailed(e.toString());
            return;
        } finally {
            mPdfDocument.close();
            mPdfDocument = null;
        }
        // Signal the print framework the document is complete
        callback.onWriteFinished(pageRanges);
    }

    private boolean containsPage(PageRange[] pageRanges, int page){
        for(PageRange range : pageRanges){
            if(page > range.getStart() && page < range.getEnd())    return true;
        }
        return false;
    }

    private void drawPage(PdfDocument.Page page){
        View content = this.view;
        Canvas c = page.getCanvas();
        float scale = c.getWidth()/getScreenWidth();
        c.scale(scale,scale);
        content.draw(page.getCanvas());
    }

    private float getScreenWidth(){
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }
}
