package com.biztracker.prints;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biztracker.R;

/**
 * Created by karan on 09/07/16.
 */


public class PDFPaymentFragment extends PDFPaymentReceiptPrintFragment{


    @Override
    protected int getlayoutID() {
        return R.layout.pdf_payment_layout;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater,container,savedInstanceState);

        return view;
    }

    @Override
    protected void loadView() {
        super.loadView();
        paymentFirstName.setText(transaction.creditAccount.name);
        paymentSecondName.setText(transaction.debitAccount.name);
        paymentFirstHolder.setText("PAYMENT TO");
        paymentSecondHolder.setText("PAYMENT THROUGH");
    }
}