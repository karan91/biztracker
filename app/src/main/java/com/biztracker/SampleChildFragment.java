package com.biztracker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by shubham on 09/07/15.
 */
public class SampleChildFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_child_sample, container, false);
        TextView tv = (TextView) v.findViewById(R.id.text_view);
        tv.setText(getArguments().getString("string"));
        return v;
    }
}
