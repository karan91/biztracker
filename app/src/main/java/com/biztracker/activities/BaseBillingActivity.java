package com.biztracker.activities;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.biztracker.fragments.registration.RegistrationObject;
import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by shubham on 23/04/16.
 */
public class BaseBillingActivity extends BaseActivity {

    private final static String BASE64_PUBLIC_LICENSING_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArRt74yxgNJwcANbtHRg4nUq5y3zPyMplfnplCEc4Wmrz/GnUJXaWHainBrPFksCUyOaqS63bB6wYDBBgMEr1Nz5q0sIEw5CWtmFuyJSSUimHbels/O9xRxDAYj6+rDSRoDkviQFJUtvCKR5ZYkvZG79WLyV76H4vxHqx+7d1ZIneb0Sy7RHgNw7gk9BftcYFCC3oTcoJfESiaO/PBjk3ZErIIZG41G0zvoW3TSI4rGBCVMvAP+Wo6fl4w7IbQSnrhsLzQ+WKCKJeYRKkmkIOqhtTQg/6hZ8VsjFbKarQiVuQAWX4fdBg2pa6iZewYRvplth8XIqNchsoYWFuiPLJyQIDAQAB";

    private final static String MONTLY_SUBSCRIPTION = "monthly_biztrack";

    private static final byte[] SALT = new byte[] {
            -46, 65, 30, -128, -103, -57, 74, -64, 51, 88, -95,
            -45, 77, -117, -36, -113, -11, 32, -64, 89
    };

    LicenseChecker mChecker;
    IInAppBillingService mService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent serviceIntent =
                new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        String deviceId = Settings.Secure.ANDROID_ID;
        mChecker = new LicenseChecker(getApplicationContext(),new ServerManagedPolicy(getApplicationContext(),
                new AESObfuscator(SALT,getPackageName(),deviceId)),BASE64_PUBLIC_LICENSING_KEY);
        mChecker.checkAccess(new LicenseCheckerCallback() {
            @Override
            public void allow(int reason) {
                Log.d("BizApp","Allow: "+reason);
            }

            @Override
            public void dontAllow(int reason) {
                Log.d("BizApp","Don't Allow: "+reason);
            }

            @Override
            public void applicationError(int errorCode) {
                Log.d("BizApp","Application Error: "+errorCode);
            }
        });
    }

    @Override
    public void onActivityCreatedForFirstTime() {
    }

    @Override
    public void onActivityRecreated(Bundle savedInstanceState) {

    }


    protected Bundle buyIntentBundle(){
        if(mRegistrationObject == null) mRegistrationObject = new RegistrationObject(getApplicationContext());
        try{
            return mService.getBuyIntent(3,getPackageName(),MONTLY_SUBSCRIPTION,"subs",mRegistrationObject.userid);
        }catch (Exception ex){
            return null;
        }
    }

    protected void startPurchase(Bundle buyBundle) {
        PendingIntent pendingIntent = buyBundle.getParcelable("BUY_INTENT");
        try {
            startIntentSenderForResult(pendingIntent.getIntentSender(),
                    1991291, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
                    Integer.valueOf(0));
        } catch (IntentSender.SendIntentException e) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1991291){
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    Log.d("BizApp","You have Successfully purchased "+sku);
                }
                catch (JSONException e) {
                    Log.e("BizApp","Exception: "+e);
                }
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);

            ArrayList<String> skuList = new ArrayList<>();
            skuList.add(MONTLY_SUBSCRIPTION);
            Bundle querySKUs = new Bundle();
            querySKUs.putStringArrayList("ITEM_ID_LIST", skuList);

            try {
                //Subscriptions
                Bundle skuDetails = mService.getSkuDetails(3, getPackageName(), "subs", querySKUs);
                Log.d("BizApp","sku: "+skuDetails);

                int response = skuDetails.getInt("RESPONSE_CODE");
                if(response == 0){
                    ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");
                    for (String thisResponse : responseList) {
                        JSONObject object = new JSONObject(thisResponse);
                        String sku = object.getString("productId");
                        String price = object.getString("price");

                        Log.d("BizApp","SKU: "+sku+" Price:"+price);
                    }
                }

            }catch (Exception ex){
                Log.e("BizApp","Exception: "+ex);
            }
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mService != null){
            unbindService(mServiceConn);
        }
        if(mChecker != null){
            mChecker.onDestroy();
        }
    }
}
