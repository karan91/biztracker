package com.biztracker.activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.biztracker.R;
import com.biztracker.barcodescanner.wrapper.BarcodeScanner;
import com.biztracker.common.Utilities;
import com.biztracker.databaseobjects.Company;
import com.biztracker.databaseobjects.TransactionType;
import com.biztracker.fragments.HomeScreen;
import com.biztracker.fragments.Transactions.paymentsAndReceipts.PaymentFrag;
import com.biztracker.fragments.Transactions.paymentsAndReceipts.ReceiptFrag;
import com.biztracker.fragments.company.CompanyInformationEditPage;
import com.biztracker.fragments.masteraccounts.CustomerAccountFragment;
import com.biztracker.fragments.masteraccounts.MasterAccountFragment;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.Transactions.invoice.PurchaseInvoiceFrag;
import com.biztracker.fragments.Transactions.invoice.SalesInvoiceFrag;
import com.biztracker.fragments.additems.ProductFragment;
import com.biztracker.fragments.Transactions.invoice.BaseInvoice.InvoiceFrag;
import com.biztracker.fragments.masteraccounts.OtherAccountFragment;
import com.biztracker.fragments.masteraccounts.SellerAccountFragment;
import com.biztracker.fragments.registration.RegistrationObject;
import com.biztracker.fragments.taxesanddiscounts.AddDiscountsFragment;
import com.biztracker.fragments.taxesanddiscounts.AddTaxChargesFragment;
import com.biztracker.fragments.universal_search.UniversalSearchFragment;
import com.biztracker.services.LockStatusChangeReceiver;
import com.freshdesk.hotline.Hotline;
import com.freshdesk.hotline.HotlineCallbackStatus;
import com.freshdesk.hotline.HotlineConfig;
import com.freshdesk.hotline.HotlineUser;
import com.freshdesk.hotline.UnreadCountCallback;
import com.google.android.gms.vision.barcode.Barcode;


public class MainActivity extends BaseActivity{

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    public BarcodeScanner barcodeScanner;

    public TextView companyNameHolder;

    private String[] allOptions = new String[]{
            "Universal Search",
            "Add Invoice",
            "Add Account",
            "Add Product",
            "Add Tax / Additional Charges",
            "Add Discount",
    };

    private Class[] allFragmentClasses = new Class[]{
            UniversalSearchFragment.class,
            InvoiceFrag.class,
            MasterAccountFragment.class,
            ProductFragment.class,
    };

    HomeScreen homescreen;

    private LockStatusChangeReceiver mLockStatusChangeReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HotlineConfig hConfig=new HotlineConfig(getResources().getString(R.string.appid),getResources().getString(R.string.apikey));
        hConfig.setVoiceMessagingEnabled(false);
        hConfig.setCameraCaptureEnabled(false);
        hConfig.setPictureMessagingEnabled(true);

        Hotline hotline = Hotline.getInstance(getApplicationContext());
        hotline.init(hConfig);

        //Set Hotline User Details
        HotlineUser user = hotline.getUser();
        user.setName(mRegistrationObject.userFullName);
        user.setExternalId(mRegistrationObject.userid);
        user.setEmail(mRegistrationObject.businessEmail);


        barcodeScanner = (BarcodeScanner) this.findViewById(R.id.barcode_stream);
        companyNameHolder = (TextView) this.findViewById(R.id.company_name);
        this.findViewById(R.id.company_edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        Company company = Company.company;
        companyNameHolder.setText(company.businessName);
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED);
        //this.deleteDatabase("null.bt");
        if(savedInstanceState == null) {
            setAsCurrentFragment(getFragmentForClass(UniversalSearchFragment.class), R.id.root_fragment_frame);
        }else{

        }

        onInitializeNavigationDrawer();

        if(Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(0xf7232E3A);
        }

        mLockStatusChangeReceiver = new LockStatusChangeReceiver(this,mRegistrationObject.isAppLocked);

    }

    @Override
    protected void onResume() {
        super.onResume();
        startBarCodeScanner();
        if(mLockStatusChangeReceiver != null) registerReceiver(mLockStatusChangeReceiver, new IntentFilter(LockStatusChangeReceiver.LOCK_ACTION));

        Hotline.getInstance(getApplicationContext()).getUnreadCountAsync(new UnreadCountCallback() {
            @Override
            public void onResult(HotlineCallbackStatus hotlineCallbackStatus, int unreadCount) {
                //Assuming "badgeTextView" is a text view to show the count on

            }
        });


    }

    @Override
    protected void onPause() {
        if(mLockStatusChangeReceiver != null)   unregisterReceiver(mLockStatusChangeReceiver);
        stopBarcodeScanner();
        super.onPause();
    }

    @Override
    public void onActivityCreatedForFirstTime() {

    }

    @Override
    public void onActivityRecreated(Bundle savedInstanceState) {

    }

    public void onOpenDrawer(View view){
        mDrawerLayout.openDrawer(Gravity.LEFT);
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
        if(manager.getBackStackEntryCount() != 0){
            super.onBackPressed();
            return;
        }
        Fragment f = manager.findFragmentById(R.id.root_fragment_frame);
        if(f instanceof SuperFragment)  ((SuperFragment)f).onBackPressed();
        else    super.onBackPressed();
    }


    /**
     * Handling Navigation Drawer
     * */
    private void onInitializeNavigationDrawer(){
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
    }

    public void onShowCompanyEditPage(View view){
        if(mCurrentFragment != null){
            mCurrentFragment.transitionToFragment(getFragmentForClass(CompanyInformationEditPage.class));
        }else{
            setAsCurrentFragment(getFragmentForClass(CompanyInformationEditPage.class),R.id.root_fragment_frame);
        }
        if(mDrawerLayout != null)   mDrawerLayout.closeDrawers();
    }

    public void openWhatsapp(String id){
        String smsNumber="9958987692@s.whatsapp.net";
        Uri uri = Uri.parse("smsto:" + smsNumber);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.putExtra("sms_body", "Prakash Gajera");
        i.setPackage("com.whatsapp");
        startActivity(i);
    }

    public void onDrawerItemClick(View view){
        Log.d("MainActivity","onDrawerClicked: ");
        if(mDrawerLayout != null)   mDrawerLayout.closeDrawers();

        //openWhatsapp("");

        switch (view.getId()){
            //Search
            case R.id.search_invoice:
            case R.id.search_accounts:
            case R.id.search_products:
                UniversalSearchFragment fragment = (UniversalSearchFragment) getFragmentForClass(UniversalSearchFragment.class);
                if(view.getId() == R.id.search_invoice){
                    fragment.setCurrentVisiblePage(0);
                }else if(view.getId() == R.id.search_accounts){
                    fragment.setCurrentVisiblePage(1);
                }else if(view.getId() == R.id.search_products){
                    fragment.setCurrentVisiblePage(2);
                }
                setAsCurrentFragment(fragment,R.id.root_fragment_frame);
                break;
            //New
            case R.id.new_sales_invoice:
                InvoiceFrag frag = (SalesInvoiceFrag) getFragmentForClass(SalesInvoiceFrag.class);
                if(frag.invoiceTransaction == null){
                    Bundle b = new Bundle();
                    b.putString("transaction_type", TransactionType.TRANSACTION_TYPE_SALES);
                    frag.setArguments(b);
                    startBarCodeScanner();
                }
                frag.onFragmentOpened();
                setAsCurrentFragment(frag,R.id.root_fragment_frame);
                break;
            case R.id.new_purchase_invoice:
                frag = (PurchaseInvoiceFrag) getFragmentForClass(PurchaseInvoiceFrag.class);
                if(frag.invoiceTransaction == null){
                    Bundle b = new Bundle();
                    b.putString("transaction_type", TransactionType.TRANSACTION_TYPE_PURCHASE);
                    frag.setArguments(b);
                }
                frag.onFragmentOpened();
                setAsCurrentFragment(frag,R.id.root_fragment_frame);
                break;
            case R.id.new_payment:
                PaymentFrag paymentFrag = (PaymentFrag) getFragmentForClass(PaymentFrag.class);
                setAsCurrentFragment(paymentFrag, R.id.root_fragment_frame);
                break;
            case R.id.new_receipt:
                ReceiptFrag receiptFrag = (ReceiptFrag) getFragmentForClass(ReceiptFrag.class);
                setAsCurrentFragment(receiptFrag, R.id.root_fragment_frame);
                break;
            //Inventory
            case R.id.add_product:
                setAsCurrentFragment(getFragmentForClass(ProductFragment.class),R.id.root_fragment_frame);
                break;
            //Add
            case R.id.add_customer:
                setAsCurrentFragment(getFragmentForClass(CustomerAccountFragment.class),R.id.root_fragment_frame);
                break;
            case R.id.add_supplier:
                setAsCurrentFragment(getFragmentForClass(SellerAccountFragment.class), R.id.root_fragment_frame);
                break;
            case R.id.add_other_accounts:
                setAsCurrentFragment(getFragmentForClass(OtherAccountFragment.class),R.id.root_fragment_frame);
                break;

            case R.id.add_taxes:
                setAsCurrentFragment(getFragmentForClass(AddTaxChargesFragment.class),R.id.root_fragment_frame);
                break;
            case R.id.add_discounts:
                setAsCurrentFragment(getFragmentForClass(AddDiscountsFragment.class),R.id.root_fragment_frame);
                break;
            case R.id.conversation_btn:
                Hotline.showConversations(getApplicationContext());
                break;
            /*case R.id.donate:

                Intent i = new Intent(this, DonateActivity.class);
                startActivity(i);
                break;*/
        }


    }

    public void activateBarcodeScanner(boolean activate){
        if(activate){
            startBarCodeScanner();
        }
        else{
            stopBarcodeScanner();
        }
    }

    private void startBarCodeScanner() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(!barcodeScanner.active){
                    barcodeScanner.activateBarcodeScanner(MainActivity.this);
                }
                barcodeScanner.startCameraSource(MainActivity.this);
            }
        }).start();
    }

    public void setBarcodeScannerHidden(boolean hidden){
        if(hidden == true){
            barcodeScanner.setVisibility(View.GONE);
        }
        else{
            barcodeScanner.setVisibility(View.VISIBLE);
        }
    }

    private void stopBarcodeScanner(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(barcodeScanner != null) {
                    barcodeScanner.active = false;
                    barcodeScanner.stop();
                    barcodeScanner.release();
                }
            }
        }).start();
    }

    private void openDrawer(){
        if(mDrawerLayout != null){
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
    }

}
