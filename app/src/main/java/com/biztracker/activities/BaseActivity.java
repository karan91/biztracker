package com.biztracker.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.biztracker.R;
import com.biztracker.dialogs.LockPopupDialog;
import com.biztracker.fragments.SuperFragment;
import com.biztracker.fragments.Transactions.invoice.ViewablePurchaseInvoiceFrag;
import com.biztracker.fragments.Transactions.invoice.ViewableSalesInvoiceFrag;
import com.biztracker.fragments.Transactions.paymentsAndReceipts.ViewablePaymentFrag;
import com.biztracker.fragments.Transactions.paymentsAndReceipts.ViewableReceiptFrag;
import com.biztracker.fragments.masteraccounts.CustomerAccountEditingFragment;
import com.biztracker.fragments.masteraccounts.OtherAccountEditingFragment;
import com.biztracker.fragments.masteraccounts.SellerAccountEditingFragment;
import com.biztracker.fragments.registration.RegistrationObject;
import com.biztracker.permissions.PermissionRequester;
import com.biztracker.services.ConnectivityChangeReceiver;
import com.biztracker.services.LockStatusChangeReceiver;
import com.biztracker.services.ServerInteractionService;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham on 09/07/15.
 */
public abstract class BaseActivity extends FragmentActivity implements LockPopupDialog.LockPopupDialogCallback,
        ConnectivityChangeReceiver.OnConnectivityChanged, LockStatusChangeReceiver.OnLockStatusChanged{

    private Object permissionLock = new Object();
    private ArrayList<PermissionRequester> mPermissionRequester = new ArrayList<>();

    //TODO: Restoration Still Not Properly Implemented
    private Object lock = new Object();
    protected List<SuperFragment> fragments = new ArrayList<>();
    protected SuperFragment mCurrentFragment = null;

    public RegistrationObject mRegistrationObject = null;

    private ConnectivityChangeReceiver mNetworkChangeReceiver = null;

    private LockPopupDialog mLockPopupDialog = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRegistrationObject = new RegistrationObject(getApplicationContext());

        if(savedInstanceState == null){
            onActivityCreatedForFirstTime();
        }else{
            onRestoreFragments(savedInstanceState);
            onActivityRecreated(savedInstanceState);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mNetworkChangeReceiver = new ConnectivityChangeReceiver(this, getApplicationContext());
        registerReceiver(mNetworkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        if(mRegistrationObject.shouldLockApp()){
            //Show Lock Dialog
            lockApp();
        }
    }

    @Override
    protected void onPause() {
        if(mNetworkChangeReceiver != null)  unregisterReceiver(mNetworkChangeReceiver);
        super.onPause();
        unlockApp();
    }


    @Override
    public void onLockStatusChanged(boolean isAppLocked) {
        if(isAppLocked){
            lockApp();
        }else{
            unlockApp();
        }
    }

    /**
     * On Connectivity Changed
     */
    @Override
    public void onConnectedToInternet() {
        Log.d("BizApp", "onConnectedToInternet");
        Intent intent = new Intent(this, ServerInteractionService.class);
        intent.putExtra("starter","receiver");
        startService(intent);
    }


    /**
     * App Lock Handler
     */
    public void lockApp(){
        //If LockPopuDialog is Already Showing Then Return
        if(mLockPopupDialog != null && mLockPopupDialog.isShowing() == true) return;
        //Show It
        if(mRegistrationObject == null) mRegistrationObject = new RegistrationObject(getApplicationContext());
        mLockPopupDialog = new LockPopupDialog(this,mRegistrationObject.lockMessage,mRegistrationObject.purchaseAction);
        mLockPopupDialog.mCallback = this;
        mLockPopupDialog.show();
    }

    public void unlockApp(){
        if(mLockPopupDialog != null){
            mLockPopupDialog.dismiss();
            mLockPopupDialog = null;
        }
    }


    @Override
    public void onClickedPurchase() {

    }

    @Override
    public void onClickedContactUs() {

    }

    public abstract void onActivityCreatedForFirstTime();

    public abstract void onActivityRecreated(Bundle savedInstanceState);

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<String> fragmentTags = new ArrayList<>();
        for(SuperFragment fragment: fragments){
            String tag = fragment.getTag();
            if(tag!=null){
                fragmentTags.add(tag);
            }
        }
        outState.putStringArrayList("fragments-list", fragmentTags);
        Log.d("onSaveInstanceState","Fragments: "+fragmentTags);
    }

    private void onRestoreFragments(Bundle savedState){
        ArrayList<String> fragmentTags = savedState.getStringArrayList("fragments-list");
        Log.d("onRestoreInstance", "Fragments: " + fragmentTags);
        if(fragmentTags != null) {
            for (String tag : fragmentTags) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
                if(fragment != null){
                    Log.d("BaseActivity","Restored Fragment: "+fragment);
                    fragments.add((SuperFragment)fragment);
                }
            }
            //initialize Fragments
        }
    }

    protected SuperFragment getFragmentForClass(Class klass){
        for(SuperFragment fragment: fragments){
            if(fragment.getClass().equals(klass)){
                return fragment;
            }
        }
        SuperFragment fragment = (SuperFragment)Fragment.instantiate(this,klass.getName());
        if(fragment instanceof ViewablePaymentFrag || fragment instanceof ViewableReceiptFrag
                || fragment instanceof ViewablePurchaseInvoiceFrag || fragment instanceof ViewableSalesInvoiceFrag
                || fragment instanceof CustomerAccountEditingFragment || fragment instanceof SellerAccountEditingFragment || fragment instanceof OtherAccountEditingFragment){

        }else {
            fragments.add(fragment);
        }
        return fragment;
    }

    public void setAsCurrentFragment(SuperFragment fragment,int resID){
        mCurrentFragment = fragment;
        getSupportFragmentManager().popBackStackImmediate("ppbackstack", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager().beginTransaction().replace(resID,fragment,fragment.getClass().getName()).commit();
    }

    /**
     * PermissionRequester Handler Started
     * */

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        synchronized (permissionLock){
            PermissionRequester mRequester = null;
            for(PermissionRequester permissionRequester: mPermissionRequester){
                if(permissionRequester.canHandleRequestCode(requestCode)){
                    permissionRequester.handlePermissionRequestCompleted(permissions,grantResults);
                    mRequester = permissionRequester;
                    break;
                }
            }

            if(mRequester!=null)    mPermissionRequester.remove(mRequester);
        }
    }

    public void requestPermission(String permission,PermissionRequester.OnPermissionRequesterCompleted callback){
        synchronized (permissionLock) {
            if (PermissionRequester.shouldShowRequestPermissionRationale(this,permission)) {
                PermissionRequester requester = new PermissionRequester(callback, new String[]{permission});
                mPermissionRequester.add(requester);
                requester.requestPermissions(this);
            } else {
                if (callback != null) {
                    callback.onPermissionGranted(permission);
                }
            }
        }
    }

    /**
     * PermissionRequester Handler Completed
     */


    public void shareIntentForFile(String filepath,Uri fileUri){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("application/pdf");
        intent.putExtra(android.content.Intent.EXTRA_STREAM, fileUri);
        try {
            startActivity(Intent.createChooser(intent, "Send Invoice To: "));
        }catch (Exception ex){}
    }


}
