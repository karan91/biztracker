package com.biztracker.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.biztracker.BizTrackerApp;
import com.biztracker.R;
import com.biztracker.fragments.registration.RegistrationObject;

/**
 * Created by shubham on 16/04/16.
 */
public class LaunchActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_get_started);
    }

    @Override
    protected void onResume() {
        super.onResume();

        RegistrationObject mRegistrationObject = new RegistrationObject(getApplicationContext());
        if(mRegistrationObject.isOfflineSetupCompleted == false){
            //Show Registration Activity

        }else{
            if(BizTrackerApp.isNetworkAvailable(getApplicationContext())){

            }

            if(mRegistrationObject.shouldLockApp()){

            }else{

            }
        }

    }

}
