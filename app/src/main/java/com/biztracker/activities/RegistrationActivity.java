package com.biztracker.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.biztracker.R;
import com.biztracker.common.Utilities;
import com.biztracker.databaseobjects.Company;
import com.biztracker.fragments.registration.CustomViewPager;
import com.biztracker.fragments.registration.fragments.BarCodeSetupScreen;
import com.biztracker.fragments.registration.fragments.CompanySetupScreen;
import com.biztracker.fragments.registration.fragments.GetStartedFragment;
import com.biztracker.fragments.registration.fragments.SetupFragment;
import com.biztracker.fragments.registration.fragments.TaxSetupScreen;
import com.biztracker.services.ServerInteractionService;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by shubham on 15/04/16.
 */
public class RegistrationActivity extends BaseActivity {

    private GoogleSignInOptions mGoogleSignInOptions;
    private GoogleApiClient mGoogleApiClient;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.setLocaleForFixingCrash();
        setContentView(R.layout.activity_registration);

        /*mGoogleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestId()
                .requestIdToken("452406325148-5brtk9kh5oaem8bnhnleppo76c95b092.apps.googleusercontent.com").build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, mOnConnectionFailedListener).addApi(Auth.GOOGLE_SIGN_IN_API, mGoogleSignInOptions).build();
        */

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        if(mViewPager instanceof CustomViewPager){
            ((CustomViewPager)mViewPager).setPagingEnabled(false);
        }
        mViewPager.setAdapter(new RegistrationPagerAdapter(getSupportFragmentManager()));

        if (mRegistrationObject.isOfflineSetupCompleted == false) {
            //User has not completed offline setup

            if (mRegistrationObject.hasCompletedBusinessRegistration == false) {
                mViewPager.setCurrentItem(0);
            } else if (mRegistrationObject.hasCompletedTaxSetupInformation == false) {
                mViewPager.setCurrentItem(2);
            } else if (mRegistrationObject.hasCompletedBarcodeScannerScreen == false) {
                Company.setCompanyFromBusinessName("company");
                mViewPager.setCurrentItem(3);
            }

        } else {
            Intent regIntent = new Intent(this, ServerInteractionService.class);
            regIntent.putExtra("starter","registration");
            startService(regIntent);

            if (mRegistrationObject.shouldLockApp() == false) {
                //Continue To Next Screen
                Company.setCompanyFromBusinessName("company");
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        }

        if(Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(0xf7232E3A);
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("BizApp","onConfigurationChanged: "+newConfig);
    }

    private GoogleApiClient.OnConnectionFailedListener mOnConnectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {

        }
    };

    public void onGoogleSignInClicked(View view){
        Intent signinIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signinIntent,1234);
    }

    public void onSuccessfulSignIn(GoogleSignInResult mGoogleResult){
        //Register User if Not

    }

    public void onFailedSignIn(){

    }




    public void onGetStartedClicked(View view){
        ((ViewPager)findViewById(R.id.view_pager)).setCurrentItem(1,true);
    }


    @Override
    public void onActivityCreatedForFirstTime() {
    }

    @Override
    public void onActivityRecreated(Bundle savedInstanceState) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1234){
            //Google SignIn
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        int position = mViewPager.getCurrentItem();
        if(position == 0){
            super.onBackPressed();
        }else{
            mViewPager.setCurrentItem(position - 1);
        }
    }

    private SetupFragment.OnSetupFragmentCallback mFragmentCallback = new SetupFragment.OnSetupFragmentCallback() {
        @Override
        public void showPagerFragmentAtIndex(int index) {
            ((ViewPager)findViewById(R.id.view_pager)).setCurrentItem(index,true);
        }

        @Override
        public void onRegistrationCompleted() {
            Intent regIntent = new Intent(RegistrationActivity.this, ServerInteractionService.class);
            startService(regIntent);
            //Move To Main Activity
            Intent intent = new Intent(RegistrationActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }
    };

    private class RegistrationPagerAdapter extends FragmentPagerAdapter{

        public RegistrationPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) return new GetStartedFragment();

            SetupFragment fragment = null;
            switch (position){
                case 1: fragment= new CompanySetupScreen(); break;
                case 2: fragment = new TaxSetupScreen(); break;
                case 3: fragment = new BarCodeSetupScreen(); break;
            }

            if(fragment != null){
                fragment.setRegistrationObject(mRegistrationObject);
                fragment.setIndex(position);
                fragment.mCallback = mFragmentCallback;
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}
