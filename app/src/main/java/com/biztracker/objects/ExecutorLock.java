package com.biztracker.objects;

/**
 * Created by shubham on 09/07/15.
 */

public class ExecutorLock {

    private boolean locked;
    private boolean hasSynchronousTask;
    private Object object = new Object();

    public ExecutorLock(){
        locked = false;
        hasSynchronousTask = false;
    }

    public void executeSynchronousTask(final Runnable task){
        hasSynchronousTask = true;
        while(locked) continue;
        synchronized (object) {
            locked = true;
            task.run();
            locked = false;
        }
    }

    public void executeAsynchronousTask(final Runnable task){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(hasSynchronousTask) continue;
                while(locked) continue;
                synchronized (object) {
                    locked = true;
                    task.run();
                    locked = false;
                }
            }
        });

    }

}
