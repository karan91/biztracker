package com.biztracker.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * Created by shubham on 24/06/16.
 */
public class AccountAutoCompleteTextView extends AutoCompleteTextView {

    private static Typeface regularTypeface = null;

    public AccountAutoCompleteTextView(Context context) {
        super(context);
        init(context);
    }

    public AccountAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AccountAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public AccountAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context context){
        if(regularTypeface == null) regularTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Regular.ttf");
        setTypeface(regularTypeface);
    }



}


