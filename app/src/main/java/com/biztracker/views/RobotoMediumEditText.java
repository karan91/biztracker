package com.biztracker.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;

import com.biztracker.R;

/**
 * Created by shubham on 16/04/16.
 */
public class RobotoMediumEditText extends KeyPreIMEEditText {

    private static Typeface mediumTypeface;
    private boolean doFocusParent = false;

    public RobotoMediumEditText(Context context) {
        super(context);
        init(context);
    }

    public RobotoMediumEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RobotoMediumEditText, 0, 0);
        doFocusParent = ta.getBoolean(0,false);
        init(context);
    }

    public RobotoMediumEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RobotoMediumEditText, 0, 0);
        doFocusParent = ta.getBoolean(0,false);
        init(context);
    }

    public RobotoMediumEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RobotoMediumEditText, 0, 0);
        doFocusParent = ta.getBoolean(0,false);
        init(context);
    }

    private void init(Context context){
        if(mediumTypeface == null)  mediumTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Medium.ttf");
        setTypeface(mediumTypeface);
        if(doFocusParent){
            setOnFocusChangeListener(mFocusChangeListener);
        }
    }

    private OnFocusChangeListener mFocusChangeListener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            setParentBackgroundColor(b);
        }
    };

    public void setParentBackgroundColor(boolean isFocussed){
        ViewParent parent = getParent();
        if(parent != null){
            if(parent instanceof View){
                View view = (View)parent;
                if(isFocussed)  view.setBackgroundResource(R.drawable.textfield_background_focussed_rounded);
                else            view.setBackgroundResource(R.drawable.textfield_background_rounded);
            }
        }else{
            Log.d("BizApp","====================>PARENT IS NULL:");
        }
    }
}
