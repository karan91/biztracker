package com.biztracker.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

import com.biztracker.databaseobjects.InvoicedTransaction;

/**
 * Created by karan on 23/07/16.
 */

public class InvoiceNumberEditText extends RobotoMediumEditText {

    public InvoicedTransaction transaction;

    public InvoiceNumberEditText(Context context) {
        super(context);
    }

    public InvoiceNumberEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InvoiceNumberEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public InvoiceNumberEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK &&
                event.getAction() == KeyEvent.ACTION_UP) {
            this.setText(transaction.transactionNumber);
            this.clearFocus();
            return false;
        }
        return super.dispatchKeyEvent(event);
    }
}
