package com.biztracker.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by shubham on 16/04/16.
 */
public class FuturaMediumTextView extends TextView {

    private static Typeface futuraMediumTypeface;

    public FuturaMediumTextView(Context context) {
        super(context);
    }

    public FuturaMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FuturaMediumTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public FuturaMediumTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void init(Context context){
        if(futuraMediumTypeface == null) futuraMediumTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/Futura.ttc");
        setTypeface(futuraMediumTypeface);
    }
}
