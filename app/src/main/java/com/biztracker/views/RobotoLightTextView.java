package com.biztracker.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by shubham on 15/04/16.
 */
public class RobotoLightTextView extends TextView {

    private static Typeface lightTypeface;

    public RobotoLightTextView(Context context) {
        super(context);
        init(context);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context){
        if(lightTypeface == null)  lightTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Light.ttf");
        setTypeface(lightTypeface);
    }
}
