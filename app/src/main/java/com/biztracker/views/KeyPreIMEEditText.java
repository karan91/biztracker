package com.biztracker.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by shubham on 23/04/16.
 */
public abstract class KeyPreIMEEditText extends EditText {

    public interface OnKeyPreIMEKeyboardHiddenListener{
        void onKeyboardHidden(EditText editText);
    }

    public OnKeyPreIMEKeyboardHiddenListener mKeyboardHiddenListener;
    private Timer timer;

    public KeyPreIMEEditText(Context context) {
        super(context);
    }

    public KeyPreIMEEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KeyPreIMEEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public KeyPreIMEEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            // Do your thing.
            if(timer != null)   timer.cancel();

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    onTimerCalled();
                }
            },200); //Delay of 200 ms
        }
        return super.dispatchKeyEvent(event);
    }

    public void onTimerCalled(){
        if(mKeyboardHiddenListener != null){
            mKeyboardHiddenListener.onKeyboardHidden(this);
        }
    }
}
