package com.biztracker.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by shubham on 15/04/16.
 */
public class RobotoBoldTextView extends TextView {

    private static Typeface boldTypeface;

    public RobotoBoldTextView(Context context) {
        super(context);
        init(context);
    }

    public RobotoBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RobotoBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public RobotoBoldTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context context){
        if(boldTypeface == null) boldTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Bold.ttf");
        setTypeface(boldTypeface);
    }
}
