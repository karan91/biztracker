package com.biztracker.utilities;

import android.util.Log;

/**
 * Created by shubham on 09/07/15.
 */

public class Logger {

    public final static int VERBOSE = 1;
    public final static int DEBUG = 2;
    public final static int INFO = 3;
    public final static int WARNING = 4;
    public final static int ERROR = 5;

    private final static boolean PRINT_LOGS = true;
    private final static String TAG = "com.biztracker";
    public static int LOG_LEVEL = INFO;

    public static void verbose(String tag,String message){
        if(LOG_LEVEL == VERBOSE && PRINT_LOGS){
            Log.v(tag,message);
        }
    }

    public static void debug(String tag,String message){
        if(LOG_LEVEL <= DEBUG && PRINT_LOGS){
            Log.d(tag,message);
        }
    }

    public static void info(String tag,String message){
        if(LOG_LEVEL <= INFO && PRINT_LOGS){
            Log.i(tag, message);
        }
    }

    public static void warn(String tag,String message){
        if(LOG_LEVEL <= WARNING && PRINT_LOGS){
            Log.w(tag,message);
        }
    }

    public static void error(String tag,String message){
        if(LOG_LEVEL <= ERROR && PRINT_LOGS){
            Log.e(tag,message);
        }
    }

    public static void verbose(String message){
        if(PRINT_LOGS) verbose(TAG,message);
    }

    public static void debug(String message){
        if(PRINT_LOGS) debug(TAG,message);
    }

    public static void info(String message){
        if(PRINT_LOGS) info(TAG,message);
    }

    public static void warn(String message){
        if(PRINT_LOGS) warn(TAG,message);
    }

    public static void error(String message){
        if(PRINT_LOGS) error(TAG,message);
    }
}