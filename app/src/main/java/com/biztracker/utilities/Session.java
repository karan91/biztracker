package com.biztracker.utilities;

import android.content.Context;

import com.biztracker.BizTrackerApp;
import com.biztracker.activities.MainActivity;
import com.biztracker.contacts_reader.ContactsReader;
import com.biztracker.database.DatabaseTalker;

/**
 * Created by karan on 06/02/16.
 */
public class Session {

    private static DatabaseTalker talker;
    private static Session session;
    public static ContactsReader mContactsReader;


    public static synchronized Session getInstance() {
        if (session == null) {
            session = new Session();
            talker = DatabaseTalker.getInstance();
        }
        return session;
    }

}
